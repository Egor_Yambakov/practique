<?
    require_once 'C:/Users/Егор/vendor/autoload.php';
    $loader = new Twig_Loader_Filesystem('templates');
    $twig = new Twig_Environment
                (
                    $loader, 
                    array
                    (
                        'cache'       => 'compilation_cache',
                        'auto_reload' => true
                    )
                );

    $side_bar_data = array
    (
        array('link' => '#', 'tittle' => 'Мой профиль'),
        array('link' => '#', 'tittle' => 'Библиотека'),
        array('link' => '#', 'tittle' => 'Заказ еды'),
    );
    $pasport = array
    (
        'name'        => $_POST['name'],
        'foto_link'   => $_POST['foto_link'],
        'department'  => $_POST['department'],
        'post'        => $_POST['post'],
        'phone'       => $_POST['phone'],
        'email'       => $_POST['email'],
        'skills'      => preg_split("/s*\,s*/", $_POST["skills"]),
        'hobbys'      => preg_split("/s*\,s*/", $_POST["hobbys"]),
        'duties_text' => $_POST['duties_text']
    );

    echo $twig->render('_base_child.tpl', array
        (
            'page_foto'    => $pasport['foto_link'],
            'page_name'    => $pasport['name'],
            'department'   => $pasport['department'],
            'post'         => $pasport['post'],
            'phone_number' => $pasport['phone'],
            'email'        => $pasport['email'],
            'duties_text'  => $pasport['duties_text'],
            'links'        => $side_bar_data,
            'tittle'       => 'Заголовок',
            'skills'       => $pasport['skills'],
            'hobbys'       => $pasport['hobbys']
        )
    );