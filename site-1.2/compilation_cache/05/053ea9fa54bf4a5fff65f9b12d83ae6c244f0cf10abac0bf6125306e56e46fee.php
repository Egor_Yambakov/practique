<?php

/* _header.tpl */
class __TwigTemplate_8c2fa9ca2ba03eaa7adf40668a9a8059d39450a650505758874ea237b4bfb9af extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"top_bar\">
  <div class=\"brand\">
    <div class=\"main_logo\">
      <a href=\"#\">
        <img src=\"templates/images/ispring-logo-bold.png\" alt=\"ispring-logo\">
      </a>
    </div>
    <div class=\"main_tittle\">   
      <span>";
        // line 9
        echo twig_escape_filter($this->env, ($context["tittle"] ?? null), "html", null, true);
        echo "</span>
    </div>
  </div>
  <form class=\"search_bar\" method=\"get\" action=\"search.php\">
    <input type=\"text\" name=\"search\" placeholder=\"Поиск\" class=\"seatch_input\">
  </form>
  <div class=\"login_btn\">
    <a href=\"#\" class=\"login_link\">Войти</a>
  </div>
  <div class=\"clear_all\"></div>
</div>";
    }

    public function getTemplateName()
    {
        return "_header.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 9,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_header.tpl", "D:\\Files\\doc\\BEP\\sites\\site-1.2\\templates\\_header.tpl");
    }
}
