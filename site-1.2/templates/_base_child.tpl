{% extends "base.html" %}

{% block tittle %}{{- page_name -}}{% endblock %}

{% block top_container %}
  {% include "_header.tpl" %}
{% endblock %}

{% block main_content %}
  {% include "_content.tpl" %}
{% endblock %}