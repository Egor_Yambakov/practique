<h2 class="page_name red_border">{{- page_name -}}</h2>
<h4 class="info_header">Работа</h4>
<ul class="record">
  <li class="department">
    <span class="field">Отделение: </span>
    <span class="note">{{- department -}}</span>
  </li>
  <li class="post">
    <span class="field">Должность: </span>
    <span class="note">{{- post -}}</span>
  </li>
</ul>
<h4 class="info_header">Контакты</h4>
<ul class="record">
  <li class="phone">
    <span class="field">Телефон: </span>
    <span class="note">{{- phone_number -}}</span>
  </li>
  <li class="mail">
    <span class="field">Почта: </span>
    <a href="mailto:{{- email -}}">{{- email -}}</a>
  </li>
</ul>
<h4 class="info_header">Чем занимается в компании</h4>
<ul class="record">
  <li class="field">
    <p>
      {{- duties_text -}}
    </p>
  </li>
</ul>