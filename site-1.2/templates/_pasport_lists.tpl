<div>
  <h3>Навыки</h3>
  <ul>
    {% for skill in skills %}
    <li>
      {{- skill -}}
    </li>
    {% endfor %}
  </ul>
</div>
<div>
  <h3>Интересы</h3>
  <ul>
    {% for hobby in hobbys %}
    <li>
      {{- hobby -}}
    </li>
    {% endfor %}
  </ul>
</div>