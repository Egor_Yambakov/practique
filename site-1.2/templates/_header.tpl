<div class="top_bar">
  <div class="brand">
    <div class="main_logo">
      <a href="#">
        <img src="images/ispring-logo-bold.png" alt="ispring-logo">
      </a>
    </div>
    <div class="main_tittle">   
      <span>{{tittle}}</span>
    </div>
  </div>
  <form class="search_bar" method="get" action="search.php">
    <input type="text" name="search" placeholder="Поиск" class="seatch_input">
  </form>
  <div class="login_btn">
    <a href="#" class="login_link">Войти</a>
  </div>
  <div class="clear_all"></div>
</div>