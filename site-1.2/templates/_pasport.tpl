<div class="side_column">
  <div class="page_block">
    {% include "_foto_block.tpl" %}
  </div>

  <div class="page_block secondary_info">
    {% include "_pasport_lists.tpl" %}
  </div>
</div>

<div class="main_column page_block">
  {% include "_pasport_main.tpl" %}
</div>