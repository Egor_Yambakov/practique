<?
    require_once 'C:/Users/Егор/vendor/autoload.php';
    $loader = new Twig_Loader_Filesystem('templates');
    $twig = new Twig_Environment
                (
                    $loader, 
                    array
                    (
                        'cache'       => 'compilation_cache',
                        'auto_reload' => true
                    )
                );

    $db = mysqli_connect("localhost", "root", "");
    mysqli_select_db($db, "ispring_team");
    mysqli_query($db, "SET NAMES 'utf8'");
    $table = mysqli_query($db, "SELECT * FROM `new`");

    $list = array();

    while($row = mysqli_fetch_array($table)) {
        $row['reg_date'] = convertTime($row['reg_date']);
        array_push($list, $row);
    };
    mysqli_close($db);

    echo $twig->render('_base_child.tpl', array('list' => $list));


    function convertTime($time) {
        return date("M j, Y", $time);
    };