window.onload = onWindowLoaded;

function onWindowLoaded() {
  $("#check_all").click(selectAll); 
  $(".checkbox").click(selectBox);

  $("#add_user").click(openForm);
  $("#new_user_form").click(closeForm);
  
  $(".form_container form").submit(addUser);
  $("#remove_user").click(removeUser);
}

function selectAll() {
  if ($("#check_all").hasClass("selected")) {
    $(".checkbox, #check_all").removeClass("selected");
  } else {
    $(".checkbox, #check_all").addClass("selected");
  }
}

function selectBox(event) {
  if ($(event.target).hasClass("selected")) {
    $(event.target).removeClass("selected");
  } else {
    $(event.target).addClass("selected");
  };
}

function openForm() {
  $("#new_user_form").css("display", "block");
}

function closeForm(event) {
  if ($(event.target).hasClass("close_form") || $(event.target).hasClass("background_container")) {
    $("#new_user_form").css("display", "auto");
  };
}

function addUser() {
  event.preventDefault();

  $.post(
    "add_user.php",
    {
      name: $("input[name = 'name']").val(),
      password: $("input[name = 'password']").val(),
      department: $("input[name = 'department']").val(),
      status: $("input[name = 'status']").val(),
      phone: $("input[name = 'phone']").val(),
      email: $("input[name = 'email']").val()
    },
    onAddSuccess
  );
}

function onAddSuccess(data) {
  data = $.parseJSON(data);

  if (data['success']) {
    $(".user_list table").append("<tr><td><div class=\"checkbox\"></div></td><td class=\"login_block\"><img src=\"images/no_avatar.png\" alt=\"" + data['login'] + "\"><span>" + data['login'] + "</span></td><td><span class=\"email\">" + data['email'] + "</span></td><td><span class=\"reg_date\">" + data['reg_date'] + "</span></td><td><span class=\"status\">" + data['status'] + "</span></td><td><span class=\"department\">" + data['department'] + "</span></td></tr>");
    $(".checkbox").last().click(selectBox);
  };
  $("#new_user_form").css("display", "auto");
}

function removeUser(event) {
  $.post(
    "remove_user.php",
    { "name": getSelectedLogins() },
    onRemoveSuccess
  );
}

function getSelectedLogins() {
  selectedSpans = $(".checkbox.selected").parent().parent().find(".login_block span").toArray();
  for (var i = 0; i < selectedSpans.length; i++) {
    selectedSpans[i] = selectedSpans[i].innerHTML;
  }
  return selectedSpans;
}
function onRemoveSuccess() {
  $(".checkbox.selected").parent().parent().remove();
  $("#check_all").removeClass("selected");
}