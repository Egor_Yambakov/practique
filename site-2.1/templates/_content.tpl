<div class="action_buttons">
  <div id="add_user">
    <span>New user</span>
  </div>
  <div id="remove_user">
    <span>Remove</span>
  </div>
</div>
<div class="background_container" id="new_user_form">
  <div class="form_container">
    <div class="close_form">
    </div>
    <h2>Новый пользователь</h2>
    <form method="POST" action="">
      <label>Имя<input placeholder="name" type="text" name="name" autofocus></label>
      <label>Пароль<input placeholder="password" type="text" name="password"></label>
      <label>Отделение<input placeholder="department" type="text" name="department"></label>
      <label>Должность<input placeholder="status" type="text" name="status"></label>
      <label>Номер телефона<input placeholder="phone" type="text" name="phone"></label>
      <label>Email<input placeholder="email" type="text" name="email"></label>
      <input type="submit" value="Добавить">
    </form>
  </div>
</div>  
<div class="user_list">
  <table>
    <tr>
      <th><div class="checkbox_all" id="check_all"></div></th>
      <th class="login_block"><span>Login</span></th>
      <th><span class="email">Email</span></th>
      <th><span class="reg_date">Registered</span></th>
      <th><span class="status">Status</span></th>
      <th><span class="department">Department</span></th>
    </tr>
    {% for row in list %}
    <tr>
      <td><div class="checkbox"></div></td>
      <td class="login_block">
        <img src="images/no_avatar.png" alt="{{row.login}}">
        <span>{{row.login}}</span>
      </td>
      <td><span class="email">{{row.email}}</span></td>
      <td><span class="reg_date">{{row.reg_date}}</span></td>
      <td><span class="status">{{row.status}}</span></td>
      <td><span class="department">{{row.department}}</span></td>
    </tr>
    {% endfor %}
  </table>
</div>