<?php

/* _content.tpl */
class __TwigTemplate_cffc48a4a71b11e9b0932dcc161028bff952b1f30b1161024f8ba7fcd5f1b838 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"action_buttons\">
  <div id=\"add_user\">
    <span>New user</span>
  </div>
  <div id=\"remove_user\">
    <span>Remove</span>
  </div>
</div>
<div class=\"background_container\" id=\"new_user_form\">
  <div class=\"form_container\">
    <div class=\"close_form\">
    </div>
    <h2>Новый пользователь</h2>
    <form method=\"POST\" action=\"\">
      <label>Имя<input placeholder=\"name\" type=\"text\" name=\"name\" autofocus></label>
      <label>Пароль<input placeholder=\"password\" type=\"text\" name=\"password\"></label>
      <label>Отделение<input placeholder=\"department\" type=\"text\" name=\"department\"></label>
      <label>Должность<input placeholder=\"status\" type=\"text\" name=\"status\"></label>
      <label>Номер телефона<input placeholder=\"phone\" type=\"text\" name=\"phone\"></label>
      <label>Email<input placeholder=\"email\" type=\"text\" name=\"email\"></label>
      <input type=\"submit\" value=\"Добавить\">
    </form>
  </div>
</div>  
<div class=\"user_list\">
  <table>
    <tr>
      <th><div class=\"checkbox_all\" id=\"check_all\"></div></th>
      <th class=\"login_block\"><span>Login</span></th>
      <th><span class=\"email\">Email</span></th>
      <th><span class=\"reg_date\">Registered</span></th>
      <th><span class=\"status\">Status</span></th>
      <th><span class=\"department\">Department</span></th>
    </tr>
    ";
        // line 35
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["list"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
            // line 36
            echo "    <tr>
      <td><div class=\"checkbox\"></div></td>
      <td class=\"login_block\">
        <img src=\"images/no_avatar.png\" alt=\"";
            // line 39
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "login", array()), "html", null, true);
            echo "\">
        <span>";
            // line 40
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "login", array()), "html", null, true);
            echo "</span>
      </td>
      <td><span class=\"email\">";
            // line 42
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "email", array()), "html", null, true);
            echo "</span></td>
      <td><span class=\"reg_date\">";
            // line 43
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "reg_date", array()), "html", null, true);
            echo "</span></td>
      <td><span class=\"status\">";
            // line 44
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "status", array()), "html", null, true);
            echo "</span></td>
      <td><span class=\"department\">";
            // line 45
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "department", array()), "html", null, true);
            echo "</span></td>
    </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo "  </table>
</div>";
    }

    public function getTemplateName()
    {
        return "_content.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 48,  89 => 45,  85 => 44,  81 => 43,  77 => 42,  72 => 40,  68 => 39,  63 => 36,  59 => 35,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_content.tpl", "D:\\Files\\doc\\BEP\\sites\\site-2.1\\templates\\_content.tpl");
    }
}
