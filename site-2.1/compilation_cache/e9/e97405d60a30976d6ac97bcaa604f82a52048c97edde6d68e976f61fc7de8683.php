<?php

/* _pasport.html */
class __TwigTemplate_bcf7ebb427285176efeb58b6537a3131dc4d784542169782a813c86c5ec04c32 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("_content.html", "_pasport.html", 1);
        $this->blocks = array(
            'content_bar' => array($this, 'block_content_bar'),
            'page_foto' => array($this, 'block_page_foto'),
            'secondary_info' => array($this, 'block_secondary_info'),
            'page_info' => array($this, 'block_page_info'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_content.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_content_bar($context, array $blocks = array())
    {
        // line 5
        echo "  <div class=\"side_column\">
    <div class=\"page_block\">
      ";
        // line 7
        $this->displayBlock('page_foto', $context, $blocks);
        // line 9
        echo "    </div>

    <div class=\"page_block secondary_info\">
      ";
        // line 12
        $this->displayBlock('secondary_info', $context, $blocks);
        // line 14
        echo "    </div>
  </div>

  <div class=\"main_column page_block\">
    ";
        // line 18
        $this->displayBlock('page_info', $context, $blocks);
        // line 20
        echo "  </div>
";
    }

    // line 7
    public function block_page_foto($context, array $blocks = array())
    {
        // line 8
        echo "      ";
    }

    // line 12
    public function block_secondary_info($context, array $blocks = array())
    {
        // line 13
        echo "      ";
    }

    // line 18
    public function block_page_info($context, array $blocks = array())
    {
        // line 19
        echo "    ";
    }

    public function getTemplateName()
    {
        return "_pasport.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 19,  78 => 18,  74 => 13,  71 => 12,  67 => 8,  64 => 7,  59 => 20,  57 => 18,  51 => 14,  49 => 12,  44 => 9,  42 => 7,  38 => 5,  35 => 4,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_pasport.html", "D:\\Files\\doc\\BEP\\sites\\site-1.2\\templates\\_pasport.html");
    }
}
