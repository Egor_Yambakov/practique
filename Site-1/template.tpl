<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="style.css">
  <title>{PAGE_NAME}</title>
</head>
<body>
  <div class="all_content">
    <header class="top_container">
      <div class="top_bar">
        <div class="brand">
          <div class="main_logo">
            <a href="#">
              <img src="images/ispring-logo-bold.png" alt="ispring-logo">
            </a>
          </div>
          <div class="main_tittle">   
            <span>Заголовок</span>
          </div>
        </div>
        <form class="search_bar" method="get" action="search.php">
          <input type="text" name="search" placeholder="Поиск" class="seatch_input">
        </form>
        <div class="login_btn">
          <a href="#" class="login_link">Войти</a>
        </div>
        <div class="clear_all"></div>
      </div>
    </header>
    <main class="main_content">
      <div class="side_bar">
        <ul class="side_list">
          <li><a href="#">Мой профиль</a></li>
          <li><a href="#">Библиотека</a></li>
          <li><a href="#">Заказ еды</a></li>
        </ul>
      </div>
      <div class="content_bar">
        <div class="side_column">
          <div class="page_block">
            <div class="page_foto">
              <img src="images/no_avatar.png" alt="avatar">
            </div>
            <div class="action">
              <a href="#">Отправить сообщение</a>
            </div>
          </div>
          <div class="page_block"> 
            <div class="secondary_info">
              <h3>Навыки</h3>
              <ul>
                {SKILLS}
              </ul>
            </div>
            <div class="secondary_info">
              <h3>Интересы</h3>
              <ul>
                {HOBBYS}
              </ul>
            </div>
          </div>
        </div>
        <div class="main_column page_block">
          <h2 class="page_name red_border">{PAGE_NAME}</h2>
          <h4 class="info_header">Работа</h4>
          <ul class="record">
            <li class="department">
              <span class="field">Отделение: </span>
              <span class="note">{DEPARTMENT}</span>
            </li>
            <li class="post">
              <span class="field">Должность: </span>
              <span class="note">{POST}</span>
            </li>
          </ul>
          <h4 class="info_header">Контакты</h4>
          <ul class="record">
            <li class="phone">
              <span class="field">Телефон: </span>
              <span class="note">{PHONE_NUMBER}</span>
            </li>
            <li class="mail">
              <span class="field">Почта: </span>
              <a href="mailto:{EMAIL}">{EMAIL}</a>
            </li>
          </ul>
          <h4 class="info_header">Чем занимается в компании</h4>
          <ul class="record">
            <li class="field">
              <p>
                {DUTIES_TEXT}
              </p>
            </li>
          </ul>
        </div>
      </div>
    </main>
    <footer></footer>
  </div>
</body>
</html>