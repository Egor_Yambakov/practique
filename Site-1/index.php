<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Form</title>
  <style>
    form
    {
        display: flex;
        flex-direction: column;
    }
    form input, form textarea
    {
        margin-bottom: 20px;
        width: min-content;
    }
  </style>
</head>
<body>
  <form method="POST" action="compile.php">
    <input placeholder="name" type="text" name="name">
    <input placeholder="department" type="text" name="department">
    <input placeholder="post" type="text" name="post">
    <input placeholder="phone" type="text" name="phone">
    <input placeholder="email" type="text" name="email">
    <textarea placeholder="duties" name="duties_text"></textarea>
    <textarea placeholder="skills" name="skills"></textarea>
    <textarea placeholder="hobbys" name="hobbys"></textarea>
    <input type="submit" value="send">
  </form>
</body>
</html>