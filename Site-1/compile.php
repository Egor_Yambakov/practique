<?php
    require('template.php');
    $parse->get_tpl('template.tpl');
    $parse->set_tpl('{PAGE_NAME}', $_POST['name']);
    $parse->set_tpl('{DEPARTMENT}', $_POST['department']);
    $parse->set_tpl('{POST}', $_POST['post']);
    $parse->set_tpl('{PHONE_NUMBER}', $_POST['phone']);
    $parse->set_tpl('{EMAIL}', $_POST['email']);
    $parse->set_tpl('{DUTIES_TEXT}', $_POST['duties_text']);
    $parse->set_tpl('{SKILLS}', formatList($_POST["skills"]));
    $parse->set_tpl('{HOBBYS}', formatList($_POST["hobbys"]));

    $parse->tpl_parse();
    print $parse->template;

    function formatList($list)
    {
        $newList = '';
        $list_arr = preg_split("/s*\,s*/", $list);
        for ($i=0; $i < count($list_arr); $i++)
        { 
            $newList .= "<li>" . $list_arr[$i] . "</li>";
        };
        return $newList;
    }
?>