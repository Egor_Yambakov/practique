<?php

/* _side_info.html */
class __TwigTemplate_26e6bcdd8695f4b5badd92810eea124b83846efc14afca3442e1eed5c08fbb1d extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("_pasport.html", "_side_info.html", 1);
        $this->blocks = array(
            'secondary_info' => array($this, 'block_secondary_info'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_pasport.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_secondary_info($context, array $blocks = array())
    {
        // line 4
        echo "<div>
  <h3>Навыки</h3>
  <ul>
    ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["skills"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["skill"]) {
            // line 8
            echo "    <li>";
            // line 9
            echo twig_escape_filter($this->env, $context["skill"], "html", null, true);
            // line 10
            echo "</li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['skill'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "  </ul>
</div>
<div>
  <h3>Интересы</h3>
  <ul>
    ";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["hobbys"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["hobby"]) {
            // line 18
            echo "    <li>";
            // line 19
            echo twig_escape_filter($this->env, $context["hobby"], "html", null, true);
            // line 20
            echo "</li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['hobby'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "  </ul>
</div>
";
    }

    public function getTemplateName()
    {
        return "_side_info.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 22,  70 => 20,  68 => 19,  66 => 18,  62 => 17,  55 => 12,  48 => 10,  46 => 9,  44 => 8,  40 => 7,  35 => 4,  32 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_side_info.html", "D:\\Files\\doc\\BEP\\sites\\site-1.2\\templates\\_side_info.html");
    }
}
