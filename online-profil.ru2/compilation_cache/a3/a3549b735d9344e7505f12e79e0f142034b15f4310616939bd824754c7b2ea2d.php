<?php

/* _login.tpl */
class __TwigTemplate_1e3964b6dd4e31b719c66424c4fbe0b546e42a1cc105436c8ba9566aa9c3d654 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"login_form_container\">
  <h2>Вход в систему</h2>
  <span class=\"err_text\">";
        // line 3
        echo twig_escape_filter($this->env, ($context["err_text"] ?? null), "html", null, true);
        echo "</span>
  <form id=\"login_form\" method=\"POST\">
    <label>Логин<input type=\"text\" name=\"login\"></label>
    <label>Пароль<input type=\"password\" name=\"password\"></label>
    <input type=\"submit\" name=\"submit\" value=\"Войти\">
  </form>
</div>";
    }

    public function getTemplateName()
    {
        return "_login.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_login.tpl", "D:\\Files\\doc\\BEP\\sites\\online-profil.ru\\templates\\_login.tpl");
    }
}
