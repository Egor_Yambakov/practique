<?php

/* _profil_main.tpl */
class __TwigTemplate_1dbdf5fb425adcbc91c40d45ede7ec4dc32221d8d6909fbba36c23c082970de5 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"profil_container box_shadow\">
  <div class=\"side_block\">
    <div class=\"profil_side\">
      <div class=\"profil_photo\" style=\"background-image:url(";
        // line 4
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "photo", array()), "html", null, true);
        echo ")\"></div>
      ";
        // line 5
        if ((twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "id", array()) == twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", array()))) {
            // line 6
            echo "        <a class=\"button reversed\" id=\"edit_btn\">Редактировать</a>
      ";
        } else {
            // line 8
            echo "        <a class=\"button\" href=\"mailto:";
            echo twig_escape_filter($this->env, (($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5 = twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "email", array())) && is_array($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5) || $__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5 instanceof ArrayAccess ? ($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5[0] ?? null) : null), "html", null, true);
            echo "\" target=\"_blank\">Написать собщение</a>
      ";
        }
        // line 10
        echo "      <div class=\"pasport_info skills tag_set\">
        <h3 class=\"tittle\">Навыки</h3>
        <ul class=\"skills_list\">
          ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "skills", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["skill"]) {
            echo "  
            <li><a href=\"full_search.php?skills_query=";
            // line 14
            echo twig_escape_filter($this->env, twig_urlencode_filter($context["skill"]), "html", null, true);
            echo "\">#";
            echo twig_escape_filter($this->env, $context["skill"], "html", null, true);
            echo "</a></li>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['skill'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "        </ul>
        <a class=\"show_btn\" id=\"show_skills\">показать все</a>
      </div>
      <div class=\"pasport_info skills tag_set\">
        <h3 class=\"tittle\">Интересы</h3>
        <ul class=\"skills_list\">
          ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "interests", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["interest"]) {
            echo "  
            <li><a href=\"full_search.php?interests_query=";
            // line 23
            echo twig_escape_filter($this->env, twig_urlencode_filter($context["interest"]), "html", null, true);
            echo "\">#";
            echo twig_escape_filter($this->env, $context["interest"], "html", null, true);
            echo "</a></li>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['interest'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "        </ul>
        <a class=\"show_btn\" id=\"show_interest\">показать все</a>
      </div>
    </div>
  </div>
  <div class=\"main_block\">
    <div id=\"profil_information\">
      <div class=\"pasport_block\">
        <div class=\"pasport_info\">
          <h3 class=\"tittle\">";
        // line 34
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "name", array()), "html", null, true);
        echo "</h3>
          <span class=\"status\">";
        // line 35
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "job_tittle", array()), "html", null, true);
        echo " в ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "department", array()), "html", null, true);
        echo "</span>
          <table>
            <tr>
              <td><span>Телефон</span></td>
              <td>
                ";
        // line 40
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "phone", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["phone"]) {
            // line 41
            echo "                <span>";
            echo twig_escape_filter($this->env, $context["phone"], "html", null, true);
            echo "</span><br>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['phone'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "              </td>
            </tr>
            <tr>
              <td><span>Кабинет</span></td>
              <td><span>";
        // line 47
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "room", array()), "html", null, true);
        echo "</span></td>
            </tr>
            <tr>
              <td><span>Email</span></td>
              <td>
                ";
        // line 52
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "email", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["email"]) {
            // line 53
            echo "                <a href=\"mailto:";
            echo twig_escape_filter($this->env, $context["email"], "html", null, true);
            echo "\" target=\"_blank\">";
            echo twig_escape_filter($this->env, $context["email"], "html", null, true);
            echo "</a><br>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['email'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "              </td>
            </tr>
            <tr>
              <td><span>Дата рождения</span></td>
              <td><span>";
        // line 59
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "birth", array()), "html", null, true);
        echo "</span></td>
            </tr>
          </table>
        </div>
      </div>
      <div class=\"pasport_info work_descr\">
        <h3 class=\"tittle underline\">Описание деятельности</h3>
        <p>";
        // line 66
        echo nl2br(twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "work_descr", array()), "html", null, true));
        echo "</p>
        <a class=\"show_btn\" id=\"show_description\">показать все</a>
      </div>
    </div>

    ";
        // line 71
        if ((twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "id", array()) == twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", array()))) {
            // line 72
            echo "    <form class=\"edit_profil\">
      <div class=\"pasport_info\">
        <h3 class=\"tittle\">Редактирование профиля</h3>
        <span class=\"status\"></span>
        <label>
          <span>Отдел</span>
          <select name=\"department\">
          ";
            // line 79
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["edit"] ?? null), "departments", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["dep"]) {
                // line 80
                echo "            <option value=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["dep"], "name", array()), "html", null, true);
                echo "\" ";
                if ((twig_get_attribute($this->env, $this->source, $context["dep"], "name", array()) == twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "department", array()))) {
                    echo "selected";
                }
                echo ">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["dep"], "name", array()), "html", null, true);
                echo "</option>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dep'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 82
            echo "          </select>
        </label>
        <label>
          <span>Должность</span>
          <select name=\"job_tittle\">
          ";
            // line 87
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["edit"] ?? null), "job_tittles", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["job"]) {
                // line 88
                echo "            <option value=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["job"], "name", array()), "html", null, true);
                echo "\" ";
                if ((twig_get_attribute($this->env, $this->source, $context["job"], "name", array()) == twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "job_tittle", array()))) {
                    echo "selected";
                }
                echo ">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["job"], "name", array()), "html", null, true);
                echo "</option>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['job'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 90
            echo "          </select>
        </label>
        <label>
          <span>Телефон</span>
          <input name=\"phone\" type=\"text\" value=\"";
            // line 94
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "phone", array()));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["phone"]) {
                echo twig_escape_filter($this->env, $context["phone"], "html", null, true);
                if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "last", array())) {
                    echo ", ";
                }
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['phone'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo "\">
        </label>
        <label>
          <span>Кабинет</span>
          <select name=\"room\">
          ";
            // line 99
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["edit"] ?? null), "rooms", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["room"]) {
                // line 100
                echo "            <option value=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["room"], "number", array()), "html", null, true);
                echo "\" ";
                if ((twig_get_attribute($this->env, $this->source, $context["room"], "number", array()) == twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "room", array()))) {
                    echo "selected";
                }
                echo ">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["room"], "number", array()), "html", null, true);
                echo "</option>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['room'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 102
            echo "          </select>
        </label>
        <label>
          <span>Email</span>
          <input name=\"email\" type=\"text\" value=\"";
            // line 106
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "email", array()));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["email"]) {
                echo twig_escape_filter($this->env, $context["email"], "html", null, true);
                if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "last", array())) {
                    echo ", ";
                }
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['email'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo "\">
        </label>
        <label class=\"textarea\">
          <h3>Навыки</h3>
          <textarea id=\"edit_skills\" name=\"skills\">";
            // line 110
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "skills", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["skill"]) {
                echo "#";
                echo twig_escape_filter($this->env, $context["skill"], "html", null, true);
                echo "   ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['skill'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo "</textarea>
        </label>
        <label class=\"textarea\">
          <h3>Интересы</h3>
          <textarea id=\"edit_interests\" name=\"interests\">";
            // line 114
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "interests", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["interest"]) {
                echo "#";
                echo twig_escape_filter($this->env, $context["interest"], "html", null, true);
                echo "   ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['interest'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo "</textarea>
        </label>
        <label class=\"textarea\">
          <h3>Описание деятельности</h3>
          <textarea name=\"work_descr\">";
            // line 118
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "work_descr", array()));
            echo "</textarea>
        </label>
        <a id=\"save_changes\" class=\"button\">Сохранить изменения</a>
      </div>
    </form>
    ";
        }
        // line 124
        echo "  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "_profil_main.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  385 => 124,  376 => 118,  360 => 114,  344 => 110,  306 => 106,  300 => 102,  285 => 100,  281 => 99,  242 => 94,  236 => 90,  221 => 88,  217 => 87,  210 => 82,  195 => 80,  191 => 79,  182 => 72,  180 => 71,  172 => 66,  162 => 59,  156 => 55,  145 => 53,  141 => 52,  133 => 47,  127 => 43,  118 => 41,  114 => 40,  104 => 35,  100 => 34,  89 => 25,  79 => 23,  73 => 22,  65 => 16,  55 => 14,  49 => 13,  44 => 10,  38 => 8,  34 => 6,  32 => 5,  28 => 4,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_profil_main.tpl", "D:\\Files\\doc\\BEP\\sites\\online-profil.ru\\template\\_profil_main.tpl");
    }
}
