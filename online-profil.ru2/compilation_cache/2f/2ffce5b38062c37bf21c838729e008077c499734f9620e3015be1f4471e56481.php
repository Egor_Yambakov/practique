<?php

/* _pasports_list.tpl */
class __TwigTemplate_e720649ca50ebca0c6411d921b07cc907d57e15b29e40d95ac7dae4cd0c771df extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<ul class=\"pasports_list\">
  <li>
    <div class=\"page_block\">
      <div class=\"photo_block\">
        <img src=\"./photos/no_avatar.png\" alt=\"avatar\">
      </div>
    </div>  
    <div class=\"page_block pass_block\">
      <div class=\"pasport\">
        <div class=\"main_info\">
          <h2 class=\"name\">Гейст Константин</h2>
          <span class=\"status\">Программист в Отдел разработки ПО</span>
        </div>
        <div class=\"side_info\">
          <table>
            <tr>
              <td class=\"tittle\">Дата рождения</td>
              <td class=\"value\">15 декабря 1989г.</td>
            </tr>
            <tr>
              <td class=\"tittle\">Телефон</td>
              <td class=\"value\">+7(926) 212-85-06</td>
            </tr>
            <tr>
              <td class=\"tittle\">Кабинет</td>
              <td class=\"value\">345</td>
            </tr>
            <tr>
              <td class=\"tittle\">Email</td>
              <td class=\"value\">konstantin.geyst@ispringsolutions.com</td>
            </tr>
            <tr>
              <td class=\"tittle\">Рабочий график</td>
              <td class=\"value\">пн-пт 10-19</td>
            </tr>
          </table>
        </div>
      </div>
    </div> 
  </li> 
</ul>";
    }

    public function getTemplateName()
    {
        return "_pasports_list.tpl";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_pasports_list.tpl", "D:\\Files\\doc\\BEP\\sites\\site-3.1\\templates\\_pasports_list.tpl");
    }
}
