<?php

/* _logout_header.tpl */
class __TwigTemplate_3e0a82569b146b82a0060e387fcbb2501797e6d70f1674574fefa1c91c4fdb4b extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"top_bar\">
  <div class=\"brand\">
    <div class=\"main_logo\">
      <a href=\"#\">
        <img src=\"images/ispring-logo-bold.png\" alt=\"ispring-logo\">
      </a>
    </div>
    <div class=\"main_tittle\">   
      <span>Онлайн-профиль</span>
    </div>
  </div>
  <div class=\"clear_all\"></div>
</div>";
    }

    public function getTemplateName()
    {
        return "_logout_header.tpl";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_logout_header.tpl", "D:\\Files\\doc\\BEP\\sites\\site-3.1\\templates\\_logout_header.tpl");
    }
}
