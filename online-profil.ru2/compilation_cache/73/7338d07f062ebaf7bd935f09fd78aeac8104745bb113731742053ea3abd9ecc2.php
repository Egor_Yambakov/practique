<?php

/* _base_child.tpl */
class __TwigTemplate_3f69e7f382e2744846237f2e3fee42693c993d4724bc20e17923b16b140f132d extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html", "_base_child.tpl", 1);
        $this->blocks = array(
            'top_container' => array($this, 'block_top_container'),
            'main_content' => array($this, 'block_main_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_top_container($context, array $blocks = array())
    {
        // line 4
        echo "  ";
        $this->loadTemplate("_logout_header.tpl", "_base_child.tpl", 4)->display($context);
    }

    // line 7
    public function block_main_content($context, array $blocks = array())
    {
        // line 8
        echo "  ";
        $this->loadTemplate("_login.tpl", "_base_child.tpl", 8)->display($context);
    }

    public function getTemplateName()
    {
        return "_base_child.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  44 => 8,  41 => 7,  36 => 4,  33 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_base_child.tpl", "D:\\Files\\doc\\BEP\\sites\\site-3.1\\templates\\_base_child.tpl");
    }
}
