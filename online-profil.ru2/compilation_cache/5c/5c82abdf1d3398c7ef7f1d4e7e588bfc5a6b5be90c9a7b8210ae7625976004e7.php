<?php

/* _header.html */
class __TwigTemplate_4a0804b1d0bec0c00bb65e8eac29c25366af5b8b6d9b890a1be99c6f08000925 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html", "_header.html", 1);
        $this->blocks = array(
            'top_container' => array($this, 'block_top_container'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_top_container($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"top_bar\">
      <div class=\"brand\">
        <div class=\"main_logo\">
          <a href=\"#\">
            <img src=\"templates/images/ispring-logo-bold.png\" alt=\"ispring-logo\">
          </a>
        </div>
        <div class=\"main_tittle\">   
          <span>";
        // line 12
        echo twig_escape_filter($this->env, ($context["tittle"] ?? null), "html", null, true);
        echo "</span>
        </div>
      </div>
      <form class=\"search_bar\" method=\"get\" action=\"search.php\">
        <input type=\"text\" name=\"search\" placeholder=\"Поиск\" class=\"seatch_input\">
      </form>
      <div class=\"login_btn\">
        <a href=\"#\" class=\"login_link\">Войти</a>
      </div>
      <div class=\"clear_all\"></div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "_header.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 12,  35 => 4,  32 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_header.html", "D:\\Files\\doc\\BEP\\sites\\site-1.2\\templates\\_header.html");
    }
}
