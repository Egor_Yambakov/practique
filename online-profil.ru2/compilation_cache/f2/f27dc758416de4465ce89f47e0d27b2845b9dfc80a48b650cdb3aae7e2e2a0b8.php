<?php

/* _search_filter.tpl */
class __TwigTemplate_a471ddb59bf0dacb773bc8944a6c1f5a04d336c81edfca3fc1eef0d89bdfa5fd extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"filter_list\">
  <div class=\"department_block\">
    <h3 class=\"tittle\">Отделы</h3>
    <ul>
      ";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["filter"] ?? null), "department", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["dep"]) {
            // line 6
            echo "        <li><a href=\"employees.php?dep_query=";
            echo twig_escape_filter($this->env, twig_urlencode_filter($context["dep"]), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $context["dep"], "html", null, true);
            echo "</a></li>
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dep'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 8
        echo "    </ul>
  </div>
  <div class=\"filter_set\">
    <h3 class=\"tittle\">Навыки</h3>
    <ul>
      ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["filter"] ?? null), "skills", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["skill"]) {
            // line 14
            echo "        <li><a href=\"full_search.php?skills_query=";
            echo twig_escape_filter($this->env, twig_urlencode_filter(twig_get_attribute($this->env, $this->source, $context["skill"], "name", array())), "html", null, true);
            echo "\">#";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["skill"], "name", array()), "html", null, true);
            echo " (";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["skill"], "count", array()), "html", null, true);
            echo ")</a></li>
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['skill'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "      <li id=\"unhideExtraSkills\"><a href=\"#\">···</a></li>
    </ul>
  </div>
  <div class=\"filter_set\">
    <h3 class=\"tittle\">Интересы</h3>
    <ul>
      ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["filter"] ?? null), "interests", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["interest"]) {
            // line 23
            echo "        <li><a href=\"full_search.php?interests_query=";
            echo twig_escape_filter($this->env, twig_urlencode_filter(twig_get_attribute($this->env, $this->source, $context["interest"], "name", array())), "html", null, true);
            echo "\">#";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["interest"], "name", array()), "html", null, true);
            echo " (";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["interest"], "count", array()), "html", null, true);
            echo ")</a></li>
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['interest'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "      <li id=\"unhideExtraInterests\"><a href=\"#\">···</a></li>
    </ul>
  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "_search_filter.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 25,  80 => 23,  76 => 22,  68 => 16,  55 => 14,  51 => 13,  44 => 8,  33 => 6,  29 => 5,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_search_filter.tpl", "D:\\Files\\doc\\BEP\\sites\\site-3.1\\templates\\_search_filter.tpl");
    }
}
