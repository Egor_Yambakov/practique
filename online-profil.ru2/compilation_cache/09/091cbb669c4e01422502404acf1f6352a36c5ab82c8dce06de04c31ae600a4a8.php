<?php

/* _profil_base.tpl */
class __TwigTemplate_3b82985be2112078055995b8a4c40cfd23d9a92bcbd892ce2e8eff2d6102c41a extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html", "_profil_base.tpl", 1);
        $this->blocks = array(
            'top_container' => array($this, 'block_top_container'),
            'main_content' => array($this, 'block_main_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_top_container($context, array $blocks = array())
    {
        // line 4
        echo "  ";
        $this->loadTemplate("_header.tpl", "_profil_base.tpl", 4)->display($context);
    }

    // line 7
    public function block_main_content($context, array $blocks = array())
    {
        // line 8
        echo "<div class=\"main_content\">
  <div class=\"side_bar\">
    ";
        // line 10
        $this->loadTemplate("_side_bar.tpl", "_profil_base.tpl", 10)->display($context);
        // line 11
        echo "  </div>

  <div class=\"content_bar\">
    ";
        // line 14
        $this->loadTemplate("_profil_main.tpl", "_profil_base.tpl", 14)->display($context);
        // line 15
        echo "  </div>
</div>
<script src=\"/js/jquery.js\" type=\"text/javascript\"></script>
<script src=\"/js/header.js\" type=\"text/javascript\"></script>
<script src=\"/js/profil.js\" type=\"text/javascript\"></script>
";
    }

    public function getTemplateName()
    {
        return "_profil_base.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 15,  55 => 14,  50 => 11,  48 => 10,  44 => 8,  41 => 7,  36 => 4,  33 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_profil_base.tpl", "D:\\Files\\doc\\BEP\\sites\\online-profil.ru\\template\\_profil_base.tpl");
    }
}
