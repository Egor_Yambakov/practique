<?php

/* _profil_container.tpl */
class __TwigTemplate_e9a318aebe7a6b4f0b1f79ae0950e4cdfb5bf7391e3b97fb9914faa75eaa6b4d extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"main_content\">
  <div class=\"side_bar\">
    ";
        // line 3
        $this->loadTemplate("_side_bar.tpl", "_profil_container.tpl", 3)->display($context);
        // line 4
        echo "  </div>

  <div class=\"content_bar\">
    ";
        // line 7
        $this->loadTemplate("_profil_main.tpl", "_profil_container.tpl", 7)->display($context);
        // line 8
        echo "  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "_profil_container.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  36 => 8,  34 => 7,  29 => 4,  27 => 3,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_profil_container.tpl", "D:\\Files\\doc\\BEP\\sites\\site-3.1\\templates\\_profil_container.tpl");
    }
}
