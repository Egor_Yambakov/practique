<?php

/* _login_header.tpl */
class __TwigTemplate_063341231988f5b7522e2424988dbd974720f3146e941c9b8acedb119eb16217 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"top_bar\">
  <div class=\"brand\">
    <div class=\"main_logo\">
      <a href=\"#\">
        <img src=\"images/ispring-logo-bold.png\" alt=\"ispring-logo\">
      </a>
    </div>
    <div class=\"main_tittle\">   
      <span>Онлайн-профиль</span>
    </div>
  </div>
  <form class=\"search_bar\" method=\"GET\" action=\"search.php\">
    <input type=\"text\" name=\"query\" id=\"search_box\" autocomplete=\"off\">
    <div id=\"search_advice\"></div>
  </form>
  <div class=\"login_btn\">
    <span>";
        // line 17
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "name", array()), "html", null, true);
        echo "</span>
    <img src=\"";
        // line 18
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "photo", array()), "html", null, true);
        echo "\" alt=\"\">
    <div class=\"dropdown_menu\">
      <a href=\"profil.php?id=";
        // line 20
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", array()), "html", null, true);
        echo "\">Мой профиль</a>
      <a href=\"logout.php\">Выйти</a>
    </div>
  </div>
  <div class=\"clear_all\"></div>
</div>";
    }

    public function getTemplateName()
    {
        return "_login_header.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 20,  45 => 18,  41 => 17,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_login_header.tpl", "D:\\Files\\doc\\BEP\\sites\\site-3.1\\templates\\_login_header.tpl");
    }
}
