<?
    function getLoginPage($page_name, $err_text)
    {
        $pageData = array('err_text' => $err_text);
        return getPage($page_name, $pageData);
    }

    function getNewsPage($page_name, $data)
    {
        return getPage($page_name, $data);
    }

    function getProfilPage($page_name, $data)
    {
        return getPage($page_name, $data);
    }

    function getEmployeesPage($page_name, $data)
    {
        return getPage($page_name, $data);
    }

    function getSearchPage($page_name, $data)
    {
        return getPage($page_name, $data);
    }