<?
    function getNewsPageData($user_id)
    {
        $data = array
                (
                    'list'   => getNewsData(),
                    'user'   => getUserDataById($user_id),
                    'filter' => getFilterData()
                );
        return $data;
    }

    function getProfilPageData($profil_id, $user_id)
    {
        $data = array
                (
                    'profil' => getProfilById($profil_id),
                    'user'   => getUserDataById($user_id),
                    'edit'   => getEditData()
                );
        return $data;
    }

    function getEmployeesPageData($dep_query, $user_id)
    {
        $list_data = getEmployeesData($dep_query);
        $data = array
                (
                    'employees'      => $list_data['employees'],
                    'query'     => $list_data['dep_query'],
                    'user'      => getUserDataById($user_id),
                    'filter'    => getFilterData()
                );
        return $data;
    }

    function getSearchPageData($query_arr, $user_id)
    {
        $list_data = getSearchData($query_arr);
        $data = array
                (
                    'list'      => $list_data['data'],
                    'query'     => $list_data['query'],
                    'user'      => getUserDataById($user_id),
                    'filter'    => getFilterData()
                );
        return $data;
    }

    function checkLoginAcces($postForm, $access_location)
    {
        $err_text = '';
        if (isset($postForm['submit']))
        {
            $user_data = getLoginData($postForm['login']);
            if (!empty($user_data))
            {
                if ($user_data['user_password'] === md5($postForm['password']))
                {
                    setcookie('id', $user_data['user_id']);
                    header("location: $access_location");
                    exit;
                }
                else
                    $err_text = 'Пожалуйста, проверьте правильность написания логина и пароля';
            }
            else
                $err_text = 'Пожалуйста, проверьте правильность написания логина и пароля';
        };   
        return $err_text;     
    }

    function checkDataExist($data, $access_location)
    {
        if (isset($data) && !empty($data) && is_numeric($data))
        {
            header("Location: $access_location");
            exit;
        }
    }    

    function checkDataNotExist($data, $err_location)
    {
        if (!isset($data) or empty($data) or !is_numeric($data))
        {
            header("Location: $err_location");
            exit;
        };
    }