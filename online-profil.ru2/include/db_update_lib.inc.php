<?
    function updateDepartment($user_id, $new_info)
    {
        $user_id = getQuery($user_id);
        $new_info = getQuery($new_info);

        $query = "UPDATE `user` 
                  SET `department_id` = (SELECT `id` 
                                         FROM `department` 
                                         WHERE `name` = '$new_info') 
                  WHERE `id` = '$user_id'";
        $result = makeQuery($query);
        if (!$result)
            return "error in department \n";
    }

    function updateEmail($user_id, $new_info)
    {
        $user_id = getQuery($user_id);
        $new_info = getQuery($new_info);

        $query = "UPDATE `user` 
                  SET `email` = '$new_info'
                  WHERE `id` = '$user_id'";
        $result = makeQuery($query);
        if (!$result)
            return "error in email \n";
    }

    function updateInterests($user_id, $new_info)
    {
        $user_id = getQuery($user_id);
        $new_info = getQuery($new_info);

        preg_match_all("/#([^\s#]+)/i", $new_info, $matches);
        $interests_arr = $matches[1];

        clearOldInterests($user_id);

        foreach ($interests_arr as $interest) 
        {
            $query = "SELECT `id` FROM `interest` WHERE `name` = '$interest'";
            $result = getDataRow($query);
            if ($result)
            {
                addNewInterestInfo($user_id, $result["id"]);
            }
            else
            {
                $interest_id = addNewInterest($interest);
                addNewInterestInfo($user_id, $interest_id);
            };
        };
    }

    function addNewInterestInfo($user_id, $interest_id)
    {
        $query_check_unique = "SELECT * 
                         FROM `user_interest` 
                         WHERE `user_id` = '$user_id' AND `interest_id` = '$interest_id'";
        if (!countRowData($query_check_unique))
        {

            $query_update = "INSERT INTO `user_interest`
                             (`user_id`, `interest_id`)
                             VALUES
                             ('$user_id', '$interest_id')";
            makeQuery($query_update);
        };
    }

    function addNewInterest($name)
    {
        $reg_time = time();
        $query_add = "INSERT INTO `interest` 
                      (`name`, `reg_date`)
                      VALUES 
                      ('$name', '$reg_time')";
        makeQuery($query_add);

        return getLastInsertedId();
    }

    function clearOldInterests($user_id)
    {      
        $query_delete = "DELETE FROM `user_interest` WHERE `user_id` = '$user_id'";
        makeQuery($query_delete);
        $query_delete = "DELETE FROM `interest` WHERE `id` NOT IN (SELECT `interest_id` FROM `user_interest`)";
        makeQuery($query_delete);
    }

    function updateSkills($user_id, $new_info)
    {
        $user_id = getQuery($user_id);
        $new_info = getQuery($new_info);

        preg_match_all("/#([^\s#]+)/i", $new_info, $matches);
        $skills_arr = $matches[1];

        clearOldSkills($user_id);

        foreach ($skills_arr as $skill) 
        {
            $query = "SELECT `id` FROM `skills` WHERE `name` = '$skill'";
            $result = getDataRow($query);
            if ($result)
            {
                addNewSkillInfo($user_id, $result["id"]);
            }
            else
            {
                $skill_id = addNewSkill($skill);
                addNewSkillInfo($user_id, $skill_id);
            };
        };
    }

    function addNewSkillInfo($user_id, $skill_id)
    {
        $query_check_unique = "SELECT * 
                         FROM `user_skills` 
                         WHERE `user_id` = '$user_id' AND `skill_id` = '$skill_id'";
        if (!countRowData($query_check_unique))
        {

            $query_update = "INSERT INTO `user_skills`
                             (`user_id`, `skill_id`)
                             VALUES
                             ('$user_id', '$skill_id')";
            makeQuery($query_update);
        };
    }

    function addNewSkill($name)
    {
        $reg_time = time();
        $query_add = "INSERT INTO `skills` 
                      (`name`, `reg_date`)
                      VALUES 
                      ('$name', '$reg_time')";
        makeQuery($query_add);

        return getLastInsertedId();
    }

    function clearOldSkills($user_id)
    {      
        $query_delete = "DELETE FROM `user_skills` WHERE `user_id` = '$user_id'";
        makeQuery($query_delete);
        $query_delete = "DELETE FROM `skills` WHERE `id` NOT IN (SELECT `skill_id` FROM `user_skills`)";
        makeQuery($query_delete);
    }

    function updateJob($user_id, $new_info)
    {
        $user_id = getQuery($user_id);
        $new_info = getQuery($new_info);

        $query = "UPDATE `user` 
                  SET `job_tittle_id` = (SELECT `id` 
                                         FROM `job_tittle` 
                                         WHERE `name` = '$new_info') 
                  WHERE `id` = '$user_id'";
        $result = makeQuery($query);
        if (!$result)
            return "error in job_tittle \n";
    }

    function updatePhone($user_id, $new_info)
    {
        $user_id = getQuery($user_id);
        $new_info = getQuery($new_info);

        $query = "UPDATE `user` 
                  SET `phone` = '$new_info'
                  WHERE `id` = '$user_id'";
        $result = makeQuery($query);
        if (!$result)
            return "error in phone \n";
    }

    function updateRoom($user_id, $new_info)
    {
        $user_id = getQuery($user_id);
        $new_info = getQuery($new_info);

        $query = "UPDATE `user` 
                  SET `room_id` = (SELECT `id` 
                                         FROM `room` 
                                         WHERE `number` = '$new_info') 
                  WHERE `id` = '$user_id'";
        $result = makeQuery($query);
        if (!$result)
            return "error in room \n";
    }

    function updateWorkDescr($user_id, $new_info)
    {
        $user_id = getQuery($user_id);
        $new_info = getQuery($new_info);

        $query = "UPDATE `user` 
                  SET `work_descr` = '$new_info'
                  WHERE `id` = '$user_id'";
        $result = makeQuery($query);
        if (!$result)
            return "error in work description \n";
    }