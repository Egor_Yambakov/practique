const NUMBER_OF_CHAR_TO_SEARCH = 1;

$(headerHandler);

function headerHandler()
{
    $(".login_btn").click(triggerProfilMenu);
    $("#search_box").focus(revealAdviceMenu);
    $("html").click(hideMenu);

    $("#search_box").keyup(sendSearchAjax);
    $("#search_box").keydown(handleKey);
}

function triggerProfilMenu()
{
    if ($(".dropdown_menu").hasClass("dropd"))
    {
        $(".dropdown_menu").removeClass("dropd");
    }
    else
    {
        $(".dropdown_menu").addClass("dropd");
    };
}


function handleKey(event)
{
    switch(event.keyCode)
    {
        case 13:
            if ($(".result_name_block:hover").attr("href"))
            {
                event.preventDefault();
                window.location.assign($(".result_name_block:hover").attr("href"));
            };

            if ($(".result_skills a:hover").attr("href"))
            {
                event.preventDefault();
                window.location.assign($(".result_skills a:hover").attr("href"));
            };
            
            if(!$("#search_box").val().length)
                event.preventDefault();
            break;
        default:
            break;
    }
}

function sendSearchAjax()
{
    if($("#search_box").val().length >= NUMBER_OF_CHAR_TO_SEARCH)
    {
        queryFullData();
    }else
    {
        queryNamesData();
    };
}

function queryFullData()
{
    $(".result_page_link").show().attr("href", "full_search.php?query=" + $("#search_box").val());
    $.get("ajax/ajax_full.php", {"query": $("#search_box").val()}, processData);
}

function queryNamesData()
{
    $(".result_page_link, .result_skills").hide();
    $.get("ajax/ajax_name.php", {}, processName);
}

function processData(data)
{   
    data = $.parseJSON(data);
    $("#result_names_list").empty();
    $("#result_skills_list").empty();
    $("#result_interests_list").empty();
    $(".result_skills").hide();
    
    if (data['err'])
    {
        addErrorResult(data['err']);
    }
    else
    {

        addNamesResult(data["names"]);
        addSkillsResult(data["skills"]);
        addInterestsResult(data["interests"]);
    }
}

function processName(data)
{
    data = $.parseJSON(data);

    $("#result_names_list").empty();

    addNamesResult(data["names"]);
}

function addErrorResult(error)
{
    $("<a>", 
    {
        class: "result_name_block",
        append: $("<span>", 
        {
            class: "tittle",
            text: error
        })
    })
    .appendTo("#result_names_list");
}

function addNamesResult(names)
{
    for(var name_id in names) 
    {
        let id = names[name_id]["id"];
        $("<a>", 
        {
            class: "result_name_block",
            href: "profil.php?id=" + id,
            append: $("<span>", 
            {
                class: "result_photo",
                append: $("<img>")
                    .attr("src", names[name_id]["photo"])
            })
            .add($("<span>", 
            {
                class: "result_info",
                append: $("<h4>", 
                {
                    class: "tittle",
                    html: names[name_id]["name"]
                })
                .add($("<span>", 
                {
                    class: "text",
                    html: names[name_id]["job_tittle"]
                }))
            }))
        })
        .appendTo("#result_names_list");
    };
}

function addSkillsResult(skills)
{
    if (skills.length)
    {
        $("#result_skills_list").parent().show();
        for(let skill_id in skills)
        {
            $("<li>", 
            {
                append: $("<a>", {
                    href: "full_search.php?skills_query=" + deleteTags(skills[skill_id]["name"]),
                    html: skills[skill_id]["name"] + ' (' + skills[skill_id]["count"] + ')'
                })
            })
            .appendTo("#result_skills_list");
        };
    };
}

function addInterestsResult(interests)
{
    if (interests.length)
    {
        $("#result_interests_list").parent().show();
        for(let interest_id in interests)
        {
            $("<li>", 
            {   
                append: $("<a>", {
                    href: "full_search.php?interests_query=" + deleteTags(interests[interest_id]["name"]),
                    html: interests[interest_id]["name"] + ' (' + interests[interest_id]["count"] + ')'
                }) 
            })
            .appendTo("#result_interests_list");
        };
    };    
}

function revealAdviceMenu()
{
    $("#search_advice").show();
    if($("#search_box").val().length < NUMBER_OF_CHAR_TO_SEARCH)
    {
        queryNamesData();
    }
}

function hideMenu(event)
{
    if (!$(event.target).is("#search_box"))
        $("#search_advice").hide();
}

function getGetParameterByName(name, url)
{
    if (!url) 
        url = window.location.href;
    url = decodeURIComponent(url);
    name = name.replace(/[\[\]]/g, "\\$&");
    let regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)");
    results = regex.exec(url);
    if (!results) 
        return null;
    if (!results[2]) 
        return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function deleteTags(string)
{
    string = string.replace(/<.*?>/g, '');
    return string;
}