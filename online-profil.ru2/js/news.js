const MAX_RESULT_LIST_LENGHT = 4;

$(newsHandler);

function newsHandler()
{
    hideExtraResults($(".new_users"));
    hideAllResultsBtn($(".show_all_btn"));
}

function hideExtraResults(element)
{
    element.children(":gt(" + (MAX_RESULT_LIST_LENGHT - 1) + ")").hide();
}

function hideAllResultsBtn(element)
{
    if (element.parent().children().length > MAX_RESULT_LIST_LENGHT + 1)
    {
        element.show();
        element.click(function() {
            $(this).parent().children().css("display", "block");
            $(this).hide();
        });
    };
}