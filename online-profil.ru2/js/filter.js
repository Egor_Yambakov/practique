const MAX_FILTER_LIST_LENGTH = 5;

$(filterHandler);

function filterHandler()
{
    hideExtraButton($("#unhideExtraSkills"));
    hideExtraButton($("#unhideExtraInterests"));
    highlightCurrDepart();
}

function highlightCurrDepart()
{
    const query = getGetParameterByName("dep_query");
    $(".department_block ul li").each(function()
    {
        if ($(this).text() == query)
        {
            $(this).addClass("curr_department");
        }
    });
}

function hideExtraButton(element)
{
    if (element.parent().children().length <= MAX_FILTER_LIST_LENGTH + 1)
    {
        element.hide();
    }
    else
    {
        element.click(function() {
            $(this).parent().children().css("display", "inline");
            $(this).hide();
        });
    };
}