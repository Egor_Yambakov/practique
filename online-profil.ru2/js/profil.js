
$(profilHandler);

function profilHandler()
{
    $("#edit_btn").click(triggerEditMenu);
    $("#save_changes").click(sendEditAjax);
    hideShowButton($("#show_skills"));
    hideShowButton($("#show_interest"));
    hideShowButton($("#show_description"));

    checkToEditLink(); 
}

function checkToEditLink()
{
    let isOpen = getGetParameterByName("edit");
    if (isOpen == "")
    {
        triggerEditMenu();
    };
}

function triggerEditMenu()
{
    if ($(".edit_profil").is(":visible"))
    {
        $("#profil_information").show();
        $(".edit_profil").hide();
    }
    else
    {
        $("#profil_information").hide();
        $(".edit_profil").show();
    };
}

function updateInfo()
{
    sendEditAjax();
}

function sendEditAjax()
{
    let data = objectifyForm($("form.edit_profil"));
    data["id"] = getGetParameterByName("id");
    console.log(data);

    $.post
    (
        "/ajax/ajax_update_data.php", 
        data,
        logData
    );
}

function logData(data)
{
    if (data != "")
        alert(data);
    else
    {
        window.location.assign("profil.php");
    };
}

function objectifyForm(form)
{
    const formArray = form.serializeArray();
    let returnArray = {};
    for (let i = 0; i < formArray.length; i++)
    {
      returnArray[formArray[i]['name']] = formArray[i]['value'];
    };
    return returnArray;
}

$.fn.overflown=function()
{
    var e=this[0];
    return e.scrollHeight>e.clientHeight||e.scrollWidth>e.clientWidth;
}

function hideShowButton(element)
{
    if (!element.parent().children("ul, p").overflown())
    {
        element.hide();
    }
    else
    {
        element.click(function() {
            $(this).parent().find("ul, p").css("height", "auto");
            $(this).hide();
        });
    };
}