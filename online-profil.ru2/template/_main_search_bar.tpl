<div class="page_tittle border_bottom">
  <h1>Результаты поиска</h1>
</div>
<form class="main_search border_bottom" id="main_search" method="GET" action="full_search.php">
  <a href="#" class="search_icon" onClick="document.getElementById('main_search').submit();">
    <img src="images/search_icon.svg" alt="">
  </a>
  <input value="{{query}} {% if employees %} ({{employees|length}}) {% endif %}" type="text" name="query" class="main_search_box" id="main_search_box" autocomplete="off">
  <a class="clear_search_btn">
    <img src="images/exit.png" alt="">
  </a>
</form>