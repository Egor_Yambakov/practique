{% extends "base.html" %}

{% block top_container %}
  {% include "_header.tpl" %}
{% endblock %}

{% block main_content %}
<div class="main_content">
  <div class="side_bar">
    {% include "_side_bar.tpl" %}
  </div>

  <div class="content_bar">
    {% include "_profil_main.tpl" %}
  </div>
</div>
<script src="/js/jquery.js" type="text/javascript"></script>
<script src="/js/header.js" type="text/javascript"></script>
<script src="/js/profil.js" type="text/javascript"></script>
{% endblock %}