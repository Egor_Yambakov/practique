<div class="filter_list box_shadow">
  <div class="department_block">
    <h3 class="tittle">Отделы</h3>
    <ul>
      {% for dep in filter.department %}
        <li class="transition"><a href="employees.php?dep_query={{dep.name|url_encode}}">{{dep.name}}</a></li>
      {% endfor %}
    </ul>
  </div>
  <div class="filter tag_set">
    <h3 class="tittle">Навыки</h3>
    <ul>
      {% for skill in filter.skills %}
        <li><a href="full_search.php?skills_query={{skill.name|url_encode}}">#{{skill.name}}</a> ({{skill.count}})</li>
      {% endfor %}
      <li id="unhideExtraSkills"><a href="#">показать все</a></li>
    </ul>
  </div>
  <div class="filter tag_set">
    <h3 class="tittle">Интересы</h3>
    <ul>
      {% for interest in filter.interests %}
        <li><a href="full_search.php?interests_query={{interest.name|url_encode}}">#{{interest.name}}</a> ({{interest.count}})</li>
      {% endfor %}
      <li id="unhideExtraInterests"><a href="#">показать все</a></li>
    </ul>
  </div>
</div>