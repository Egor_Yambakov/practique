{% extends "base.html" %}

{% block top_container %}
  {% include "_header.tpl" %}
{% endblock %}

{% block main_content %}
<div class="main_content">
  <div class="side_bar">
    {% include "_side_bar.tpl" %}
  </div>

  <div class="main_content_bar">
    {% include "_news_list.tpl" %}
    {% include "_search_filter.tpl"%}
  </div>
</div>
<script src="/js/jquery.js" type="text/javascript"></script>
<script src="/js/header.js" type="text/javascript"></script>
<script src="/js/news.js" type="text/javascript"></script>
<script src="/js/filter.js" type="text/javascript"></script>
{% endblock %}