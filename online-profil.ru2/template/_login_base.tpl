{% extends "base.html" %}

{% block top_container %}
  {% include "_header.tpl" %}
{% endblock %}

{% block main_content %}
  {% include "_login.tpl" %}
  <script src="/js/jquery.js" type="text/javascript"></script>
  <script src="/js/login.js" type="text/javascript"></script>
{% endblock %}