<?

    function getUser($user_id, $db)
    {   
        $query = "SELECT `id`, `name`, `photo` FROM `user` WHERE `id` = '$user_id'";
        $result = mysqli_query($db, $query);
        $user_data = mysqli_fetch_assoc($result);
        return $user_data;
    }

    function getFilterList($db)
    {
        $filter_data = array();

        $query_dep = "SELECT `name` FROM `department` ORDER BY `id`";
        $result = mysqli_query($db, $query_dep);
        while ($dep = mysqli_fetch_assoc($result))
        {
            $filter_data["department"][] = $dep["name"];
        };

        $query_skills = "SELECT `id`, `name` FROM `skills`";
        $result = mysqli_query($db, $query_skills);
        while ($skill = mysqli_fetch_assoc($result))
        {
            $skill_id = $skill["id"];
            $q2 = "SELECT COUNT(*) FROM `user_skills` WHERE `skill_id` = '$skill_id' GROUP BY `skill_id`";
            $request = mysqli_query($db, $q2);
            $skill["count"] = mysqli_fetch_assoc($request)["COUNT(*)"];
            if ($skill["count"])
                $filter_data["skills"][] = $skill;
        };

        $query_interests = "SELECT `id`, `name` FROM `interest`";
        $result = mysqli_query($db, $query_interests);
        while ($interest = mysqli_fetch_assoc($result))
        {
            $interest_id = $interest["id"];
            $q2 = "SELECT COUNT(*) FROM `user_interest` WHERE `interest_id` = '$interest_id' GROUP BY `interest_id`";
            $request = mysqli_query($db, $q2);
            $interest["count"] = mysqli_fetch_assoc($request)["COUNT(*)"];
            if ($interest["count"])
                $filter_data["interests"][] = $interest;
        };

        return $filter_data;
    }

    // NEWS

    function getNewsData($user_id)
    {   
        global $db;

        $list_data["names"] = getNewEmployees($db);
        $list_data["skills"] = getNewSkills($db);
        $list_data["interests"] = getNewInterests($db);

        $user_data = getUser($user_id, $db);
        $filter_data = getFilterList($db);

        mysqli_close($db);
        $data = array
        (
            "list_data"   => $list_data,
            "user_data"   => $user_data,
            "filter_data" => $filter_data
        );

        return $data;
    }

    function getNewEmployees($db)
    {
        $deltaTime = time() - (60 * 60 * 24 * NEWS_DAYS_PERIOD);
        $query = "SELECT * FROM `user` WHERE `reg_date` > $deltaTime";
        $result = mysqli_query($db, $query);
        $users = array();

        while ($user = mysqli_fetch_assoc($result))
        {
            $job_id = $user["job_tittle_id"];
            $query_job = "SELECT `name` FROM `job_tittle` WHERE `id` = '$job_id'";
            $user["job_tittle"] = mysqli_fetch_assoc(mysqli_query($db, $query_job))["name"];
            $users[] = $user;
        }

        return $users;
    }

    function getNewSkills($db)
    {
        $deltaTime = time() - (60 * 60 * 24 * NEWS_DAYS_PERIOD);
        $query = "SELECT * FROM `skills` WHERE `reg_date` > $deltaTime";
        $result = mysqli_query($db, $query);

        $skills = array();
        while ($skill = mysqli_fetch_assoc($result))
        {
            $skill_id = $skill["id"];
            $q1 = "SELECT * FROM `user_skills` WHERE `skill_id` = $skill_id";
            $skill["count"] = mysqli_num_rows(mysqli_query($db, $q1));
            if ($skill["count"])
            {
                $skills[] = $skill;
            };
        };

        return $skills;
    }

    function getNewInterests($db)
    {
        $deltaTime = time() - (60 * 60 * 24 * NEWS_DAYS_PERIOD);
        $query = "SELECT * FROM `interest` WHERE `reg_date` > $deltaTime";
        $result = mysqli_query($db, $query);
        
        $interests = array();
        while ($interest = mysqli_fetch_assoc($result))
        {
            $interest_id = $interest["id"];
            $q1 = "SELECT * FROM `user_interest` WHERE `interest_id` = $interest_id";
            $interest["count"] = mysqli_num_rows(mysqli_query($db, $q1));
            if ($interest["count"])
            {
                $interests[] = $interest;
            };
        };

        return $interests;
    }

    // EMPLOYEES

    function getEmployeesData($dep_filter, $user_id)
    {
        global $db;

        $employees_data = getDepartmentEmployees($dep_filter, $db);
        $user_data = getUser($user_id, $db);
        $filter_data = getFilterList($db);

        mysqli_close($db);
        $data = array
        (
            "employees_data" => $employees_data["list"],
            "dep_query"      => $employees_data["dep_query"],
            "user_data"      => $user_data,
            "filter_data"    => $filter_data
        );
        return $data;
    }

    function getDepartmentEmployees($filter, $db)
    {   
        $query = "";
        if (!isset($filter) OR empty($filter))
        {
            $query = "SELECT * FROM `user`";
        }
        else
        {
            $q1 = "SELECT `id` FROM `department` WHERE `name` = '$filter'";
            $dep_id = mysqli_fetch_assoc(mysqli_query($db, $q1))["id"];
            $query = "SELECT * FROM `user` WHERE `department_id` = '$dep_id'";
        };

        $result = mysqli_query($db, $query);
        $dep_query = array
        (
            "department" => $filter,
            "count" => mysqli_num_rows($result)
        );

        $list_data = array();
        while ($user_info = mysqli_fetch_assoc($result)) 
        {
            $user_id = $user_info["id"];
            $q1 = "SELECT * FROM `user` WHERE `id` = '$user_id'";
            $request = mysqli_query($db, $q1);

            $department_id = $user_info["department_id"];
            $job_tittle_id = $user_info["job_tittle_id"];
            $room_id = $user_info["room_id"];

            $query_dep = "SELECT `name` FROM `department` WHERE `id` = '$department_id'";
            $query_job = "SELECT `name` FROM `job_tittle` WHERE `id` = '$job_tittle_id'";
            $query_room = "SELECT `number` FROM `room` WHERE `id` = '$room_id'";

            $user_info["department"] = mysqli_fetch_assoc(mysqli_query($db, $query_dep))["name"];
            $user_info["job_tittle"] = mysqli_fetch_assoc(mysqli_query($db, $query_job))["name"];
            $user_info["room"] = mysqli_fetch_assoc(mysqli_query($db, $query_room))["number"];
            $user_info["email"] = preg_split("/\s*\,\s*/", $user_info["email"])[0];

            $list_data[] = $user_info;
        };

        $data_arr = array
        (
            "list"      => $list_data,
            "dep_query" => $dep_query
        );

        return $data_arr;
    }

    // PROFIL

    function getProfilData($filter, $user_id)
    {
        global $db;

        $profil_data = getProfil($filter, $db);
        $user_data = getUser($user_id, $db);
        $edit_data = getEditData($db);
        mysqli_close($db);

        $data = array
        (
            "profil_data" => $profil_data,
            "user_data"   => $user_data,
            "edit_data"   => $edit_data
        );
        return $data;
    }

    function getProfil($filter, $db)
    {
        $query = "SELECT * FROM `user` WHERE `id` = $filter";

        $result = mysqli_query($db, $query);
        $data = mysqli_fetch_assoc($result);
        
        $department_id = $data["department_id"];
        $job_tittle_id = $data["job_tittle_id"];
        $room_id = $data["room_id"];

        $query_dep = "SELECT `name` FROM `department` WHERE `id` = '$department_id'";
        $query_job = "SELECT `name` FROM `job_tittle` WHERE `id` = '$job_tittle_id'";
        $query_room = "SELECT `number` FROM `room` WHERE `id` = '$room_id'";

        $data["department"] = mysqli_fetch_assoc(mysqli_query($db, $query_dep))["name"];
        $data["job_tittle"] = mysqli_fetch_assoc(mysqli_query($db, $query_job))["name"];
        $data["room"] = mysqli_fetch_assoc(mysqli_query($db, $query_room))["number"];
        $data["profil_id"] = $filter;
        $data["birth"] = convertTime($data["birth"]);
        $data["phone_arr"] = preg_split("/\s*\,\s*/", $data["phone"]);
        $data["email_arr"] = preg_split("/\s*\,\s*/", $data["email"]);

        $query = "SELECT `skill_id` FROM `user_skills` WHERE `user_id` = '$filter'";
        $data["skills"] = getCharacteristics("skills", "skill_id", $db, $query);

        $query = "SELECT `interest_id` FROM `user_interest` WHERE `user_id` = '$filter'";
        $data["interest"] = getCharacteristics("interest", "interest_id", $db, $query);

        return $data;
    }

    function getCharacteristics($name, $id_name, $db, $query)
    {
        $name_arr = array();
        $result = mysqli_query($db, $query);
        while ($id = mysqli_fetch_assoc($result)[$id_name])
        {
            $query = "SELECT `name` FROM `$name` WHERE `id` = '$id'";
            $character = mysqli_fetch_assoc(mysqli_query($db, $query))["name"];
            array_push($name_arr, $character);
        };
        return $name_arr;
    }

    function getEditData($db)
    {
        $query_room = "SELECT * FROM `room`";
        $query_dep = "SELECT * FROM `department`";
        $query_job_name = "SELECT * FROM `job_tittle`";

        $result = mysqli_query($db, $query_room);
        $data["rooms"] = mysqli_fetch_all($result, MYSQLI_ASSOC);

        $result = mysqli_query($db, $query_dep);
        $data["departments"] = mysqli_fetch_all($result, MYSQLI_ASSOC);

        $result = mysqli_query($db, $query_job_name);
        $data["job_tittles"] = mysqli_fetch_all($result, MYSQLI_ASSOC);

        return $data;
    }

    function convertTime($time)
    {
        return date("j", $time).getMonth(date("n", $time)).date("Y", $time)."г.";
    }

    function getMonth($month_id)
    {
        $months = array
        (
            " null ", 
            " января ", 
            " февраля ", 
            " марта ", 
            " aпреля ", 
            " мая ", 
            " июня ", 
            " июля ", 
            " aвгуста ", 
            " сентября ", 
            " октября ", 
            " ноября ", 
            " декабря "
        );
        return $months[$month_id];
    }

    // SEARCH

    function getSearchData($filter_arr, $user_id)
    {
        global $db;

        $employees_data = getSearchEmployees($filter_arr, $db);
        $user_data = getUser($user_id, $db);
        $filter_data = getFilterList($db);

        mysqli_close($db);
        $data = array
        (
            "employees_data" => $employees_data["data_list"],
            "user_data"      => $user_data,
            "filter_data"    => $filter_data,
            "search_query"   => $employees_data["search_query"]
        );

        return $data;
    }

    function getSearchEmployees($filter_arr, $db)
    {
        $filter = "";
        $data = array();
        if (isset($filter_arr["skills_query"]) AND !empty($filter_arr["skills_query"]))
        {
            $filter = $filter_arr["skills_query"];
            $data["skills"] = getSkillsUser($filter, $db);
        }
        else if (isset($filter_arr["interests_query"]) AND !empty($filter_arr["interests_query"]))
        {
            $filter = $filter_arr["interests_query"];
            $data["interests"] = getInterestsUser($filter, $db);
        }
        else if (isset($filter_arr["query"]) AND !empty($filter_arr["query"]))
        {
            $filter = $filter_arr["query"];
            $data["query_data"] = getFullList($filter, $db);
        }
        else 
        {
            header("Location: employees.php");
        };

        $data_arr = array
        (
            "search_query" => $filter,
            "data_list"    => $data
        );

        return $data_arr;
    }

    function getSkillsUser($filter, $db)
    {
        $skills_arr = array();
        $q1 = "SELECT `id` FROM `skills` WHERE `name` = '$filter'";
        $skill_id = mysqli_fetch_assoc(mysqli_query($db, $q1))["id"];
        $q2 = "SELECT `user_id` FROM `user_skills` WHERE `skill_id` = '$skill_id'";
        $result = mysqli_query($db, $q2);
        while ($user_id = mysqli_fetch_assoc($result)["user_id"])
        {
            $query = "SELECT * FROM `user` WHERE `id` = '$user_id'";
            $user = mysqli_fetch_assoc(mysqli_query($db, $query));

            $department_id = $user["department_id"];
            $job_tittle_id = $user["job_tittle_id"];

            $query_dep = "SELECT `name` FROM `department` WHERE `id` = '$department_id'";
            $query_job = "SELECT `name` FROM `job_tittle` WHERE `id` = '$job_tittle_id'";


            $user["department"] = mysqli_fetch_assoc(mysqli_query($db, $query_dep))["name"];
            $user["job_tittle"] = mysqli_fetch_assoc(mysqli_query($db, $query_job))["name"];

            $skills_arr[] = $user;
        };
        return $skills_arr;
    }

    function getInterestsUser($filter, $db)
    {
        $interests_arr = array();
        $q1 = "SELECT `id` FROM `interest` WHERE `name` = '$filter'";
        $interest_id = mysqli_fetch_assoc(mysqli_query($db, $q1))["id"];
        $q2 = "SELECT `user_id` FROM `user_interest` WHERE `interest_id` = '$interest_id'";
        $result = mysqli_query($db, $q2);
        while ($user_id = mysqli_fetch_assoc($result)["user_id"])
        {
            $query = "SELECT * FROM `user` WHERE `id` = '$user_id'";
            $user = mysqli_fetch_assoc(mysqli_query($db, $query));

            $department_id = $user["department_id"];
            $job_tittle_id = $user["job_tittle_id"];

            $query_dep = "SELECT `name` FROM `department` WHERE `id` = '$department_id'";
            $query_job = "SELECT `name` FROM `job_tittle` WHERE `id` = '$job_tittle_id'";


            $user["department"] = mysqli_fetch_assoc(mysqli_query($db, $query_dep))["name"];
            $user["job_tittle"] = mysqli_fetch_assoc(mysqli_query($db, $query_job))["name"];

            $interests_arr[] = $user;
        };
        return $interests_arr;
    }

    function getFullList($filter, $db)
    {
        $data["names"] = getNamesList($db, $filter);
        $data["skills"] = getSkillsList($db, $filter);
        $data["interests"] = getInterestsList($db, $filter);

        return $data;
    }
    function getNamesList($db, $filter)
    {
        $q1 = "SELECT `id`, `name`, `job_tittle_id`, `photo` FROM `user` WHERE `name` LIKE '%".$filter."%'";
        $request = mysqli_query($db, $q1);
        $names_arr = array();

        while ($name = mysqli_fetch_assoc($request))
        {   
            $job_tittle_id = $name["job_tittle_id"];
            $q2 = "SELECT `name` FROM `job_tittle` WHERE `id` = '$job_tittle_id'";
            $result = mysqli_query($db, $q2);
            $name["job_tittle"] = mysqli_fetch_assoc($result)["name"];

            $names_arr[] = $name;
        };

        return $names_arr;
    }

    function getSkillsList($db, $filter)
    {
        $q1 = "SELECT `id`, `name` FROM `skills` WHERE `name` LIKE '%".$filter."%'";
        $request = mysqli_query($db, $q1);
        $skills_arr = array();

        while ($skill = mysqli_fetch_assoc($request)) 
        {
            $skill_id = $skill["id"];
            $q2 = "SELECT COUNT(*) FROM `user_skills` WHERE `skill_id` = '$skill_id' GROUP BY `skill_id`";
            $result = mysqli_query($db, $q2);
            $skill["count"] = mysqli_fetch_assoc($result)["COUNT(*)"];
            if ($skill["count"])
                $skills_arr[] = $skill;
        };

        return $skills_arr;
    }

    function getInterestsList($db, $filter)
    {
        $q1 = "SELECT `id`, `name` FROM `interest` WHERE `name` LIKE '%".$filter."%'";
        $request = mysqli_query($db, $q1);
        $interests_arr = array();

        while ($interest = mysqli_fetch_assoc($request)) 
        {
            $interest_id = $interest["id"];
            $q2 = "SELECT COUNT(*) FROM `user_interest` WHERE `interest_id` = '$interest_id' GROUP BY `interest_id`";
            $result = mysqli_query($db, $q2);
            $interest["count"] = mysqli_fetch_assoc($result)["COUNT(*)"];
            if ($interest["count"])
                $interests_arr[] = $interest;
        };

        return $interests_arr;
    }

    // INDEX
    
    function checkPostForm($postForm, $accesLocation)
    {
        if (isset($postForm["submit"]))
        {
            global $db;

            $login = $postForm["login"];

            $query = "SELECT `user_id`, `user_password` FROM `login` WHERE `user_login` = '$login'";
            $result = mysqli_query($db, $query);
            if (mysqli_num_rows($result))
            {
                $data = mysqli_fetch_assoc($result);
                if ($data["user_password"] == md5($postForm["password"]))
                {
                    setcookie("id", $data['user_id']);
                    header("location: $accesLocation");
                    exit;
                };
            };
        };        
    }