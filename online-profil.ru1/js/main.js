const NUMBER_OF_CHAR_TO_SEARCH = 1;
const MAX_FILTER_LIST_LENGTH = 5;
const MAX_RESULT_LIST_LENGHT = 4;

window.onload = onWindowLoaded;

function onWindowLoaded()
{
    $(".login_btn").click(triggerProfilMenu);
    $("#search_box").focus(revealAdviceMenu);
    $("html").click(hideMenu);
    $("#search_box").keyup(searchAdvice);

    hideExtraButton($("#unhideExtraSkills"));
    hideExtraButton($("#unhideExtraInterests"));

    $(".edit_btn").click(triggerEditMenu);
    $("#save_changes").click(sendEditAjax);

    hideExtraResults($(".new_users"));
    hideAllResultsBtn($(".show_all_btn"));

    $(".clear_search_btn").click(clearSearchBar);

    checkToEditLink();
}

function checkToEditLink()
{
    let isOpen = getGetParameterByName("edit");
    if (isOpen == "true")
    {
        triggerEditMenu();
    }
}

function clearSearchBar()
{
    $("#main_search_box").val("").focus();
}

function hideExtraResults(element)
{
    element.children(":gt(" + (MAX_RESULT_LIST_LENGHT - 1) + ")").hide();
}

function hideAllResultsBtn(element)
{
    if (element.parent().children().length > MAX_RESULT_LIST_LENGHT + 1)
    {
        element.show();
        element.click(function() {
            $(this).parent().children().css("display", "block");
            $(this).hide();
        });
    }
}

function triggerProfilMenu()
{
    if ($(".dropdown_menu").hasClass("dropd"))
    {
        $(".dropdown_menu").removeClass("dropd");
    }
    else
    {
        $(".dropdown_menu").addClass("dropd");
    };
}

function triggerEditMenu()
{
    if ($(".edit_profil").is(":visible"))
    {   
        $("#profil_information").show();
        $(".edit_profil").hide();
    }
    else
    {
        $("#profil_information").hide();
        $(".edit_profil").show();
    }
}

function updateInfo()
{
    sendEditAjax();
}

function searchAdvice(event)
{
    switch(event.keyCode)
    {
        case 13:
        case 27:
            break;
        default:
            sendSearchAjax();
            break;
    };
}

function sendEditAjax()
{
    let data = objectifyForm($("form.edit_profil"));
    data["id"] = getGetParameterByName("id");
    console.log(data);

    $.post
    (
        "/ajax/ajax_update_data.php", 
        data,
        logData
    );
}

function logData(data)
{
    if (data != "")
        alert(data);
    else
    {
        window.location.reload();
    };
}

function sendSearchAjax()
{
    if($("#search_box").val().length >= NUMBER_OF_CHAR_TO_SEARCH)
    {
        queryFullData();
    }else
    {
        queryNamesData();
    };
}

function queryFullData()
{
    $(".result_page_link").show().attr("href", "full_search.php?query=" + $("#search_box").val());
    $.get("ajax/ajax_full.php", {"query": $("#search_box").val()}, processData);
}

function queryNamesData()
{
    $(".result_page_link, .result_skills").hide();
    $.get("ajax/ajax_name.php", {}, processName);
}

function processData(data)
{   
    data = $.parseJSON(data);

    $("#result_names_list").empty();
    $("#result_skills_list").empty();
    $("#result_interests_list").empty();
    $(".result_skills").hide();

    addNamesResult(data["names"]);
    addSkillsResult(data["skills"]);
    addInterestsResult(data["interests"]);
}

function processName(data)
{
    data = $.parseJSON(data);

    $("#result_names_list").empty();

    addNamesResult(data["names"]);
}

function addNamesResult(names)
{
    for(let name of names) 
    {
        let id = name["id"];
        $("<a>", 
        {
            class: "result_name_block",
            href: "profil.php?id=" + id,
            append: $("<span>", 
            {
                class: "result_photo",
                append: $("<img>")
                    .attr("src", name["photo"])
            })
            .add($("<span>", 
            {
                class: "result_info",
                append: $("<h4>", 
                {
                    class: "tittle",
                    text: name["name"]
                })
                .add($("<span>", 
                {
                    class: "text",
                    text: name["job_tittle"]
                }))
            }))
        })
        .appendTo("#result_names_list");
    };
}

function addSkillsResult(skills)
{
    if (skills.length)
    {
        $("#result_skills_list").parent().show();
        for(let skill of skills)
        {
            $("<li>", 
            {
                append: $("<a>", {
                    href: "full_search.php?skills_query=" + skill["name"],
                    text: skill["name"] + ' (' + skill["count"] + ')'
                })
            })
            .appendTo("#result_skills_list");
        };
    };
}

function addInterestsResult(interests)
{
    if (interests.length)
    {
        $("#result_interests_list").parent().show();
        for(let interest of interests)
        {
            $("<li>", 
            {   
                append: $("<a>", {
                    href: "full_search.php?interests_query=" + interest["name"],
                    text: interest["name"] + ' (' + interest["count"] + ')'
                }) 
            })
            .appendTo("#result_interests_list");
        };
    };    
}

function revealAdviceMenu()
{
    $("#search_advice").show();
    if($("#search_box").val().length < NUMBER_OF_CHAR_TO_SEARCH)
    {
        queryNamesData();
    }
}

function hideMenu(event)
{
    if (!$(event.target).is("#search_box"))
        $("#search_advice").hide();
}

function hideExtraButton(element)
{
    if (element.parent().children().length <= MAX_FILTER_LIST_LENGTH + 1)
    {
        element.hide();
    }
    else
    {
        element.click(function() {
            $(this).parent().children().css("display", "inline");
            $(this).hide();
        });
    };
}

function getGetParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    let regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function objectifyForm(form) 
{
  const formArray = form.serializeArray();
  let returnArray = {};
  for (let i = 0; i < formArray.length; i++){
    returnArray[formArray[i]['name']] = formArray[i]['value'];
  }
  return returnArray;
}
