<?
    require_once "../db_config.php";
    $data["names"] = findNames($db);
    mysqli_close($db);
    echo json_encode($data);

    function findNames($db)
    {
        $q1 = "SELECT `id`, `name`, `job_tittle_id`, `photo` FROM `user` LIMIT 6";
        $request = mysqli_query($db, $q1);
        $names_arr = array();

        while ($name = mysqli_fetch_assoc($request))
        {   
            $job_tittle_id = $name["job_tittle_id"];
            $q2 = "SELECT `name` FROM `job_tittle` WHERE `id` = '$job_tittle_id'";
            $result = mysqli_query($db, $q2);
            $name["job_tittle"] = mysqli_fetch_assoc($result)["name"];

            $names_arr[] = $name;
        };

        return $names_arr;
    }