<?
    require_once "../db_config.php";
    if(!empty($_GET['query']))
    {   
        global $db;
        $query = $_GET['query'];

        $data["names"] = findNames($db, $query);
        $data["skills"] = findSkills($db, $query);
        $data["interests"] = findInterests($db, $query);

        mysqli_close($db);
        echo json_encode($data);
    }

    function findNames($db, $query)
    {
        $q1 = "SELECT `id`, `name`, `job_tittle_id`, `photo` FROM `user` WHERE `name` LIKE '%".$query."%' LIMIT 8";
        $request = mysqli_query($db, $q1);
        $names_arr = array();

        while ($name = mysqli_fetch_assoc($request))
        {   
            $job_tittle_id = $name["job_tittle_id"];
            $q2 = "SELECT `name` FROM `job_tittle` WHERE `id` = '$job_tittle_id'";
            $result = mysqli_query($db, $q2);
            $name["job_tittle"] = mysqli_fetch_assoc($result)["name"];

            $names_arr[] = $name;
        };

        return $names_arr;
    }

    function findSkills($db, $query)
    {
        $q1 = "SELECT `id`, `name` FROM `skills` WHERE `name` LIKE '%".$query."%' LIMIT 2";
        $request = mysqli_query($db, $q1);
        $skills_arr = array();

        while ($skill = mysqli_fetch_assoc($request)) 
        {
            $skill_id = $skill["id"];
            $q2 = "SELECT COUNT(*) FROM `user_skills` WHERE `skill_id` = '$skill_id' GROUP BY `skill_id`";
            $result = mysqli_query($db, $q2);
            $skill["count"] = mysqli_fetch_assoc($result)["COUNT(*)"];
            if ($skill["count"])
                $skills_arr[] = $skill;
        };

        return $skills_arr;
    }

    function findInterests($db, $query)
    {
        $q1 = "SELECT `id`, `name` FROM `interest` WHERE `name` LIKE '%".$query."%' LIMIT 2";
        $request = mysqli_query($db, $q1);
        $interests_arr = array();

        while ($interest = mysqli_fetch_assoc($request)) 
        {
            $interest_id = $interest["id"];
            $q2 = "SELECT COUNT(*) FROM `user_interest` WHERE `interest_id` = '$interest_id' GROUP BY `interest_id`";
            $result = mysqli_query($db, $q2);
            $interest["count"] = mysqli_fetch_assoc($result)["COUNT(*)"];
            if ($interest["count"])
                $interests_arr[] = $interest;
        };

        return $interests_arr;
    }