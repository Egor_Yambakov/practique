<?
    require_once "../db_config.php";
    $err = updateData($_POST);
    echo $err;

    function updateData($new_data)
    { 
        global $db; 
        
        $err = "";
        updateDepartment($db, $new_data["id"], $new_data["department"]);
        updateEmail($db, $new_data["id"], $new_data["email"]);
        updateInterests($db, $new_data["id"], $new_data["interests"]);
        updateSkills($db, $new_data["id"], $new_data["skills"]);
        updateJob($db, $new_data["id"], $new_data["job_tittle"]);
        updatePhone($db, $new_data["id"], $new_data["phone"]);
        updateRoom($db, $new_data["id"], $new_data["room"]);
        updateWorkDescr($db, $new_data["id"], $new_data["work_descr"]);

        mysqli_close($db);

        return $err;
    };

    function updateWorkDescr($db, $id, $new_info)
    {
        $query = "UPDATE `user` 
                  SET `work_descr` = '$new_info'
                  WHERE `id` = '$id'";
        mysqli_query($db, $query);
    }

    function updateRoom($db, $id, $new_info)
    {
        $query = "UPDATE `user` 
                  SET `room_id` = (SELECT `id` 
                                         FROM `room` 
                                         WHERE `number` = '$new_info') 
                  WHERE `id` = '$id'";
        $result = mysqli_query($db, $query);
    }

    function updatePhone($db, $id, $new_info)
    {
        $query = "UPDATE `user` 
                  SET `phone` = '$new_info'
                  WHERE `id` = '$id'";
        mysqli_query($db, $query);
    };

    function updateJob($db, $id, $new_info)
    {
        $query = "UPDATE `user` 
                  SET `job_tittle_id` = (SELECT `id` 
                                         FROM `job_tittle` 
                                         WHERE `name` = '$new_info') 
                  WHERE `id` = '$id'";
        $result = mysqli_query($db, $query);
    };

    function updateSkills($db, $id, $new_info)
    {
        preg_match_all("/#([^\s#]+)/i", $new_info, $matches);
        $skills_arr = $matches[1];

        clearOldSkills($db, $id);

        foreach ($skills_arr as $skill)
        {
            $query = "SELECT `id` FROM `skills` WHERE `name` = '$skill'";
            $result = mysqli_query($db, $query);
            if (mysqli_num_rows($result))
            { 
                $skill_id = mysqli_fetch_assoc($result)["id"];
                addNewSkillInfo($db, $id, $skill_id);
            }
            else
            {
                $skill_id = addNewSkill($db, $skill);
                addNewSkillInfo($db, $id, $skill_id);
            };
        };
    };

    function addNewSkillInfo($db, $id, $skill_id)
    {
        $query_update = "INSERT INTO `user_skills` 
                         (`user_id`, `skill_id`)
                         VALUES
                         ('$id', '$skill_id')";
        mysqli_query($db, $query_update); 
    };

    function addNewSkill($db, $name)
    {
        $reg_time = time();
        $query_add = "INSERT INTO `skills` 
                      (`name`, `reg_date`)
                      VALUES 
                      ('$name', '$reg_time')";
        mysqli_query($db, $query_add);

        return mysqli_insert_id($db);
    };

    function updateInterests($db, $id, $new_info)
    {
        preg_match_all("/#([^\s#]+)/i", $new_info, $matches);
        $interests_arr = $matches[1];

        clearOldInterests($db, $id);

        foreach ($interests_arr as $interest) 
        {
            $query = "SELECT `id` FROM `interest` WHERE `name` = '$interest'";
            $result = mysqli_query($db, $query);
            if (mysqli_num_rows($result))
            {
                $interest_id = mysqli_fetch_assoc($result)["id"];
                addNewInterestInfo($db, $id, $interest_id);
            }
            else
            {
                $interest_id = addNewInterest($db, $interest);
                addNewInterestInfo($db, $id, $interest_id);
            };
        };
    };

    function addNewInterestInfo($db, $id, $interest_id)
    {
        $query_update = "INSERT INTO `user_interest`
                         (`user_id`, `interest_id`)
                         VALUES
                         ('$id', '$interest_id')";
        mysqli_query($db, $query_update);
    };

    function addNewInterest($db, $name)
    {
        $reg_time = time();
        $query_add = "INSERT INTO `interest` 
                      (`name`, `reg_date`)
                      VALUES 
                      ('$name', '$reg_time')";
        mysqli_query($db, $query_add);

        return mysqli_insert_id($db);
    };

    function clearOldSkills($db, $id)
    {
        $query_delete = "DELETE FROM `user_skills` WHERE `user_id` = '$id'";
        mysqli_query($db, $query_delete);
        $query_delete = "DELETE FROM `skills` WHERE `id` NOT IN (SELECT `skill_id` FROM `user_skills`)";
        mysqli_query($db, $query_delete);

    };

    function clearOldInterests($db, $id)
    {
        $query_delete = "DELETE FROM `user_interest` WHERE `user_id` = '$id'";
        mysqli_query($db, $query_delete);
        $query_delete = "DELETE FROM `interest` WHERE `id` NOT IN (SELECT `interest_id` FROM `user_interest`)";
        mysqli_query($db, $query_delete);
    };

    function updateEmail($db, $id, $new_info)
    {
        $query = "UPDATE `user` 
                  SET `email` = '$new_info'
                  WHERE `id` = '$id'";
        mysqli_query($db, $query);
    };

    function updateDepartment($db, $id, $new_info)
    {
        $query = "UPDATE `user` 
                  SET `department_id` = (SELECT `id` 
                                         FROM `department` 
                                         WHERE `name` = '$new_info') 
                  WHERE `id` = '$id'";
        $result = mysqli_query($db, $query);
    };