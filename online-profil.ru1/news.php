<?
    require_once('require_config.php');
    define('NEWS_DAYS_PERIOD', 30);
    checkData($_COOKIE["id"], "login");
    $data = getNewsData($_COOKIE["id"]);
    buildPage('_news_base.tpl',    array
                                        (
                                            "list"   => $data["list_data"],
                                            "user"   => $data["user_data"],
                                            "filter" => $data["filter_data"]
                                        ));