<?php

/* _news_list.tpl */
class __TwigTemplate_6794d12fab59d98d069097063d4cfc97d993610de838dba264a7b312e73eca10 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"full_search_list\">
  ";
        // line 2
        if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "names", array()))) {
            // line 3
            echo "    <div class=\"main_search_tittle\">
      <h1>Новые сотрудники (";
            // line 4
            echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "names", array())), "html", null, true);
            echo ")</h1>
    </div>
    <div class=\"result_blocks new_users\">
      ";
            // line 7
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "names", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["userName"]) {
                // line 8
                echo "      <div class=\"pasport_block\">
        <a href=\"profil.php?id=";
                // line 9
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["userName"], "id", array()), "html", null, true);
                echo "\" class=\"pasport_photo\">
          <img src=\"";
                // line 10
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["userName"], "photo", array()), "html", null, true);
                echo "\" alt=\"\">
        </a>
        <div class=\"pasport_info\">
          <h3 class=\"tittle\"><a href=\"profil.php?id=";
                // line 13
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["userName"], "id", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["userName"], "name", array()), "html", null, true);
                echo "</a></h3>
          <span>";
                // line 14
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["userName"], "job_tittle", array()), "html", null, true);
                echo "</span>
        </div>
      </div>
      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['userName'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 18
            echo "      <a class=\"show_all_btn\" href=\"#\">Показать всех</a>
    </div>
  ";
        }
        // line 21
        echo "  ";
        if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "skills", array()))) {
            // line 22
            echo "    <div class=\"main_search_tittle\">
      <h1>Новые навыки (";
            // line 23
            echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "skills", array())), "html", null, true);
            echo ")</h1>
    </div>
    <div class=\"result_skills_list\">
      ";
            // line 26
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "skills", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["skill"]) {
                // line 27
                echo "      <span><a href=\"full_search.php?skills_query=";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["skill"], "name", array()), "html", null, true);
                echo "\">#";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["skill"], "name", array()), "html", null, true);
                echo " (";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["skill"], "count", array()), "html", null, true);
                echo ")</a></span>
      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['skill'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 29
            echo "    </div>
  ";
        }
        // line 31
        echo "  ";
        if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "interests", array()))) {
            // line 32
            echo "    <div class=\"main_search_tittle\">
      <h1>Новые интересы (";
            // line 33
            echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "interests", array())), "html", null, true);
            echo ")</h1>
    </div>
    <div class=\"result_skills_list\">
      ";
            // line 36
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "interests", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["interest"]) {
                // line 37
                echo "      <span><a href=\"full_search.php?interests_query=";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["interest"], "name", array()), "html", null, true);
                echo "\">#";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["interest"], "name", array()), "html", null, true);
                echo " (";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["interest"], "count", array()), "html", null, true);
                echo ")</a></span>
      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['interest'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 39
            echo "    </div>
  ";
        }
        // line 41
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "_news_list.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  141 => 41,  137 => 39,  124 => 37,  120 => 36,  114 => 33,  111 => 32,  108 => 31,  104 => 29,  91 => 27,  87 => 26,  81 => 23,  78 => 22,  75 => 21,  70 => 18,  60 => 14,  54 => 13,  48 => 10,  44 => 9,  41 => 8,  37 => 7,  31 => 4,  28 => 3,  26 => 2,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_news_list.tpl", "D:\\Files\\doc\\BEP\\sites\\templates\\_news_list.tpl");
    }
}
