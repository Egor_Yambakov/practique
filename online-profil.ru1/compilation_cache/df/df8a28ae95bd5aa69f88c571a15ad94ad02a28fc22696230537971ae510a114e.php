<?php

/* base.php */
class __TwigTemplate_82e3e7e147beae0fb0c4ad19cd2b49b222fc2ea6bc26478e0cf0823672e4c3b1 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
            'top_container' => array($this, 'block_top_container'),
            'main_content' => array($this, 'block_main_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"ru\">
<head>
  <meta charset=\"UTF-8\">
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
  <link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\">
  <title>Онлайн-профиль</title>
</head>
<body>
  <div class=\"all_content\">
    <header class=\"top_container\">
      ";
        // line 12
        $this->displayBlock('top_container', $context, $blocks);
        // line 14
        echo "    </header>
    <main class=\"main_content\">
      ";
        // line 16
        $this->displayBlock('main_content', $context, $blocks);
        // line 18
        echo "    </main>
  <footer></footer>
  </div>
</body>
</html>";
    }

    // line 12
    public function block_top_container($context, array $blocks = array())
    {
        // line 13
        echo "      ";
    }

    // line 16
    public function block_main_content($context, array $blocks = array())
    {
        // line 17
        echo "      ";
    }

    public function getTemplateName()
    {
        return "base.php";
    }

    public function getDebugInfo()
    {
        return array (  64 => 17,  61 => 16,  57 => 13,  54 => 12,  46 => 18,  44 => 16,  40 => 14,  38 => 12,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "base.php", "D:\\Files\\doc\\BEP\\sites\\site-3.1\\templates\\base.php");
    }
}
