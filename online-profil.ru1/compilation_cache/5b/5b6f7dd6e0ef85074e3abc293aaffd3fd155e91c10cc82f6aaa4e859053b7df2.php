<?php

/* _main_info.html */
class __TwigTemplate_e0ab04567fb6c708fa85b66c4a7668cd94957bb272d74d5ed08fa68a862aa3ab extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("_pasport.html", "_main_info.html", 1);
        $this->blocks = array(
            'page_foto' => array($this, 'block_page_foto'),
            'page_info' => array($this, 'block_page_info'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_pasport.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_page_foto($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"page_foto\">
  <img src=\"";
        // line 5
        echo twig_escape_filter($this->env, ($context["page_foto"] ?? null), "html", null, true);
        echo "\" alt=\"avatar\">
</div>
<div class=\"action\">
  <a href=\"#\">Отправить сообщение</a>
</div>
";
    }

    // line 12
    public function block_page_info($context, array $blocks = array())
    {
        // line 13
        echo "<h2 class=\"page_name red_border\">";
        echo twig_escape_filter($this->env, ($context["page_name"] ?? null), "html", null, true);
        echo "</h2>
<h4 class=\"info_header\">Работа</h4>
<ul class=\"record\">
  <li class=\"department\">
    <span class=\"field\">Отделение: </span>
    <span class=\"note\">";
        // line 18
        echo twig_escape_filter($this->env, ($context["department"] ?? null), "html", null, true);
        echo "</span>
  </li>
  <li class=\"post\">
    <span class=\"field\">Должность: </span>
    <span class=\"note\">";
        // line 22
        echo twig_escape_filter($this->env, ($context["post"] ?? null), "html", null, true);
        echo "</span>
  </li>
</ul>
<h4 class=\"info_header\">Контакты</h4>
<ul class=\"record\">
  <li class=\"phone\">
    <span class=\"field\">Телефон: </span>
    <span class=\"note\">";
        // line 29
        echo twig_escape_filter($this->env, ($context["phone_number"] ?? null), "html", null, true);
        echo "</span>
  </li>
  <li class=\"mail\">
    <span class=\"field\">Почта: </span>
    <a href=\"mailto:";
        // line 33
        echo twig_escape_filter($this->env, ($context["email"] ?? null), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, ($context["email"] ?? null), "html", null, true);
        echo "</a>
  </li>
</ul>
<h4 class=\"info_header\">Чем занимается в компании</h4>
<ul class=\"record\">
  <li class=\"field\">
    <p>";
        // line 40
        echo twig_escape_filter($this->env, ($context["duties_text"] ?? null), "html", null, true);
        // line 41
        echo "</p>
  </li>
</ul>
";
    }

    public function getTemplateName()
    {
        return "_main_info.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 41,  96 => 40,  85 => 33,  78 => 29,  68 => 22,  61 => 18,  52 => 13,  49 => 12,  39 => 5,  36 => 4,  33 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_main_info.html", "D:\\Files\\doc\\BEP\\sites\\site-1.2\\templates\\_main_info.html");
    }
}
