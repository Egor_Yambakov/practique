<?php

/* _profil_main.tpl */
class __TwigTemplate_7f306ca2ba359d84f24543f3a01f309509e4f32c0be94c4c5b59b61d5605ed6a extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"profil_container\">
  <div class=\"side_block\">
    <div class=\"profil_side\">
      <div class=\"profil_photo\">
        <img src=\"";
        // line 5
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "photo", array()), "html", null, true);
        echo "\" alt=\"\">
      </div>
      ";
        // line 7
        if ((twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "id", array()) == twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", array()))) {
            // line 8
            echo "        <a class=\"button edit_btn\">Редактировать</a>
      ";
        } else {
            // line 10
            echo "        <a class=\"button\" href=\"mailto:";
            echo twig_escape_filter($this->env, (($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5 = twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "email_arr", array())) && is_array($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5) || $__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5 instanceof ArrayAccess ? ($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5[0] ?? null) : null), "html", null, true);
            echo "\" target=\"_blank\">Написать собщение</a>
      ";
        }
        // line 12
        echo "    </div>
  </div>
  <div class=\"main_block\">
    <div id=\"profil_information\">
      <div class=\"pasport_block\">
        <div class=\"pasport_info\">
          <h3 class=\"tittle\">";
        // line 18
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "name", array()), "html", null, true);
        echo "</h3>
          <span class=\"status\">";
        // line 19
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "job_tittle", array()), "html", null, true);
        echo " в ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "department", array()), "html", null, true);
        echo "</span>
          <table>
              <tr>
                <td><span>Телефон</span></td>
                <td>
                  ";
        // line 24
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "phone_arr", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["phone"]) {
            // line 25
            echo "                  <span>";
            echo twig_escape_filter($this->env, $context["phone"], "html", null, true);
            echo "</span><br>
                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['phone'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 27
        echo "                </td>
              </tr>
              <tr>
                <td><span>Кабинет</span></td>
                <td><span>";
        // line 31
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "room", array()), "html", null, true);
        echo "</span></td>
              </tr>
              <tr>
                <td><span>Email</span></td>
                <td>
                  ";
        // line 36
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "email_arr", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["email"]) {
            // line 37
            echo "                  <a href=\"mailto:";
            echo twig_escape_filter($this->env, $context["email"], "html", null, true);
            echo "\" target=\"_blank\">";
            echo twig_escape_filter($this->env, $context["email"], "html", null, true);
            echo "</a><br>
                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['email'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 39
        echo "                </td>
              </tr>
              <tr>
                <td><span>Дата рождения</span></td>
                <td><span>";
        // line 43
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "birth", array()), "html", null, true);
        echo "</span></td>
              </tr>
            </table>
        </div>
      </div>
      <div class=\"pasport_info skills\">
        <h3 class=\"tittle\">Навыки</h3>
        <ul class=\"skills_list\">
          ";
        // line 51
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "skills", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["skill"]) {
            echo "  
            <li>#";
            // line 52
            echo twig_escape_filter($this->env, $context["skill"], "html", null, true);
            echo "</li>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['skill'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 54
        echo "        </ul>
      </div>
      <div class=\"pasport_info skills\">
        <h3 class=\"tittle\">Интересы</h3>
        <ul class=\"skills_list\">
          ";
        // line 59
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "interest", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["interest"]) {
            echo "  
            <li>#";
            // line 60
            echo twig_escape_filter($this->env, $context["interest"], "html", null, true);
            echo "</li>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['interest'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 62
        echo "        </ul>
    </div>
    <div class=\"pasport_info work_descr\">
      <h3 class=\"tittle underline\">Описание деятельности</h3>
      <p>";
        // line 66
        echo nl2br(twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "work_descr", array()), "html", null, true));
        echo "</p>
    </div>
  </div>

  ";
        // line 70
        if ((twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "id", array()) == twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", array()))) {
            // line 71
            echo "  <form class=\"edit_profil\">
    <div class=\"pasport_info\">
      <h3 class=\"tittle\">Редактирование профиля</h3>
      <span class=\"status\"></span>
      <label>
        <span>Отдел</span>
        <select name=\"department\">
        ";
            // line 78
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["edit"] ?? null), "departments", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["dep"]) {
                // line 79
                echo "          <option value=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["dep"], "name", array()), "html", null, true);
                echo "\" ";
                if ((twig_get_attribute($this->env, $this->source, $context["dep"], "name", array()) == twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "department", array()))) {
                    echo "selected";
                }
                echo ">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["dep"], "name", array()), "html", null, true);
                echo "</option>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dep'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 81
            echo "        </select>
      </label>
      <label>
        <span>Должность</span>
        <select name=\"job_tittle\">
        ";
            // line 86
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["edit"] ?? null), "job_tittles", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["job"]) {
                // line 87
                echo "          <option value=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["job"], "name", array()), "html", null, true);
                echo "\" ";
                if ((twig_get_attribute($this->env, $this->source, $context["job"], "name", array()) == twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "job_tittle", array()))) {
                    echo "selected";
                }
                echo ">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["job"], "name", array()), "html", null, true);
                echo "</option>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['job'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 89
            echo "        </select>
      </label>
      <label>
        <span>Телефон</span>
        <input name=\"phone\" type=\"text\" value=\"";
            // line 93
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "phone", array()), "html", null, true);
            echo "\">
      </label>
      <label>
        <span>Кабинет</span>
        <select name=\"room\">
        ";
            // line 98
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["edit"] ?? null), "rooms", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["room"]) {
                // line 99
                echo "          <option value=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["room"], "number", array()), "html", null, true);
                echo "\" ";
                if ((twig_get_attribute($this->env, $this->source, $context["room"], "number", array()) == twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "room", array()))) {
                    echo "selected";
                }
                echo ">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["room"], "number", array()), "html", null, true);
                echo "</option>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['room'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 101
            echo "        </select>
      </label>
      <label>
        <span>Email</span>
        <input name=\"email\" type=\"text\" value=\"";
            // line 105
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "email", array()), "html", null, true);
            echo "\">
      </label>
      <label class=\"textarea\">
        <h3>Навыки</h3>
        <textarea id=\"edit_skills\" name=\"skills\">";
            // line 109
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "skills", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["skill"]) {
                echo "#";
                echo twig_escape_filter($this->env, $context["skill"], "html", null, true);
                echo "   ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['skill'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo "</textarea>
      </label>
      <label class=\"textarea\">
        <h3>Интересы</h3>
        <textarea id=\"edit_interests\" name=\"interests\">";
            // line 113
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "interest", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["interest"]) {
                echo "#";
                echo twig_escape_filter($this->env, $context["interest"], "html", null, true);
                echo "   ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['interest'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo "</textarea>
      </label>
      <label class=\"textarea\">
        <h3>Описание деятельности</h3>
        <textarea name=\"work_descr\">";
            // line 117
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "work_descr", array()));
            echo "</textarea>
      </label>
      <a id=\"save_changes\" class=\"button\">Сохранить изменения</a>
    </div>
  </form>
  ";
        }
        // line 123
        echo "</div>
</div>";
    }

    public function getTemplateName()
    {
        return "_profil_main.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  318 => 123,  309 => 117,  293 => 113,  277 => 109,  270 => 105,  264 => 101,  249 => 99,  245 => 98,  237 => 93,  231 => 89,  216 => 87,  212 => 86,  205 => 81,  190 => 79,  186 => 78,  177 => 71,  175 => 70,  168 => 66,  162 => 62,  154 => 60,  148 => 59,  141 => 54,  133 => 52,  127 => 51,  116 => 43,  110 => 39,  99 => 37,  95 => 36,  87 => 31,  81 => 27,  72 => 25,  68 => 24,  58 => 19,  54 => 18,  46 => 12,  40 => 10,  36 => 8,  34 => 7,  29 => 5,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_profil_main.tpl", "D:\\Files\\doc\\BEP\\sites\\templates\\_profil_main.tpl");
    }
}
