<?php

/* _main_profil.tpl */
class __TwigTemplate_73b7c63c2d7d271a799aeadcfc50e8e27fbcba28ecf1e336510ea3770e908da4 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"profil_container\">
  <div class=\"side_block\">
    <div class=\"profil_side\">
      <div class=\"profil_photo\">
        <img src=\"";
        // line 5
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "photo", array()), "html", null, true);
        echo "\" alt=\"\">
      </div>
      ";
        // line 7
        if ((twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "id", array()) == twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", array()))) {
            // line 8
            echo "        <a class=\"button edit_btn\">Редактировать</a>
      ";
        } else {
            // line 10
            echo "        <a class=\"button\" href=\"mailto:";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "email", array()), "html", null, true);
            echo "\">Написать собщение</a>
      ";
        }
        // line 12
        echo "    </div>
  </div>
  <div class=\"main_block\">
    <div class=\"pasport_block\">
      <div class=\"pasport_info\">
        <h3 class=\"tittle\">";
        // line 17
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "name", array()), "html", null, true);
        echo "</h3>
        <span class=\"status\">";
        // line 18
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "job_tittle", array()), "html", null, true);
        echo " в ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "department", array()), "html", null, true);
        echo "</span>
        <table>
            <tr>
              <td><span>Телефон</span></td>
              <td><span>";
        // line 22
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "phone", array()), "html", null, true);
        echo "</span></td>
            </tr>
            <tr>
              <td><span>Кабинет</span></td>
              <td><span>";
        // line 26
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "room", array()), "html", null, true);
        echo "</span></td>
            </tr>
            <tr>
              <td><span>Email</span></td>
              <td><span>";
        // line 30
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "email", array()), "html", null, true);
        echo "</span></td>
            </tr>
            <tr>
              <td><span>Дата рождения</span></td>
              <td><span>";
        // line 34
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "birth", array()), "html", null, true);
        echo "</span></td>
            </tr>
          </table>
      </div>
    </div>
    <div class=\"pasport_info skills\">
      <h3 class=\"tittle\">Навыки</h3>
      <ul class=\"skills_list\">
        ";
        // line 42
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "skills", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["skill"]) {
            echo "  
          <li>#";
            // line 43
            echo twig_escape_filter($this->env, $context["skill"], "html", null, true);
            echo "</li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['skill'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "      </ul>
    </div>
    <div class=\"pasport_info skills\">
      <h3 class=\"tittle\">Интересы</h3>
      <ul class=\"skills_list\">
        ";
        // line 50
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "interest", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["interest"]) {
            echo "  
          <li>#";
            // line 51
            echo twig_escape_filter($this->env, $context["interest"], "html", null, true);
            echo "</li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['interest'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 53
        echo "      </ul>
    </div>
    <div class=\"pasport_info work_descr\">
      <h3 class=\"tittle underline\">Описание деятельности</h3>
      <p>";
        // line 57
        echo twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "work_descr", array());
        echo "</p>
    </div>
  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "_main_profil.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  139 => 57,  133 => 53,  125 => 51,  119 => 50,  112 => 45,  104 => 43,  98 => 42,  87 => 34,  80 => 30,  73 => 26,  66 => 22,  57 => 18,  53 => 17,  46 => 12,  40 => 10,  36 => 8,  34 => 7,  29 => 5,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_main_profil.tpl", "D:\\Files\\doc\\BEP\\sites\\site-3.1\\templates\\_main_profil.tpl");
    }
}
