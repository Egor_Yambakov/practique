<?php

/* _header.tpl */
class __TwigTemplate_8c2fa9ca2ba03eaa7adf40668a9a8059d39450a650505758874ea237b4bfb9af extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"top_bar\">
  <div class=\"brand\">
    <div class=\"main_logo\">
      <a href=\"#\">
        <img src=\"images/ispring-logo-bold.png\" alt=\"ispring-logo\">
      </a>
    </div>
    <div class=\"main_tittle\">   
      <span>Онлайн-профиль</span>
    </div>
  </div>
  ";
        // line 12
        if (twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", array())) {
            // line 13
            echo "  <form class=\"search_bar\" id=\"top_search\" method=\"GET\" action=\"full_search.php\">
    <a href=\"#\" class=\"search_icon\" onClick=\"document.
    getElementById('top_search').submit();\">
      <img src=\"images/search-icon-images-5413.png\" alt=\"\">
    </a>
    <input type=\"text\" name=\"query\" id=\"search_box\" autocomplete=\"off\">
    <div class=\"search_advice\" id=\"search_advice\">
      <a class=\"tittle result_page_link\">Показать все результаты</a>
      <div id=\"result_names_list\">
      </div>
      <div class=\"result_skills\">
        <h4 class=\"tittle\">Навыки</h4>
        <ul id=\"result_skills_list\"></ul>
      </div>
      <div class=\"result_skills\">
        <h4 class=\"tittle\">Интересы</h4>
        <ul id=\"result_interests_list\"></ul>
      </div>
    </div>
  </form>
  <div class=\"login_btn\">
    <span>";
            // line 34
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "name", array()), "html", null, true);
            echo "</span>
    <img src=\"";
            // line 35
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "photo", array()), "html", null, true);
            echo "\" alt=\"\">
    <div class=\"dropdown_menu\">
      <a href=\"profil.php?id=";
            // line 37
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", array()), "html", null, true);
            echo "\">Мой профиль</a>
      <a href=\"profil.php?id=";
            // line 38
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", array()), "html", null, true);
            echo "&edit=true\">Редактировать</a>
      <a href=\"logout.php\">Выйти</a>
    </div>
  </div>
  ";
        }
        // line 43
        echo "  <div class=\"clear_all\"></div>
</div>";
    }

    public function getTemplateName()
    {
        return "_header.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 43,  74 => 38,  70 => 37,  65 => 35,  61 => 34,  38 => 13,  36 => 12,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_header.tpl", "D:\\Files\\doc\\BEP\\sites\\templates\\_header.tpl");
    }
}
