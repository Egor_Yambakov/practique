<?php

/* _content.html */
class __TwigTemplate_e6e32f30e5c74e88c7b008997c476fd4fb9ac022ad40c70ca773823383d29475 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html", "_content.html", 1);
        $this->blocks = array(
            'tittle' => array($this, 'block_tittle'),
            'main_content' => array($this, 'block_main_content'),
            'side_bar' => array($this, 'block_side_bar'),
            'content_bar' => array($this, 'block_content_bar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_tittle($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, ($context["page_name"] ?? null), "html", null, true);
    }

    // line 5
    public function block_main_content($context, array $blocks = array())
    {
        // line 6
        echo "  <div class=\"side_bar\">
    ";
        // line 7
        $this->displayBlock('side_bar', $context, $blocks);
        // line 9
        echo "  </div>

  <div class=\"content_bar\">
    ";
        // line 12
        $this->displayBlock('content_bar', $context, $blocks);
        // line 14
        echo "  </div>
";
    }

    // line 7
    public function block_side_bar($context, array $blocks = array())
    {
        // line 8
        echo "    ";
    }

    // line 12
    public function block_content_bar($context, array $blocks = array())
    {
        // line 13
        echo "    ";
    }

    public function getTemplateName()
    {
        return "_content.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 13,  68 => 12,  64 => 8,  61 => 7,  56 => 14,  54 => 12,  49 => 9,  47 => 7,  44 => 6,  41 => 5,  35 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_content.html", "D:\\Files\\doc\\BEP\\sites\\site-1.2\\templates\\_content.html");
    }
}
