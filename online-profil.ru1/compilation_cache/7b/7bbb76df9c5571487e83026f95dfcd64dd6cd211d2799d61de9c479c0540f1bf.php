<?php

/* base.html */
class __TwigTemplate_d8a407b44b8e5f2ff44a699121a49153a96326250b9d150c82535f85ddb6c8ba extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
            'top_container' => array($this, 'block_top_container'),
            'main_content' => array($this, 'block_main_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"ru\">
<head>
  <meta charset=\"UTF-8\">
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
  <link rel=\"stylesheet\" type=\"text/css\" href=\"/style.css\">
  <title>Онлайн-профиль</title>
</head>
<body>
  <div class=\"all_content\">
    <header class=\"top_container\">
      ";
        // line 12
        $this->displayBlock('top_container', $context, $blocks);
        // line 14
        echo "    </header>
    <main class=\"main\">
      ";
        // line 16
        $this->displayBlock('main_content', $context, $blocks);
        // line 18
        echo "    </main>
  </div>
  <script src=\"/js/jquery.js\" type=\"text/javascript\"></script>
  <script src=\"/js/main.js\" type=\"text/javascript\"></script>
</body>
</html>";
    }

    // line 12
    public function block_top_container($context, array $blocks = array())
    {
        // line 13
        echo "      ";
    }

    // line 16
    public function block_main_content($context, array $blocks = array())
    {
        // line 17
        echo "      ";
    }

    public function getTemplateName()
    {
        return "base.html";
    }

    public function getDebugInfo()
    {
        return array (  65 => 17,  62 => 16,  58 => 13,  55 => 12,  46 => 18,  44 => 16,  40 => 14,  38 => 12,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "base.html", "D:\\Files\\doc\\BEP\\sites\\templates\\base.html");
    }
}
