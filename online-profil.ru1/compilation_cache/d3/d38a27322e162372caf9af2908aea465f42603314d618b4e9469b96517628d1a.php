<?php

/* _employees_list.tpl */
class __TwigTemplate_ca35ff5fb08a0fab2ffc1f164fe5e8589590e51819c52b96e444a83f45652349 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"pasport_list\">
  <div class=\"main_search_tittle\">
    <h1>Результаты поиска</h1>
  </div>
  <form class=\"main_search\" id=\"main_search\" method=\"GET\" action=\"full_search.php\">
    <a href=\"#\" class=\"search_icon\" onClick=\"document.getElementById('main_search').submit();\">
      <img src=\"images/search-icon-images-5413.png\" alt=\"\">
    </a>
    <input value=\"";
        // line 9
        if ((twig_get_attribute($this->env, $this->source, ($context["dep_query"] ?? null), "department", array()) == "")) {
            echo "Сотрудники ";
        } else {
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["dep_query"] ?? null), "department", array()), "html", null, true);
            echo " ";
        }
        echo "(";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["dep_query"] ?? null), "count", array()), "html", null, true);
        echo ")\" type=\"text\" name=\"query\" class=\"main_search_box\" id=\"main_search_box\" autocomplete=\"off\">
    <div class=\"clear_search_btn\">
      <img src=\"images/exit.png\" alt=\"\">
    </div>
  </form>
  ";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["list"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
            // line 15
            echo "  <div class=\"pasport_block\">
    <a href=\"profil.php?id=";
            // line 16
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "id", array()), "html", null, true);
            echo "\" class=\"pasport_photo\">
      <img src=\"";
            // line 17
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "photo", array()), "html", null, true);
            echo "\" alt=\"\">
    </a>
    <div class=\"pasport_info\">
      <h3 class=\"tittle\">";
            // line 20
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "name", array()), "html", null, true);
            echo "</h3>
      <span>";
            // line 21
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "job_tittle", array()), "html", null, true);
            echo " в ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "department", array()), "html", null, true);
            echo "</span>
      <span>";
            // line 22
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "email", array()), "html", null, true);
            echo "</span>
      <span>кабинет ";
            // line 23
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "room", array()), "html", null, true);
            echo "</span>
      <a class=\"email_button\" href=\"mailto:";
            // line 24
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "email", array()), "html", null, true);
            echo "\" target=\"_blank\"></a>
    </div>
  </div>
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "_employees_list.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 28,  83 => 24,  79 => 23,  75 => 22,  69 => 21,  65 => 20,  59 => 17,  55 => 16,  52 => 15,  48 => 14,  33 => 9,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_employees_list.tpl", "D:\\Files\\doc\\BEP\\sites\\templates\\_employees_list.tpl");
    }
}
