<?php

/* _content.tpl */
class __TwigTemplate_cffc48a4a71b11e9b0932dcc161028bff952b1f30b1161024f8ba7fcd5f1b838 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"side_bar\">
  ";
        // line 2
        $this->loadTemplate("_side_bar.tpl", "_content.tpl", 2)->display($context);
        // line 3
        echo "</div>

<div class=\"content_bar\">
  ";
        // line 6
        $this->loadTemplate("_pasports_list.tpl", "_content.tpl", 6)->display($context);
        // line 7
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "_content.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 7,  33 => 6,  28 => 3,  26 => 2,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_content.tpl", "D:\\Files\\doc\\BEP\\sites\\site-3.1\\templates\\_content.tpl");
    }
}
