<?php

/* _news_base.tpl */
class __TwigTemplate_38f32ee4343a83b3b28f951646bd7b2afc4dfc8a14446db380a8a4b2bcda13dd extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html", "_news_base.tpl", 1);
        $this->blocks = array(
            'top_container' => array($this, 'block_top_container'),
            'main_content' => array($this, 'block_main_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_top_container($context, array $blocks = array())
    {
        // line 4
        echo "  ";
        $this->loadTemplate("_header.tpl", "_news_base.tpl", 4)->display($context);
    }

    // line 7
    public function block_main_content($context, array $blocks = array())
    {
        // line 8
        echo "<div class=\"main_content\">
  <div class=\"side_bar\">
    ";
        // line 10
        $this->loadTemplate("_side_bar.tpl", "_news_base.tpl", 10)->display($context);
        // line 11
        echo "  </div>

  <div class=\"main_content_bar\">
    ";
        // line 14
        $this->loadTemplate("_news_list.tpl", "_news_base.tpl", 14)->display($context);
        // line 15
        echo "    ";
        $this->loadTemplate("_search_filter.tpl", "_news_base.tpl", 15)->display($context);
        // line 16
        echo "  </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "_news_base.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 16,  57 => 15,  55 => 14,  50 => 11,  48 => 10,  44 => 8,  41 => 7,  36 => 4,  33 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_news_base.tpl", "D:\\Files\\doc\\BEP\\sites\\site-3.1\\templates\\_news_base.tpl");
    }
}
