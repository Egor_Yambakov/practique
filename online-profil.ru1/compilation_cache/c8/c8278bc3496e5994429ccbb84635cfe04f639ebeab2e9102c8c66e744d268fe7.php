<?php

/* _full_search_list.tpl */
class __TwigTemplate_9bba2cfa31021ddf84b572912d411bc4f73a5ab20890bd28ac47c3d77c67fff0 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"full_search_list\">
  <div class=\"main_search_tittle\">
    <h1>Результаты поиска</h1>
  </div>
  <form class=\"main_search\" id=\"main_search\" action=\"\">
    <a href=\"#\" class=\"search_icon\" onClick=\"document.getElementById('main_search').submit();\">
      <img src=\"images/search-icon-images-5413.png\" alt=\"\">
    </a>
    <input value=\"";
        // line 9
        echo twig_escape_filter($this->env, ($context["query"] ?? null), "html", null, true);
        echo " ";
        if ((twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "interests", array()) || twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "skills", array()))) {
            echo "(";
            echo twig_escape_filter($this->env, (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "interests", array())) + twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "skills", array()))), "html", null, true);
            echo ")";
        }
        echo "\" type=\"text\" name=\"query\" class=\"main_search_box\" id=\"main_search_box\" autocomplete=\"off\">
    <div class=\"clear_search_btn\">
      <img src=\"images/exit.png\" alt=\"\">
    </div>
  </form>
  ";
        // line 14
        if (twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "interests", array())) {
            // line 15
            echo "  <div class=\"main_search_tittle\">
    <h1>Интересы (";
            // line 16
            echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "interests", array())), "html", null, true);
            echo ")</h1>
  </div>
  <div class=\"result_blocks\">
    ";
            // line 19
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "interests", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
                // line 20
                echo "    <div class=\"pasport_block\">
      <a href=\"profil.php?id=";
                // line 21
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "id", array()), "html", null, true);
                echo "\" class=\"pasport_photo\">
        <img src=\"";
                // line 22
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "photo", array()), "html", null, true);
                echo "\" alt=\"\">
      </a>
      <div class=\"pasport_info\">
        <h3 class=\"tittle\"><a href=\"profil.php?id=";
                // line 25
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "id", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "name", array()), "html", null, true);
                echo "</a></h3>
        <span>";
                // line 26
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "job_tittle", array()), "html", null, true);
                echo "</span>
      </div>
    </div>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 30
            echo "  </div>
  ";
        } elseif (twig_get_attribute($this->env, $this->source,         // line 31
($context["list"] ?? null), "skills", array())) {
            // line 32
            echo "  <div class=\"main_search_tittle\">
    <h1>Навыки (";
            // line 33
            echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "skills", array())), "html", null, true);
            echo ")</h1>
  </div>
  <div class=\"result_blocks\">
    ";
            // line 36
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "skills", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
                // line 37
                echo "    <div class=\"pasport_block\">
      <a href=\"profil.php?id=";
                // line 38
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "id", array()), "html", null, true);
                echo "\" class=\"pasport_photo\">
        <img src=\"";
                // line 39
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "photo", array()), "html", null, true);
                echo "\" alt=\"\">
      </a>
      <div class=\"pasport_info\">
        <h3 class=\"tittle\"><a href=\"profil.php?id=";
                // line 42
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "id", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "name", array()), "html", null, true);
                echo "</a></h3>
        <span>";
                // line 43
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "job_tittle", array()), "html", null, true);
                echo "</span>
      </div>
    </div>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 47
            echo "  </div>
  ";
        } else {
            // line 49
            echo "    ";
            if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "query_data", array()), "names", array()))) {
                // line 50
                echo "      <div class=\"main_search_tittle\">
        <h1>Сотрудники (";
                // line 51
                echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "query_data", array()), "names", array())), "html", null, true);
                echo ")</h1>
      </div>
      <div class=\"result_blocks\">
        ";
                // line 54
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "query_data", array()), "names", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
                    // line 55
                    echo "        <div class=\"pasport_block\">
          <a href=\"profil.php?id=";
                    // line 56
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "id", array()), "html", null, true);
                    echo "\" class=\"pasport_photo\">
            <img src=\"";
                    // line 57
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "photo", array()), "html", null, true);
                    echo "\" alt=\"\">
          </a>
          <div class=\"pasport_info\">
            <h3 class=\"tittle\"><a href=\"profil.php?id=";
                    // line 60
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "id", array()), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "name", array()), "html", null, true);
                    echo "</a></h3>
            <span>";
                    // line 61
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "job_tittle", array()), "html", null, true);
                    echo "</span>
          </div>
        </div>
        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 65
                echo "      </div>
    ";
            }
            // line 67
            echo "    ";
            if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "query_data", array()), "skills", array()))) {
                // line 68
                echo "      <div class=\"main_search_tittle\">
        <h1>Навыки (";
                // line 69
                echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "query_data", array()), "skills", array())), "html", null, true);
                echo ")</h1>
      </div>
      <div class=\"result_skills_list\">
        ";
                // line 72
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "query_data", array()), "skills", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["skill"]) {
                    // line 73
                    echo "        <span><a href=\"full_search.php?skills_query=";
                    echo twig_escape_filter($this->env, twig_urlencode_filter(twig_get_attribute($this->env, $this->source, $context["skill"], "name", array())), "html", null, true);
                    echo "\">#";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["skill"], "name", array()), "html", null, true);
                    echo " (";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["skill"], "count", array()), "html", null, true);
                    echo ")</a></span>
        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['skill'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 75
                echo "      </div>
    ";
            }
            // line 77
            echo "    ";
            if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "query_data", array()), "interests", array()))) {
                // line 78
                echo "      <div class=\"main_search_tittle\">
        <h1>Интересы (";
                // line 79
                echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "query_data", array()), "interests", array())), "html", null, true);
                echo ")</h1>
      </div>
      <div class=\"result_skills_list\">
        ";
                // line 82
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "query_data", array()), "interests", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["interest"]) {
                    // line 83
                    echo "        <span><a href=\"full_search.php?interests_query=";
                    echo twig_escape_filter($this->env, twig_urlencode_filter(twig_get_attribute($this->env, $this->source, $context["interest"], "name", array())), "html", null, true);
                    echo "\">#";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["interest"], "name", array()), "html", null, true);
                    echo " (";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["interest"], "count", array()), "html", null, true);
                    echo ")</a></span>
        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['interest'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 85
                echo "      </div>
    ";
            }
            // line 87
            echo "    ";
            if ((( !twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "query_data", array()), "names", array())) &&  !twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "query_data", array()), "skills", array()))) &&  !twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "query_data", array()), "interests", array())))) {
                // line 88
                echo "      <div class=\"main_search_tittle\">
        <h1>Совпадений не обнаружено</h1>
      </div>
    ";
            }
            // line 92
            echo "  ";
        }
        // line 93
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "_full_search_list.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  269 => 93,  266 => 92,  260 => 88,  257 => 87,  253 => 85,  240 => 83,  236 => 82,  230 => 79,  227 => 78,  224 => 77,  220 => 75,  207 => 73,  203 => 72,  197 => 69,  194 => 68,  191 => 67,  187 => 65,  177 => 61,  171 => 60,  165 => 57,  161 => 56,  158 => 55,  154 => 54,  148 => 51,  145 => 50,  142 => 49,  138 => 47,  128 => 43,  122 => 42,  116 => 39,  112 => 38,  109 => 37,  105 => 36,  99 => 33,  96 => 32,  94 => 31,  91 => 30,  81 => 26,  75 => 25,  69 => 22,  65 => 21,  62 => 20,  58 => 19,  52 => 16,  49 => 15,  47 => 14,  33 => 9,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_full_search_list.tpl", "D:\\Files\\doc\\BEP\\sites\\templates\\_full_search_list.tpl");
    }
}
