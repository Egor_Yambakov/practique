<?
    require_once('require_config.php');    
    checkData($_COOKIE["id"], "index.php");
    $data = getSearchData($_GET, $_COOKIE["id"]);
    buildPage('_full_search_base.tpl',    array
                                        (
                                            "list"   => $data["employees_data"],
                                            "user"   => $data["user_data"],
                                            "filter" => $data["filter_data"],
                                            "query"  => $data["search_query"]
                                        ));