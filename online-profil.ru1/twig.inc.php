<?
    require_once ('C:/Users/Егор/vendor/autoload.php');

    function buildPage($page_name, $data_arr)
    {
        $loader = new Twig_Loader_Filesystem('templates');
        $twig = new Twig_Environment
                    (
                        $loader, 
                        array
                        (
                            'cache'       => 'compilation_cache',
                            'auto_reload' => true
                        )
                    );

        echo $twig->render
                    (
                        $page_name, 
                        $data_arr
                    );
    }

    function checkData($data, $err_location)
    {
        if (!isset($data) OR empty($data))
        {
            header("Location: ".$err_location);
            exit;
        };
    }
    
    function checkCookie($cookie, $accesLocation)
    {
        if (isset($cookie) and !empty($cookie))
        {
            header("Location: $accesLocation");
            exit();
        };
    }