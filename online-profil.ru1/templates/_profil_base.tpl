{% extends "base.html" %}

{% block top_container %}
  {% include "_header.tpl" %}
{% endblock %}

{% block main_content %}
<div class="main_content">
  <div class="side_bar">
    {% include "_side_bar.tpl" %}
  </div>

  <div class="content_bar">
    {% include "_profil_main.tpl" %}
  </div>
</div>
{% endblock %}