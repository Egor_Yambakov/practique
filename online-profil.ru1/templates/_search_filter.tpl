<div class="filter_list">
  <div class="department_block">
    <h3 class="tittle">Отделы</h3>
    <ul>
      {% for dep in filter.department %}
        <li><a href="employees.php?dep_query={{dep|url_encode}}">{{dep}}</a></li>
      {% endfor %}
    </ul>
  </div>
  <div class="filter_set">
    <h3 class="tittle">Навыки</h3>
    <ul>
      {% for skill in filter.skills %}
        <li><a href="full_search.php?skills_query={{skill.name|url_encode}}">#{{skill.name}} ({{skill.count}})</a></li>
      {% endfor %}
      <li id="unhideExtraSkills"><a href="#">···</a></li>
    </ul>
  </div>
  <div class="filter_set">
    <h3 class="tittle">Интересы</h3>
    <ul>
      {% for interest in filter.interests %}
        <li><a href="full_search.php?interests_query={{interest.name|url_encode}}">#{{interest.name}} ({{interest.count}})</a></li>
      {% endfor %}
      <li id="unhideExtraInterests"><a href="#">···</a></li>
    </ul>
  </div>
</div>