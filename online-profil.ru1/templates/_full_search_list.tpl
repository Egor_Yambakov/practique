<div class="full_search_list">
  <div class="main_search_tittle">
    <h1>Результаты поиска</h1>
  </div>
  <form class="main_search" id="main_search" action="">
    <a href="#" class="search_icon" onClick="document.getElementById('main_search').submit();">
      <img src="images/search-icon-images-5413.png" alt="">
    </a>
    <input value="{{query}} {% if list.interests or list.skills %}({{list.interests | length + list.skills | length}}){% endif %}" type="text" name="query" class="main_search_box" id="main_search_box" autocomplete="off">
    <div class="clear_search_btn">
      <img src="images/exit.png" alt="">
    </div>
  </form>
  {% if list.interests %}
  <div class="main_search_tittle">
    <h1>Интересы ({{list.interests | length}})</h1>
  </div>
  <div class="result_blocks">
    {% for user in list.interests %}
    <div class="pasport_block">
      <a href="profil.php?id={{user.id}}" class="pasport_photo">
        <img src="{{user.photo}}" alt="">
      </a>
      <div class="pasport_info">
        <h3 class="tittle"><a href="profil.php?id={{user.id}}">{{user.name}}</a></h3>
        <span>{{user.job_tittle}}</span>
      </div>
    </div>
    {% endfor %}
  </div>
  {% elseif  list.skills %}
  <div class="main_search_tittle">
    <h1>Навыки ({{list.skills | length}})</h1>
  </div>
  <div class="result_blocks">
    {% for user in list.skills %}
    <div class="pasport_block">
      <a href="profil.php?id={{user.id}}" class="pasport_photo">
        <img src="{{user.photo}}" alt="">
      </a>
      <div class="pasport_info">
        <h3 class="tittle"><a href="profil.php?id={{user.id}}">{{user.name}}</a></h3>
        <span>{{user.job_tittle}}</span>
      </div>
    </div>
    {% endfor %}
  </div>
  {% else %}
    {% if list.query_data.names | length %}
      <div class="main_search_tittle">
        <h1>Сотрудники ({{list.query_data.names | length}})</h1>
      </div>
      <div class="result_blocks">
        {% for user in list.query_data.names %}
        <div class="pasport_block">
          <a href="profil.php?id={{user.id}}" class="pasport_photo">
            <img src="{{user.photo}}" alt="">
          </a>
          <div class="pasport_info">
            <h3 class="tittle"><a href="profil.php?id={{user.id}}">{{user.name}}</a></h3>
            <span>{{user.job_tittle}}</span>
          </div>
        </div>
        {% endfor%}
      </div>
    {% endif %}
    {% if list.query_data.skills | length %}
      <div class="main_search_tittle">
        <h1>Навыки ({{list.query_data.skills | length}})</h1>
      </div>
      <div class="result_skills_list">
        {% for skill in list.query_data.skills %}
        <span><a href="full_search.php?skills_query={{skill.name|url_encode}}">#{{skill.name}} ({{skill.count}})</a></span>
        {% endfor%}
      </div>
    {% endif %}
    {% if list.query_data.interests | length %}
      <div class="main_search_tittle">
        <h1>Интересы ({{list.query_data.interests | length}})</h1>
      </div>
      <div class="result_skills_list">
        {% for interest in list.query_data.interests %}
        <span><a href="full_search.php?interests_query={{interest.name|url_encode}}">#{{interest.name}} ({{interest.count}})</a></span>
        {% endfor%}
      </div>
    {% endif %}
    {% if not list.query_data.names | length and not list.query_data.skills | length and not list.query_data.interests | length %}
      <div class="main_search_tittle">
        <h1>Совпадений не обнаружено</h1>
      </div>
    {% endif%}
  {% endif %}
</div>