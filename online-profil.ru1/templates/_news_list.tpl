<div class="full_search_list">
  {% if list.names | length %}
    <div class="main_search_tittle">
      <h1>Новые сотрудники ({{list.names | length}})</h1>
    </div>
    <div class="result_blocks new_users">
      {% for userName in list.names %}
      <div class="pasport_block">
        <a href="profil.php?id={{userName.id}}" class="pasport_photo">
          <img src="{{userName.photo}}" alt="">
        </a>
        <div class="pasport_info">
          <h3 class="tittle"><a href="profil.php?id={{userName.id}}">{{userName.name}}</a></h3>
          <span>{{userName.job_tittle}}</span>
        </div>
      </div>
      {% endfor%}
      <a class="show_all_btn" href="#">Показать всех</a>
    </div>
  {% endif %}
  {% if list.skills | length %}
    <div class="main_search_tittle">
      <h1>Новые навыки ({{list.skills | length}})</h1>
    </div>
    <div class="result_skills_list">
      {% for skill in list.skills %}
      <span><a href="full_search.php?skills_query={{skill.name}}">#{{skill.name}} ({{skill.count}})</a></span>
      {% endfor%}
    </div>
  {% endif %}
  {% if list.interests | length %}
    <div class="main_search_tittle">
      <h1>Новые интересы ({{list.interests | length}})</h1>
    </div>
    <div class="result_skills_list">
      {% for interest in list.interests %}
      <span><a href="full_search.php?interests_query={{interest.name}}">#{{interest.name}} ({{interest.count}})</a></span>
      {% endfor%}
    </div>
  {% endif %}
</div>