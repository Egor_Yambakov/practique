{% extends "base.html" %}

{% block top_container %}
  {% include "_header.tpl" %}
{% endblock %}

{% block main_content %}
<div class="main_content">
  <div class="side_bar">
    {% include "_side_bar.tpl" %}
  </div>

  <div class="main_content_bar">
    {% include "_full_search_list.tpl" %}
    {% include "_search_filter.tpl"%}
  </div>
</div>
{% endblock %}