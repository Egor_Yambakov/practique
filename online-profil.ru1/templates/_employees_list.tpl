<div class="pasport_list">
  <div class="main_search_tittle">
    <h1>Результаты поиска</h1>
  </div>
  <form class="main_search" id="main_search" method="GET" action="full_search.php">
    <a href="#" class="search_icon" onClick="document.getElementById('main_search').submit();">
      <img src="images/search-icon-images-5413.png" alt="">
    </a>
    <input value="{% if dep_query.department == '' %}Сотрудники {% else %}{{dep_query.department}} {% endif %}({{dep_query.count}})" type="text" name="query" class="main_search_box" id="main_search_box" autocomplete="off">
    <div class="clear_search_btn">
      <img src="images/exit.png" alt="">
    </div>
  </form>
  {% for row in list %}
  <div class="pasport_block">
    <a href="profil.php?id={{row.id}}" class="pasport_photo">
      <img src="{{row.photo}}" alt="">
    </a>
    <div class="pasport_info">
      <h3 class="tittle">{{row.name}}</h3>
      <span>{{row.job_tittle}} в {{row.department}}</span>
      <span>{{row.email}}</span>
      <span>кабинет {{row.room}}</span>
      <a class="email_button" href="mailto:{{row.email}}" target="_blank"></a>
    </div>
  </div>
  {% endfor %}
</div>