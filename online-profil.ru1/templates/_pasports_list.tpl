<ul class="pasports_list">
  <li>
    <div class="page_block">
      <div class="photo_block">
        <img src="./photos/no_avatar.png" alt="avatar">
      </div>
    </div>  
    <div class="page_block pass_block">
      <div class="pasport">
        <div class="main_info">
          <h2 class="name">Гейст Константин</h2>
          <span class="status">Программист в Отдел разработки ПО</span>
        </div>
        <div class="side_info">
          <table>
            <tr>
              <td class="tittle">Дата рождения</td>
              <td class="value">15 декабря 1989г.</td>
            </tr>
            <tr>
              <td class="tittle">Телефон</td>
              <td class="value">+7(926) 212-85-06</td>
            </tr>
            <tr>
              <td class="tittle">Кабинет</td>
              <td class="value">345</td>
            </tr>
            <tr>
              <td class="tittle">Email</td>
              <td class="value">konstantin.geyst@ispringsolutions.com</td>
            </tr>
            <tr>
              <td class="tittle">Рабочий график</td>
              <td class="value">пн-пт 10-19</td>
            </tr>
          </table>
        </div>
      </div>
    </div> 
  </li> 
</ul>