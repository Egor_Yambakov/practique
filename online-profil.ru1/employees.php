<?  
    require_once('require_config.php');
    checkData($_COOKIE["id"], "index.php");
    $data = getEmployeesData($_GET["dep_query"], $_COOKIE["id"]);
    buildPage('_employees_base.tpl',    array
                                        (
                                            "list"   => $data["employees_data"],
                                            "user"   => $data["user_data"],
                                            "filter" => $data["filter_data"],
                                            "dep_query"  => $data["dep_query"]
                                        ));