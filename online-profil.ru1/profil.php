<?
    require_once('require_config.php');
    checkData($_COOKIE["id"], "index.php");
    $id = $_COOKIE["id"];
    checkData($_GET["id"], "profil.php?id=$id");
    $data = getProfilData($_GET["id"], $id);
    buildPage('_profil_base.tpl',   array
                                    (
                                        "profil" => $data["profil_data"], 
                                        "user"   => $data["user_data"],
                                        "edit"   => $data["edit_data"]
                                    ));