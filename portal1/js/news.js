const MAX_RESULT_LIST_LENGHT = 4;

$(newsHandler);

function newsHandler()
{
    hideExtraResults($(".new_users"));
    hideShowAllBtn($(".show_all_btn"));
    hideHideAllBtn($(".hide_all_btn"));
}

function hideExtraResults(element)
{
    element.children(":gt(" + (MAX_RESULT_LIST_LENGHT - 1) + ")").hide();
}

function hideShowAllBtn(element)
{
    if (element.parent().children().length > MAX_RESULT_LIST_LENGHT + 1)
    {
        element.show();
        element.click(function() {
            $(this).parent().children().show();
            $(this).hide();
        });
    };
}

function hideHideAllBtn(element)
{
    element.hide();
    element.click(function() {
        $(this).parent().children().css("display", "");
        $(this).parent().children(".show_all_btn").show();
        $(this).hide();
    });

}