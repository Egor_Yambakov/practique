<?
    function getUserDataById($user_id)
    {
        $query = "SELECT * FROM `user` WHERE `id` = '$user_id'";
        return getDataRow($query);
    }

    function getFilterData()
    {
        $query_dep = "SELECT `name` FROM `department`";
        $query_skills = "SELECT * FROM `skills`";
        $query_interests = "SELECT * FROM `interest`";

        $data = array
                (
                    'department' => getDataArray($query_dep),
                    'skills'     => getSkills($query_skills),      
                    'interests'  => getInterests($query_interests)
                );
        return $data;
    }

    function getEmployeesData($dep_query)
    {
        $data = array();
        $query = "SELECT `id` FROM `department` WHERE `name` = '$dep_query'";
        $dep_data = getDataRow($query);
        if (!$dep_data)
        {
            $query = "SELECT * FROM `user`";
            $data['dep_query'] = DEP_QUERY_EMPTY_TITTLE;
        }
        else
        {
            $query = "SELECT * FROM `user` WHERE `department_id` = '".$dep_data["id"]."'";  
            $data['dep_query'] = $dep_query;      
        };
        $data['employees'] = getUsersData($query);
        return $data;
    }

    function getLoginData($login)
    {
        $login = getQuery($login);
        $query = "SELECT `user_id`, `user_password` FROM `login` WHERE `user_login` = '$login'";
        return getDataRow($query);
    }

    function getNewsData()
    {
        $delta_time = time() - (60 * 60 * 24 * NEWS_DAYS_PERIOD);

        $user_query = "SELECT * FROM `user` WHERE `reg_date` > $delta_time";
        $skills_query = "SELECT * FROM `skills` WHERE `reg_date` > $delta_time";
        $interests_query = "SELECT * FROM `interest` WHERE `reg_date` > $delta_time";

        $data = array
                (
                    'names'     => getUsersData($user_query),
                    'skills'    => getSkills($skills_query),
                    'interests' => getInterests($interests_query)
                );
        return $data;
    }

    function getProfilById($profil_id)
    {
        $profil_id = getQuery($profil_id);
        $query = "SELECT * FROM `user` WHERE `id` = '$profil_id'";
        return getFullUsersData($query)[0];
    }

    function getSearchData($query_arr)
    {
        $filter = '';
        $data = array();
        if (isset($query_arr["skills_query"]) AND !empty($query_arr["skills_query"]))
        {
            $filter = getQuery($query_arr["skills_query"]);
            $query = "SELECT * 
                      FROM `user` 
                      WHERE `id` 
                      IN (SELECT `user_id` 
                          FROM `user_skills` 
                          WHERE `skill_id` 
                          IN (SELECT `id` 
                              FROM `skills` 
                              WHERE `name` = \"".$filter."\"))";
            $data["skills"] = getUsersData($query);
        }
        if (isset($query_arr["interests_query"]) AND !empty($query_arr["interests_query"]))
        {
            $filter = getQuery($query_arr["interests_query"]);
            $query = "SELECT * 
                      FROM `user` 
                      WHERE `id` 
                      IN (SELECT `user_id` 
                          FROM `user_interest` 
                          WHERE `interest_id` 
                          IN (SELECT `id` 
                              FROM `interest` 
                              WHERE `name` = '".$filter."'))";
            $data["interests"] = getUsersData($query);
        }
        if (isset($query_arr["query"]) AND !empty($query_arr["query"]))
        {
            $filter = getQuotedQuery($query_arr["query"]);
            $data["query_data"] = getFullList($filter);
        };
        $data_arr = array
        (
            'query' => $filter,
            'data'  => $data
        );
        return $data_arr;
    }

    function getFullList($search_query)
    {
        $query_names = "SELECT * 
                        FROM `user` 
                        WHERE `id` IN (SELECT `user_id` 
                                       FROM `user_mirror` 
                                       WHERE MATCH(`description`) 
                                       AGAINST('".getFullSearchAnd($search_query)."' IN BOOLEAN MODE)>0)";
        $query_skills = "SELECT * FROM `skills` WHERE `name` REGEXP '".getFullSearchOr($search_query)."'";
        $query_interests = "SELECT * FROM `interest` WHERE `name` REGEXP '".getFullSearchOr($search_query)."'";
        $data = array
        (
            'names' => getUsersData($query_names),
            'skills' => getSkills($query_skills),
            'interests' => getInterests($query_interests)
        );
        return $data;
    }

    function getFullSearchAnd($query_string)
    {
        return preg_replace("/(?:^|\s)([^\s]+)/", "+(\\1*) ", $query_string);
    }

    function getFullSearchOr($query_string)
    {
        return preg_replace("/\s+/", '|', $query_string);
    }

    function getEditData()
    {
        $query_room = "SELECT * FROM `room`";
        $query_dep = "SELECT * FROM `department`";
        $query_job = "SELECT * FROM `job_tittle`";

        $data = array
        (
            'rooms'       => getDataArray($query_room),
            'departments' => getDataArray($query_dep),
            'job_tittles' => getDataArray($query_job)
        );
        return $data;
    }

    function getJobTittle($job_id)
    {
        $job_query = "SELECT `name` FROM `job_tittle` WHERE `id` = '$job_id'";
        return getDataRow($job_query)['name'];
    }

    function getDepTittle($dep_id)
    {
        $job_query = "SELECT `name` FROM `department` WHERE `id` = '$dep_id'";
        return getDataRow($job_query)['name'];
    }

    function getRoomNumber($room_id)
    {
        $job_query = "SELECT `number` FROM `room` WHERE `id` = '$room_id'";
        return getDataRow($job_query)['number'];
    }

    function getSkillsById($user_id)
    { 
        $skills = array();
        $id_query = "SELECT `skill_id` FROM `user_skills` WHERE `user_id` = '$user_id'";
        $skills_id = getDataArray($id_query);
        foreach ($skills_id as $id) {
            $skill_query = "SELECT `name` FROM `skills` WHERE `id` = '".$id['skill_id']."'";
            $skills[] = getDataRow($skill_query)['name'];
        };
        return $skills;
    }

    function getInterestById($user_id)
    { 
        $interests = array();
        $id_query = "SELECT `interest_id` FROM `user_interest` WHERE `user_id` = '$user_id'";
        $interests_id = getDataArray($id_query);
        foreach ($interests_id as $id) 
        {
            $interest_query = "SELECT `name` FROM `interest` WHERE `id` = '".$id['interest_id']."'";
            $interests[] = getDataRow($interest_query)['name'];
        };
        return $interests;
    }

    function getUsersData($user_query)
    {
        $result_arr = getDataArray($user_query);

        $users_data = array();
        foreach ($result_arr as $user)
        {
            $user['job_tittle'] = getJobTittle($user['job_tittle_id']);
            $user['department'] = getDepTittle($user['department_id']);
            $user['room'] = getRoomNumber($user['room_id']);
            $user['email'] = preg_split("/\s*\,\s*/", $user['email']);
            $users_data[] = $user;
        };
        return $users_data;
    }

    function getFullUsersData($user_query)
    {
        $result_arr = getDataArray($user_query);

        $users_data = array();
        foreach ($result_arr as $user)
        {
            $user['job_tittle'] = getJobTittle($user['job_tittle_id']);
            $user['department'] = getDepTittle($user['department_id']);
            $user['room'] = getRoomNumber($user['room_id']);
            $user['skills'] = getSkillsById($user['id']);
            $user['interests'] = getInterestById($user['id']);
            $user["birth"] = convertTime($user["birth"]);
            $user["phone"] = preg_split("/\s*\,\s*/", $user["phone"]);
            $user["email"] = preg_split("/\s*\,\s*/", $user["email"]);
            $users_data[] = $user;
        };
        return $users_data;
    }

    function getSkills($query)
    {
        $skills = array();
        $result_arr = getDataArray($query);

        foreach ($result_arr as $skill)
        {
            $q1 = "SELECT * FROM `user_skills` WHERE `skill_id` = '".$skill['id']."'";
            $skill['count'] = countRowData($q1);
            if ($skill['count'])
            {
                $skills[] = $skill; 
            };         
        };
        return $skills;
    }

    function getInterests($query)
    {
        $interests = array();
        $result_arr = getDataArray($query);

        foreach ($result_arr as $interest)
        {
            $q1 = "SELECT * FROM `user_interest` WHERE `interest_id` = '".$interest['id']."'";
            $interest['count'] = countRowData($q1);
            if ($interest['count'])
            {
                $interests[] = $interest; 
            };         
        };
        return $interests;
    }

    function convertTime($time)
    {
        return date("j", $time).getMonth(date("n", $time)).date("Y", $time)."г.";
    }

    function getMonth($month_id)
    {
        $months = array
        (
            " null ", 
            " января ", 
            " февраля ", 
            " марта ", 
            " aпреля ", 
            " мая ", 
            " июня ", 
            " июля ", 
            " aвгуста ", 
            " сентября ", 
            " октября ", 
            " ноября ", 
            " декабря "
        );
        return $months[$month_id];
    }

    function getLimitUser($limit)
    {
        $query = "SELECT * FROM `user` LIMIT $limit";
        return getUsersData($query);
    }