<?
    function getQuery($query)
    {
        global $g_dblink;
        return trim(mysqli_real_escape_string($g_dblink, $query));
    }

    function getQuotedQuery($query)
    {
        global $g_dblink;
        return trim(mysqli_real_escape_string($g_dblink, quotemeta($query)));
    }

    function getDataArray($query)
    {
        global $g_dblink;
        $data = array();
        $result = mysqli_query($g_dblink, $query);
        if ($result && mysqli_num_rows($result))
        {
            $data = mysqli_fetch_all($result, MYSQLI_ASSOC);
        };
        mysqli_free_result($result);
        return $data;
    }

    function getDataRow($query)
    {
        global $g_dblink;
        $row = array();
        $result = mysqli_query($g_dblink, $query);
        if ($result && mysqli_num_rows($result))
        {
            $row = mysqli_fetch_assoc($result);
        };
        mysqli_free_result($result);
        return $row;
    }

    function countRowData($query)
    {
        global $g_dblink;
        $count = 0;
        $result = mysqli_query($g_dblink, $query);
        if ($result)
        {
            $count = mysqli_num_rows($result);
        };
        mysqli_free_result($result);
        return $count; 
    }

    function makeQuery($query)
    {
        global $g_dblink;
        return mysqli_query($g_dblink, $query);
    }

    function getLastInsertedId()
    {
        global $g_dblink;
        return mysqli_insert_id($g_dblink);
    }