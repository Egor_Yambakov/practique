-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Июл 12 2018 г., 14:06
-- Версия сервера: 10.1.33-MariaDB
-- Версия PHP: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `ispring_users`
--

-- --------------------------------------------------------

--
-- Структура таблицы `department`
--
-- Создание: Июн 19 2018 г., 14:01
--

CREATE TABLE `department` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- ССЫЛКИ ТАБЛИЦЫ `department`:
--

--
-- Дамп данных таблицы `department`
--

INSERT INTO `department` (`id`, `name`) VALUES
(1, 'Дизайн'),
(3, 'Контроль качества'),
(4, 'Маркетинг'),
(5, 'Продажи'),
(2, 'Разработка ПО'),
(6, 'Техническая поддержка');

-- --------------------------------------------------------

--
-- Структура таблицы `interest`
--
-- Создание: Июл 09 2018 г., 12:07
--

CREATE TABLE `interest` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `reg_date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- ССЫЛКИ ТАБЛИЦЫ `interest`:
--

--
-- Дамп данных таблицы `interest`
--

INSERT INTO `interest` (`id`, `name`, `reg_date`) VALUES
(1, 'Лес', 1530186431),
(2, 'ДВП', 1530186431),
(3, 'Баня', 1530186431),
(5, 'Мотоциклы', 1530186431),
(6, 'Шахматы', 1530186431),
(7, 'Баскетбол', 1530186431),
(8, 'ПилотированиеСамолета', 1530186431),
(34, 'Руководство', 1531304880);

-- --------------------------------------------------------

--
-- Структура таблицы `job_tittle`
--
-- Создание: Июн 19 2018 г., 12:51
--

CREATE TABLE `job_tittle` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- ССЫЛКИ ТАБЛИЦЫ `job_tittle`:
--

--
-- Дамп данных таблицы `job_tittle`
--

INSERT INTO `job_tittle` (`id`, `name`) VALUES
(3, 'Программист'),
(1, 'Руководитель'),
(2, 'Стажер');

-- --------------------------------------------------------

--
-- Структура таблицы `login`
--
-- Создание: Июн 19 2018 г., 17:09
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `user_login` varchar(255) NOT NULL,
  `user_password` varchar(32) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- ССЫЛКИ ТАБЛИЦЫ `login`:
--   `user_id`
--       `user` -> `id`
--

--
-- Дамп данных таблицы `login`
--

INSERT INTO `login` (`id`, `user_login`, `user_password`, `user_id`) VALUES
(1, 'test', '202cb962ac59075b964b07152d234b70', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `room`
--
-- Создание: Июн 19 2018 г., 14:39
--

CREATE TABLE `room` (
  `id` int(11) NOT NULL,
  `number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- ССЫЛКИ ТАБЛИЦЫ `room`:
--

--
-- Дамп данных таблицы `room`
--

INSERT INTO `room` (`id`, `number`) VALUES
(2, 319),
(3, 321),
(1, 345);

-- --------------------------------------------------------

--
-- Структура таблицы `skills`
--
-- Создание: Июл 09 2018 г., 12:07
--

CREATE TABLE `skills` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `reg_date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- ССЫЛКИ ТАБЛИЦЫ `skills`:
--

--
-- Дамп данных таблицы `skills`
--

INSERT INTO `skills` (`id`, `name`, `reg_date`) VALUES
(1, 'CSS', 1530186370),
(2, 'PHP', 1530186370),
(3, 'PASCAL', 1530186370),
(4, 'JS', 1530186370),
(5, 'HTML', 1530186370),
(6, 'MySQL', 1530186370),
(7, 'NodeJS', 1530186370),
(8, 'Преподавание', 1530186370),
(9, 'ДизайнБилбордов', 1530186370),
(16, 'sss\r\n', 1531304880);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--
-- Создание: Июл 02 2018 г., 15:08
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `department_id` int(11) NOT NULL,
  `job_tittle_id` int(11) NOT NULL,
  `birth` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `work_descr` text NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `reg_date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- ССЫЛКИ ТАБЛИЦЫ `user`:
--   `department_id`
--       `department` -> `id`
--   `job_tittle_id`
--       `job_tittle` -> `id`
--   `room_id`
--       `room` -> `id`
--

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `email`, `photo`, `department_id`, `job_tittle_id`, `birth`, `room_id`, `work_descr`, `name`, `phone`, `reg_date`) VALUES
(1, 'test1@ispring.ru, test2@ispring.ru, test3@ispring.ru, test4@ispring.ru', './photos/no_avatar_m.png', 2, 3, 886453200, 2, 'Руководство команды Visuals\r\nАнализ исходного ТЗ и разбиение его на подзадачи\r\nРазработка и сопровождение приложений на javascript\r\nАнализ чужого программного кода\r\nПроектирование и реализация GUI.\r\nРуководство команды Visuals\r\nАнализ исходного ТЗ и разбиение его на подзадачи\r\nРазработка и сопровождение приложений на javascript\r\nАнализ чужого программного кода\r\nПроектирование и реализация GUI.', 'Иванов Иван', '+7(912) 212-14-01, +7(912) 212-14-02, +7(912) 212-14-03', 1530185594),
(2, 'admin@ispring.ru', './photos/no_avatar_w.png', 5, 2, 919112400, 1, 'Руководство команды Visuals\r\nАнализ исходного ТЗ и разбиение его на подзадачи\r\nРазработка и сопровождение приложений на javascript\r\nАнализ чужого программного кода\r\nПроектирование и реализация GUI.', 'Иванов Иван', '+7(912) 212-14-06', 1530185635),
(3, 'mail@ispring.ru, mail2@ispring.ru, mail3@ispring.ru', './photos/no_avatar_m.png', 6, 3, 647902800, 3, 'Руководство команды Visuals\r\nАнализ исходного ТЗ и разбиение его на подзадачи\r\nРазработка и сопровождение приложений на javascript\r\nАнализ чужого программного кода\r\nПроектирование и реализация GUI.', 'Иванов Иван', '+7(926) 488-75-06', 1530185635),
(4, 'andrey.ilyin@ispringsolutions.com', './photos/no_avatar_m.png', 2, 1, 650926800, 3, 'Анализ исходного ТЗ и разбиение его на подзадачи\r\nРазработка и сопровождение приложений на C++ во фреймворке Qt\r\nАнализ чужого программного кода\r\nНаписание документированного кода по действующему в отделе стандарту\r\nПроектирование и реализация GUI\r\nУчастие в испытаниях техники, показах\r\nОформление необходимой программной документации.', 'Ильин Андрей', '+7(964) 863 79 43', 1530185635),
(5, 'andrey.ilyin@ispringsolutions.com', './photos/no_avatar_w.png', 3, 1, 659998800, 1, 'Анализ исходного ТЗ и разбиение его на подзадачи\r\nРазработка и сопровождение приложений на C++ во фреймворке Qt\r\nАнализ чужого программного кода\r\nНаписание документированного кода по действующему в отделе стандарту\r\nПроектирование и реализация GUI\r\nУчастие в испытаниях техники, показах\r\nОформление необходимой программной документации.', 'Петров Пётр', '+7(964) 863 79 43', 1530273645);

--
-- Триггеры `user`
--
DELIMITER $$
CREATE TRIGGER `user_delete` AFTER DELETE ON `user` FOR EACH ROW DELETE FROM `user_mirror` 
WHERE `id` = OLD.`id`
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `user_insert` AFTER INSERT ON `user` FOR EACH ROW INSERT INTO `user_mirror` (`user_id`, `description`)
VALUES (NEW.`id`, 
       (SELECT LOWER(CONCAT_WS(' ', (SELECT `work_descr`
                                     FROM `user`
                                     WHERE `id` = NEW.`id`),
                               		LOWER(NEW.`name`),
                                    (SELECT LOWER(`name`) 
                                     FROM `job_tittle` 
                                     WHERE `id` IN (SELECT `job_tittle_id` 
                                                    FROM `user` 
                                                    WHERE `id` = NEW.`id`)),
                                    (SELECT 
                                     GROUP_CONCAT(`name` SEPARATOR ' ') 
                                     FROM `skills` 
                                     WHERE `id` IN (SELECT `skill_id` 
                                                    FROM `user_skills` 
                                                    WHERE `user_id` = NEW.`id`)),
                                    (SELECT 
                                     GROUP_CONCAT(`name` SEPARATOR ' ') 
                                     FROM `interest` 
                                     WHERE `id` IN (SELECT `interest_id` 
                                                    FROM `user_interest` 
                                                    WHERE `user_id` = NEW.`id`))))))
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `user_update` AFTER UPDATE ON `user` FOR EACH ROW BEGIN
        DELETE FROM `user_mirror` 
        WHERE `user_id` = NEW.`id`;

        INSERT INTO `user_mirror` (`user_id`, `description`)
		VALUES (NEW.`id`, 
       (SELECT LOWER(CONCAT_WS(' ', (SELECT `work_descr`
                                     FROM `user`
                                     WHERE `id` = NEW.`id`),
                               		LOWER(NEW.`name`),
                                    (SELECT LOWER(`name`) 
                                     FROM `job_tittle` 
                                     WHERE `id` IN (SELECT `job_tittle_id` 
                                                    FROM `user` 
                                                    WHERE `id` = NEW.`id`)),
                                    (SELECT 
                                     GROUP_CONCAT(`name` SEPARATOR ' ') 
                                     FROM `skills` 
                                     WHERE `id` IN (SELECT `skill_id` 
                                                    FROM `user_skills` 
                                                    WHERE `user_id` = NEW.`id`)),
                                    (SELECT 
                                     GROUP_CONCAT(`name` SEPARATOR ' ') 
                                     FROM `interest` 
                                     WHERE `id` IN (SELECT `interest_id` 
                                                    FROM `user_interest` 
                                                    WHERE `user_id` = NEW.`id`))))));
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблицы `user_interest`
--
-- Создание: Июн 19 2018 г., 14:06
--

CREATE TABLE `user_interest` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `interest_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- ССЫЛКИ ТАБЛИЦЫ `user_interest`:
--   `user_id`
--       `user` -> `id`
--   `interest_id`
--       `interest` -> `id`
--

--
-- Дамп данных таблицы `user_interest`
--

INSERT INTO `user_interest` (`id`, `user_id`, `interest_id`) VALUES
(3, 2, 2),
(4, 2, 3),
(5, 4, 7),
(6, 4, 5),
(9, 4, 6),
(10, 4, 1),
(11, 4, 3),
(12, 4, 2),
(14, 2, 8),
(15, 3, 8),
(381, 1, 1),
(382, 1, 34),
(383, 1, 2);

--
-- Триггеры `user_interest`
--
DELIMITER $$
CREATE TRIGGER `interest_insert` AFTER INSERT ON `user_interest` FOR EACH ROW UPDATE `user_mirror` 
    SET `description` = (SELECT LOWER(CONCAT_WS(' ', 
                                               (SELECT `work_descr` 
                                                FROM `user` 
                                                WHERE `id` = NEW.`user_id`), 
                                               (SELECT LOWER(`name`)
                                                FROM `user`
                                                WHERE `id` = NEW.`user_id`), 
                                               (SELECT LOWER(`name`) 
                                                FROM `job_tittle` 
                                                WHERE `id` IN (SELECT `job_tittle_id` 
                                                               FROM `user` 
                                                               WHERE `id` = NEW.`user_id`)), 
                                               (SELECT GROUP_CONCAT(`name` SEPARATOR ' ') 
                                                FROM `skills` 
                                                WHERE `id` IN (SELECT `skill_id` 
                                                               FROM `user_skills` 
                                                               WHERE `user_id` = NEW.`user_id`)), 
                                               (SELECT GROUP_CONCAT(`name` SEPARATOR ' ') 
                                                FROM `interest` 
                                                WHERE `id` = NEW.`interest_id`)))) 
	WHERE `user_id` = NEW.`user_id`
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблицы `user_mirror`
--
-- Создание: Июл 07 2018 г., 23:19
-- Последнее обновление: Июл 11 2018 г., 18:23
-- Последняя проверка: Июл 12 2018 г., 17:04
--

CREATE TABLE `user_mirror` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- ССЫЛКИ ТАБЛИЦЫ `user_mirror`:
--

--
-- Дамп данных таблицы `user_mirror`
--

INSERT INTO `user_mirror` (`id`, `user_id`, `description`) VALUES
(169, 2, 'руководство команды visuals\r\nанализ исходного тз и разбиение его на подзадачи\r\nразработка и сопровождение приложений на javascript\r\nанализ чужого программного кода\r\nпроектирование и реализация gui. иванов иван стажер дизайнбилбордов баня двп пилотированиесамолета'),
(170, 3, 'руководство команды visuals\r\nанализ исходного тз и разбиение его на подзадачи\r\nразработка и сопровождение приложений на javascript\r\nанализ чужого программного кода\r\nпроектирование и реализация gui. иванов иван программист дизайнбилбордов пилотированиесамолета'),
(171, 4, 'анализ исходного тз и разбиение его на подзадачи\r\nразработка и сопровождение приложений на c++ во фреймворке qt\r\nанализ чужого программного кода\r\nнаписание документированного кода по действующему в отделе стандарту\r\nпроектирование и реализация gui\r\nучастие в испытаниях техники, показах\r\nоформление необходимой программной документации. ильин андрей руководитель css html js mysql nodejs pascal php преподавание баня баскетбол двп лес мотоциклы шахматы'),
(172, 5, 'анализ исходного тз и разбиение его на подзадачи\r\nразработка и сопровождение приложений на c++ во фреймворке qt\r\nанализ чужого программного кода\r\nнаписание документированного кода по действующему в отделе стандарту\r\nпроектирование и реализация gui\r\nучастие в испытаниях техники, показах\r\nоформление необходимой программной документации. петров пётр руководитель'),
(268, 1, 'руководство команды visuals\r\nанализ исходного тз и разбиение его на подзадачи\r\nразработка и сопровождение приложений на javascript\r\nанализ чужого программного кода\r\nпроектирование и реализация gui.\r\nруководство команды visuals\r\nанализ исходного тз и разбиение его на подзадачи\r\nразработка и сопровождение приложений на javascript\r\nанализ чужого программного кода\r\nпроектирование и реализация gui. иванов иван программист css php pascal js html mysql nodejs преподавание sss\r\n двп лес руководство');

-- --------------------------------------------------------

--
-- Структура таблицы `user_skills`
--
-- Создание: Июл 09 2018 г., 12:06
--

CREATE TABLE `user_skills` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `skill_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- ССЫЛКИ ТАБЛИЦЫ `user_skills`:
--   `skill_id`
--       `skills` -> `id`
--   `user_id`
--       `user` -> `id`
--

--
-- Дамп данных таблицы `user_skills`
--

INSERT INTO `user_skills` (`id`, `user_id`, `skill_id`) VALUES
(3, 4, 1),
(4, 4, 5),
(5, 4, 4),
(6, 4, 6),
(7, 4, 7),
(8, 4, 3),
(9, 4, 2),
(10, 4, 8),
(11, 2, 9),
(12, 3, 9),
(94, 1, 1),
(95, 1, 5),
(96, 1, 4),
(97, 1, 6),
(98, 1, 7),
(99, 1, 3),
(100, 1, 2),
(101, 1, 8),
(102, 1, 16);

--
-- Триггеры `user_skills`
--
DELIMITER $$
CREATE TRIGGER `skill_insert` AFTER INSERT ON `user_skills` FOR EACH ROW UPDATE `user_mirror` 
    SET `description` = (SELECT LOWER(CONCAT_WS(' ', 
                                               (SELECT `work_descr` 
                                                FROM `user` 
                                                WHERE `id` = NEW.`user_id`), 
                                               (SELECT LOWER(`name`)
                                                FROM `user`
                                                WHERE `id` = NEW.`user_id`), 
                                               (SELECT LOWER(`name`) 
                                                FROM `job_tittle` 
                                                WHERE `id` IN (SELECT `job_tittle_id` 
                                                               FROM `user` 
                                                               WHERE `id` = NEW.`user_id`)), 
                                               (SELECT GROUP_CONCAT(`name` SEPARATOR ' ') 
                                                FROM `skills` 
                                                WHERE `id` = NEW.`skill_id`), 
                                               (SELECT GROUP_CONCAT(`name` SEPARATOR ' ') 
                                                FROM `interest` 
                                                WHERE `id` IN (SELECT `interest_id` 
                                                               FROM `user_interest` 
                                                               WHERE `user_id` = NEW.`user_id`))))) 
	WHERE `user_id` = NEW.`user_id`
$$
DELIMITER ;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `id` (`id`);

--
-- Индексы таблицы `interest`
--
ALTER TABLE `interest`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `job_tittle`
--
ALTER TABLE `job_tittle`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_login` (`user_login`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `number` (`number`);

--
-- Индексы таблицы `skills`
--
ALTER TABLE `skills`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `department_id` (`department_id`),
  ADD KEY `job_tittle_id` (`job_tittle_id`),
  ADD KEY `room_id` (`room_id`);

--
-- Индексы таблицы `user_interest`
--
ALTER TABLE `user_interest`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `interest_id` (`interest_id`);

--
-- Индексы таблицы `user_mirror`
--
ALTER TABLE `user_mirror`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`);
ALTER TABLE `user_mirror` ADD FULLTEXT KEY `description` (`description`);

--
-- Индексы таблицы `user_skills`
--
ALTER TABLE `user_skills`
  ADD PRIMARY KEY (`id`),
  ADD KEY `skill_id` (`skill_id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `department`
--
ALTER TABLE `department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `interest`
--
ALTER TABLE `interest`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT для таблицы `job_tittle`
--
ALTER TABLE `job_tittle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `room`
--
ALTER TABLE `room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `skills`
--
ALTER TABLE `skills`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `user_interest`
--
ALTER TABLE `user_interest`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=384;

--
-- AUTO_INCREMENT для таблицы `user_mirror`
--
ALTER TABLE `user_mirror`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=269;

--
-- AUTO_INCREMENT для таблицы `user_skills`
--
ALTER TABLE `user_skills`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `login`
--
ALTER TABLE `login`
  ADD CONSTRAINT `login_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `user_ibfk_2` FOREIGN KEY (`job_tittle_id`) REFERENCES `job_tittle` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `user_ibfk_3` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `user_interest`
--
ALTER TABLE `user_interest`
  ADD CONSTRAINT `user_interest_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `user_interest_ibfk_2` FOREIGN KEY (`interest_id`) REFERENCES `interest` (`id`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `user_skills`
--
ALTER TABLE `user_skills`
  ADD CONSTRAINT `user_skills_ibfk_1` FOREIGN KEY (`skill_id`) REFERENCES `skills` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `user_skills_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
