<?php

/* base.html */
class __TwigTemplate_a011746e563e8c4f1a3e008aeec849520a3f71296b9162f6f46e654e16023edf extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
            'top_container' => array($this, 'block_top_container'),
            'main_content' => array($this, 'block_main_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"ru\">
<head>
  <meta charset=\"UTF-8\">
  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
  <link rel=\"stylesheet\" type=\"text/css\" href=\"css/style.css\">
  <title>Онлайн-профиль</title>
</head>
<body>
  <div class=\"all_content\">
    <header class=\"top_container\">
      ";
        // line 13
        $this->displayBlock('top_container', $context, $blocks);
        // line 15
        echo "    </header>
    <main class=\"main\">
      ";
        // line 17
        $this->displayBlock('main_content', $context, $blocks);
        // line 19
        echo "    </main>
  </div>
</body>
</html>";
    }

    // line 13
    public function block_top_container($context, array $blocks = array())
    {
        // line 14
        echo "      ";
    }

    // line 17
    public function block_main_content($context, array $blocks = array())
    {
        // line 18
        echo "      ";
    }

    public function getTemplateName()
    {
        return "base.html";
    }

    public function getDebugInfo()
    {
        return array (  64 => 18,  61 => 17,  57 => 14,  54 => 13,  47 => 19,  45 => 17,  41 => 15,  39 => 13,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "base.html", "D:\\Files\\doc\\BEP\\sites\\online-profil.ru\\template\\base.html");
    }
}
