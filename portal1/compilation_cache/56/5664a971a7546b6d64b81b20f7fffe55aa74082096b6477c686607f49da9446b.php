<?php

/* _pasport_lists.tpl */
class __TwigTemplate_2c358ed7057d70faf8a834599903825b3672a9e963efc4f11c5e2a4d1db35255 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div>
  <h3>Навыки</h3>
  <ul>
    ";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["skills"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["skill"]) {
            // line 5
            echo "    <li>";
            // line 6
            echo twig_escape_filter($this->env, $context["skill"], "html", null, true);
            // line 7
            echo "</li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['skill'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 9
        echo "  </ul>
</div>
<div>
  <h3>Интересы</h3>
  <ul>
    ";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["hobbys"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["hobby"]) {
            // line 15
            echo "    <li>";
            // line 16
            echo twig_escape_filter($this->env, $context["hobby"], "html", null, true);
            // line 17
            echo "</li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['hobby'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "  </ul>
</div>";
    }

    public function getTemplateName()
    {
        return "_pasport_lists.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 19,  58 => 17,  56 => 16,  54 => 15,  50 => 14,  43 => 9,  36 => 7,  34 => 6,  32 => 5,  28 => 4,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_pasport_lists.tpl", "D:\\Files\\doc\\BEP\\sites\\site-1.2\\templates\\_pasport_lists.tpl");
    }
}
