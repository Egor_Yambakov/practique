<?php

/* _search_filter.tpl */
class __TwigTemplate_6b9d745301bb63953fa5fb9055e6e28061f9269bb98403bf4e93c49724e6e924 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"filter_list box_shadow\">
  <div class=\"department_block\">
    <h3 class=\"tittle\">Отделы</h3>
    <ul>
      ";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["filter"] ?? null), "department", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["dep"]) {
            // line 6
            echo "        <li class=\"transition\"><a href=\"employees.php?dep_query=";
            echo twig_escape_filter($this->env, twig_urlencode_filter(twig_get_attribute($this->env, $this->source, $context["dep"], "name", array())), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["dep"], "name", array()), "html", null, true);
            echo "</a></li>
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dep'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 8
        echo "    </ul>
  </div>
  <div class=\"filter tag_set\">
    <h3 class=\"tittle\">Навыки</h3>
    <ul>
      ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["filter"] ?? null), "skills", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["skill"]) {
            // line 14
            echo "        <li><a href=\"full_search.php?skills_query=";
            echo twig_escape_filter($this->env, twig_urlencode_filter(twig_get_attribute($this->env, $this->source, $context["skill"], "name", array())), "html", null, true);
            echo "\">#";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["skill"], "name", array()), "html", null, true);
            echo "</a> (";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["skill"], "count", array()), "html", null, true);
            echo ")</li>
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['skill'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "    </ul>
    <a class=\"show_tags\">показать все</a>
    <a class=\"hide_tags\">скрыть</a>
  </div>
  <div class=\"filter tag_set\">
    <h3 class=\"tittle\">Интересы</h3>
    <ul>
      ";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["filter"] ?? null), "interests", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["interest"]) {
            // line 24
            echo "        <li><a href=\"full_search.php?interests_query=";
            echo twig_escape_filter($this->env, twig_urlencode_filter(twig_get_attribute($this->env, $this->source, $context["interest"], "name", array())), "html", null, true);
            echo "\">#";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["interest"], "name", array()), "html", null, true);
            echo "</a> (";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["interest"], "count", array()), "html", null, true);
            echo ")</li>
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['interest'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "    </ul>
    <a class=\"show_tags\">показать все</a>
    <a class=\"hide_tags\">скрыть</a>
  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "_search_filter.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 26,  81 => 24,  77 => 23,  68 => 16,  55 => 14,  51 => 13,  44 => 8,  33 => 6,  29 => 5,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_search_filter.tpl", "D:\\Files\\doc\\BEP\\sites\\portal\\template\\_search_filter.tpl");
    }
}
