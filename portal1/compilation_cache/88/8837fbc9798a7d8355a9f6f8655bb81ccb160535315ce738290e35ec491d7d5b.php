<?php

/* _full_search_container.tpl */
class __TwigTemplate_ea288f6b6fdf4a3c9558fb40f592ac20d1359eb5b0d9e2e162c31b0107afae1c extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"main_content\">
  <div class=\"side_bar\">
    ";
        // line 3
        $this->loadTemplate("_side_bar.tpl", "_full_search_container.tpl", 3)->display($context);
        // line 4
        echo "  </div>

  <div class=\"main_content_bar\">
    ";
        // line 7
        $this->loadTemplate("_full_search_list.tpl", "_full_search_container.tpl", 7)->display($context);
        // line 8
        echo "    ";
        $this->loadTemplate("_search_filter.tpl", "_full_search_container.tpl", 8)->display($context);
        // line 9
        echo "  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "_full_search_container.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 9,  36 => 8,  34 => 7,  29 => 4,  27 => 3,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_full_search_container.tpl", "D:\\Files\\doc\\BEP\\sites\\site-3.1\\templates\\_full_search_container.tpl");
    }
}
