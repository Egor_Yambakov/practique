<?php

/* _pasport_main.tpl */
class __TwigTemplate_6d84f6474490640681050aea2f750b2b7986aa0280ef4f9a5b1c465df57dc1c6 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h2 class=\"page_name red_border\">";
        echo twig_escape_filter($this->env, ($context["page_name"] ?? null), "html", null, true);
        echo "</h2>
<h4 class=\"info_header\">Работа</h4>
<ul class=\"record\">
  <li class=\"department\">
    <span class=\"field\">Отделение: </span>
    <span class=\"note\">";
        // line 6
        echo twig_escape_filter($this->env, ($context["department"] ?? null), "html", null, true);
        echo "</span>
  </li>
  <li class=\"post\">
    <span class=\"field\">Должность: </span>
    <span class=\"note\">";
        // line 10
        echo twig_escape_filter($this->env, ($context["post"] ?? null), "html", null, true);
        echo "</span>
  </li>
</ul>
<h4 class=\"info_header\">Контакты</h4>
<ul class=\"record\">
  <li class=\"phone\">
    <span class=\"field\">Телефон: </span>
    <span class=\"note\">";
        // line 17
        echo twig_escape_filter($this->env, ($context["phone_number"] ?? null), "html", null, true);
        echo "</span>
  </li>
  <li class=\"mail\">
    <span class=\"field\">Почта: </span>
    <a href=\"mailto:";
        // line 21
        echo twig_escape_filter($this->env, ($context["email"] ?? null), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, ($context["email"] ?? null), "html", null, true);
        echo "</a>
  </li>
</ul>
<h4 class=\"info_header\">Чем занимается в компании</h4>
<ul class=\"record\">
  <li class=\"field\">
    <p>";
        // line 28
        echo twig_escape_filter($this->env, ($context["duties_text"] ?? null), "html", null, true);
        // line 29
        echo "</p>
  </li>
</ul>";
    }

    public function getTemplateName()
    {
        return "_pasport_main.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 29,  67 => 28,  56 => 21,  49 => 17,  39 => 10,  32 => 6,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_pasport_main.tpl", "D:\\Files\\doc\\BEP\\sites\\site-1.2\\templates\\_pasport_main.tpl");
    }
}
