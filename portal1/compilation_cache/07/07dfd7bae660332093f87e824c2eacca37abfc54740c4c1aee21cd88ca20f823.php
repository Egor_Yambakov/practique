<?php

/* _side_bar.tpl */
class __TwigTemplate_dc0c945743df3e6acef806942027873535da68f00ab7bd81622f7cd2352d7614 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<ul>
  <li><a href=\"profil.php?id=";
        // line 2
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", array()), "html", null, true);
        echo "\">Моя страница</a></li>
  <li><a href=\"news.php\">Новости</a></li>
  <li><a href=\"employees.php\">Сотрудники</a></li>
  <li><a href=\"#\">Библиотека</a></li>
  <li><a href=\"#\">Столовая</a></li>
</ul>";
    }

    public function getTemplateName()
    {
        return "_side_bar.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 2,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_side_bar.tpl", "D:\\Files\\doc\\BEP\\sites\\site-3.1\\templates\\_side_bar.tpl");
    }
}
