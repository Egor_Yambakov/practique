<?php

/* _pasport.tpl */
class __TwigTemplate_b774f97ad821c3a382604ac65ac7d392188e6e4c45baadeaa38ad90ecb992ab9 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"side_column\">
  <div class=\"page_block\">
    ";
        // line 3
        $this->loadTemplate("_foto_block.tpl", "_pasport.tpl", 3)->display($context);
        // line 4
        echo "  </div>

  <div class=\"page_block secondary_info\">
    ";
        // line 7
        $this->loadTemplate("_pasport_lists.tpl", "_pasport.tpl", 7)->display($context);
        // line 8
        echo "  </div>
</div>

<div class=\"main_column page_block\">
  ";
        // line 12
        $this->loadTemplate("_pasport_main.tpl", "_pasport.tpl", 12)->display($context);
        // line 13
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "_pasport.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  44 => 13,  42 => 12,  36 => 8,  34 => 7,  29 => 4,  27 => 3,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_pasport.tpl", "D:\\Files\\doc\\BEP\\sites\\site-1.2\\templates\\_pasport.tpl");
    }
}
