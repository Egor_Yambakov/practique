<?php

/* _foto_block.tpl */
class __TwigTemplate_8869d5dd206224a3b277dac65687d1815aa88bef9b92c2152de727e205ff3863 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"page_foto\">
  <img src=\"";
        // line 2
        echo twig_escape_filter($this->env, ($context["page_foto"] ?? null), "html", null, true);
        echo "\" alt=\"avatar\">
</div>

<div class=\"action\">
  <a href=\"#\">Отправить сообщение</a>
</div>";
    }

    public function getTemplateName()
    {
        return "_foto_block.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 2,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_foto_block.tpl", "D:\\Files\\doc\\BEP\\sites\\site-1.2\\templates\\_foto_block.tpl");
    }
}
