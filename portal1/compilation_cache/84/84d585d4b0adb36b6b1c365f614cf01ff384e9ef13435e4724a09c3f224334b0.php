<?php

/* _profil_main.twig */
class __TwigTemplate_a3d3773c21927531fdb14e55007aef9365b45362e887cd33eb295d549356fcfa extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"profil_container box_shadow\">
  <div class=\"side_block\">
    <div class=\"profil_side\">
      <div class=\"profil_photo\" style=\"background-image:url(";
        // line 4
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "photo", array()), "html", null, true);
        echo ")\"></div>
      ";
        // line 5
        if ((twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "id", array()) == twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", array()))) {
            // line 6
            echo "        <a class=\"button reversed\" id=\"edit_btn\">Редактировать</a>
      ";
        } else {
            // line 8
            echo "        <a class=\"button\" href=\"mailto:";
            echo twig_escape_filter($this->env, (($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5 = twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "email", array())) && is_array($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5) || $__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5 instanceof ArrayAccess ? ($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5[0] ?? null) : null), "html", null, true);
            echo "\" target=\"_blank\">Написать собщение</a>
      ";
        }
        // line 10
        echo "      <div class=\"pasport_info skills tag_set\">
        <h3 class=\"tittle\">Навыки</h3>
        <ul class=\"skills_list\">
          ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "skills", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["skill"]) {
            echo "  
            <li><a href=\"full_search.php?skills_query=";
            // line 14
            echo twig_escape_filter($this->env, twig_urlencode_filter($context["skill"]), "html", null, true);
            echo "\">#";
            echo twig_escape_filter($this->env, $context["skill"], "html", null, true);
            echo "</a></li>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['skill'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "        </ul>
        <a class=\"show_btn\">показать все</a>
        <a class=\"hide_btn\">скрыть</a>
      </div>
      <div class=\"pasport_info skills tag_set\">
        <h3 class=\"tittle\">Интересы</h3>
        <ul class=\"skills_list\">
          ";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "interests", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["interest"]) {
            echo "  
            <li><a href=\"full_search.php?interests_query=";
            // line 24
            echo twig_escape_filter($this->env, twig_urlencode_filter($context["interest"]), "html", null, true);
            echo "\">#";
            echo twig_escape_filter($this->env, $context["interest"], "html", null, true);
            echo "</a></li>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['interest'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "        </ul>
        <a class=\"show_btn\">показать все</a>
        <a class=\"hide_btn\">скрыть</a>
      </div>
    </div>
  </div>
  <div class=\"main_block\">
    <div id=\"profil_information\">
      <div class=\"pasport_block\">
        <div class=\"pasport_info\">
          <h3 class=\"tittle\">";
        // line 36
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "name", array()), "html", null, true);
        echo "</h3>
          <span class=\"status\">";
        // line 37
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "job_tittle", array()), "html", null, true);
        echo " в ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "department", array()), "html", null, true);
        echo "</span>
          <table>
            <tr>
              <td><span>Телефон</span></td>
              <td>
                ";
        // line 42
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "phone", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["phone"]) {
            // line 43
            echo "                <span>";
            echo twig_escape_filter($this->env, $context["phone"], "html", null, true);
            echo "</span><br>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['phone'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "              </td>
            </tr>
            <tr>
              <td><span>Кабинет</span></td>
              <td><span>";
        // line 49
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "room", array()), "html", null, true);
        echo "</span></td>
            </tr>
            <tr>
              <td><span>Email</span></td>
              <td>
                ";
        // line 54
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "email", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["email"]) {
            // line 55
            echo "                <a href=\"mailto:";
            echo twig_escape_filter($this->env, $context["email"], "html", null, true);
            echo "\" target=\"_blank\">";
            echo twig_escape_filter($this->env, $context["email"], "html", null, true);
            echo "</a><br>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['email'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 57
        echo "              </td>
            </tr>
            <tr>
              <td><span>Дата рождения</span></td>
              <td><span>";
        // line 61
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "birth", array()), "html", null, true);
        echo "</span></td>
            </tr>
          </table>
        </div>
      </div>
      <div class=\"pasport_info work_descr\">
        <h3 class=\"tittle underline\">Описание деятельности</h3>
        <p>";
        // line 68
        echo nl2br(twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "work_descr", array()), "html", null, true));
        echo "</p>
        <a class=\"show_btn\">показать все</a>
        <a class=\"hide_btn\">скрыть</a>
      </div>
    </div>

    ";
        // line 74
        if ((twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "id", array()) == twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", array()))) {
            // line 75
            echo "    <form class=\"edit_profil\">
      <div class=\"pasport_info\">
        <h3 class=\"tittle\">Редактирование профиля</h3>
        <span class=\"status\"></span>
        <label>
          <span>Отдел</span>
          <select name=\"department\">
          ";
            // line 82
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["edit"] ?? null), "departments", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["dep"]) {
                // line 83
                echo "            <option value=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["dep"], "name", array()), "html", null, true);
                echo "\" ";
                if ((twig_get_attribute($this->env, $this->source, $context["dep"], "name", array()) == twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "department", array()))) {
                    echo "selected";
                }
                echo ">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["dep"], "name", array()), "html", null, true);
                echo "</option>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dep'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 85
            echo "          </select>
        </label>
        <label>
          <span>Должность</span>
          <select name=\"job_tittle\">
          ";
            // line 90
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["edit"] ?? null), "job_tittles", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["job"]) {
                // line 91
                echo "            <option value=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["job"], "name", array()), "html", null, true);
                echo "\" ";
                if ((twig_get_attribute($this->env, $this->source, $context["job"], "name", array()) == twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "job_tittle", array()))) {
                    echo "selected";
                }
                echo ">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["job"], "name", array()), "html", null, true);
                echo "</option>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['job'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 93
            echo "          </select>
        </label>
        <label>
          <span>Телефон</span>
          <input name=\"phone\" type=\"text\" value=\"";
            // line 97
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "phone", array()));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["phone"]) {
                echo twig_escape_filter($this->env, $context["phone"], "html", null, true);
                if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "last", array())) {
                    echo ", ";
                }
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['phone'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo "\">
        </label>
        <label>
          <span>Кабинет</span>
          <select name=\"room\">
          ";
            // line 102
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["edit"] ?? null), "rooms", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["room"]) {
                // line 103
                echo "            <option value=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["room"], "number", array()), "html", null, true);
                echo "\" ";
                if ((twig_get_attribute($this->env, $this->source, $context["room"], "number", array()) == twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "room", array()))) {
                    echo "selected";
                }
                echo ">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["room"], "number", array()), "html", null, true);
                echo "</option>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['room'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 105
            echo "          </select>
        </label>
        <label>
          <span>Email</span>
          <input name=\"email\" type=\"text\" value=\"";
            // line 109
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "email", array()));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["email"]) {
                echo twig_escape_filter($this->env, $context["email"], "html", null, true);
                if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "last", array())) {
                    echo ", ";
                }
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['email'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo "\">
        </label>
        <label class=\"textarea\">
          <h3>Навыки</h3>
          <textarea id=\"edit_skills\" name=\"skills\">";
            // line 113
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "skills", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["skill"]) {
                echo "#";
                echo twig_escape_filter($this->env, $context["skill"], "html", null, true);
                echo "   ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['skill'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo "</textarea>
        </label>
        <label class=\"textarea\">
          <h3>Интересы</h3>
          <textarea id=\"edit_interests\" name=\"interests\">";
            // line 117
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "interests", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["interest"]) {
                echo "#";
                echo twig_escape_filter($this->env, $context["interest"], "html", null, true);
                echo "   ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['interest'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo "</textarea>
        </label>
        <label class=\"textarea\">
          <h3>Описание деятельности</h3>
          <textarea name=\"work_descr\">";
            // line 121
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "work_descr", array()));
            echo "</textarea>
        </label>
        <a id=\"save_changes\" class=\"button\">Сохранить изменения</a>
      </div>
    </form>
    ";
        }
        // line 127
        echo "  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "_profil_main.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  388 => 127,  379 => 121,  363 => 117,  347 => 113,  309 => 109,  303 => 105,  288 => 103,  284 => 102,  245 => 97,  239 => 93,  224 => 91,  220 => 90,  213 => 85,  198 => 83,  194 => 82,  185 => 75,  183 => 74,  174 => 68,  164 => 61,  158 => 57,  147 => 55,  143 => 54,  135 => 49,  129 => 45,  120 => 43,  116 => 42,  106 => 37,  102 => 36,  90 => 26,  80 => 24,  74 => 23,  65 => 16,  55 => 14,  49 => 13,  44 => 10,  38 => 8,  34 => 6,  32 => 5,  28 => 4,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_profil_main.twig", "D:\\Files\\doc\\BEP\\sites\\portal\\template\\_profil_main.twig");
    }
}
