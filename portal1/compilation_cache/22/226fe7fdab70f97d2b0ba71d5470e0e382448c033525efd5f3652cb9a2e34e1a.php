<?php

/* _news_base.twig */
class __TwigTemplate_eb17aae1c4b2f53c87771d3d7f1d305b2510a5b20f955a90d8d3622cd790b836 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html", "_news_base.twig", 1);
        $this->blocks = array(
            'top_container' => array($this, 'block_top_container'),
            'main_content' => array($this, 'block_main_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_top_container($context, array $blocks = array())
    {
        // line 4
        echo "  ";
        $this->loadTemplate("_header.twig", "_news_base.twig", 4)->display($context);
    }

    // line 7
    public function block_main_content($context, array $blocks = array())
    {
        // line 8
        echo "<div class=\"main_content\">
  <div class=\"side_bar\">
    ";
        // line 10
        $this->loadTemplate("_side_bar.twig", "_news_base.twig", 10)->display($context);
        // line 11
        echo "  </div>

  <div class=\"main_content_bar\">
    ";
        // line 14
        $this->loadTemplate("_news_list.twig", "_news_base.twig", 14)->display($context);
        // line 15
        echo "    ";
        $this->loadTemplate("_search_filter.twig", "_news_base.twig", 15)->display($context);
        // line 16
        echo "  </div>
</div>
<script src=\"/js/jquery.js\" type=\"text/javascript\"></script>
<script src=\"/js/header.js\" type=\"text/javascript\"></script>
<script src=\"/js/news.js\" type=\"text/javascript\"></script>
<script src=\"/js/filter.js\" type=\"text/javascript\"></script>
";
    }

    public function getTemplateName()
    {
        return "_news_base.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 16,  57 => 15,  55 => 14,  50 => 11,  48 => 10,  44 => 8,  41 => 7,  36 => 4,  33 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_news_base.twig", "D:\\Files\\doc\\BEP\\sites\\portal\\template\\_news_base.twig");
    }
}
