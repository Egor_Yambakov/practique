<?php

/* _login.tpl */
class __TwigTemplate_197e155c11dde23858eab8199d7aac9912c89bf68dca61e235efb67acb329449 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"login_form_container box_shadow\">
  <h2>Вход в корпоративный портал iSpring</h2>
  <span class=\"err_text\">";
        // line 3
        echo twig_escape_filter($this->env, ($context["err_text"] ?? null), "html", null, true);
        echo "</span>
  <form id=\"login_form\" method=\"POST\">
    <label>Логин<input type=\"text\" name=\"login\"></label>
    <label>Пароль<input type=\"password\" name=\"password\"></label>
    <input type=\"submit\" name=\"submit\" value=\"Войти\">
  </form>
</div>";
    }

    public function getTemplateName()
    {
        return "_login.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_login.tpl", "D:\\Files\\doc\\BEP\\sites\\online-profil.ru\\template\\_login.tpl");
    }
}
