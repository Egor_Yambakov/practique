<?php

/* _news_list.tpl */
class __TwigTemplate_249724c2699350279b96d3603788ebd197b8fe95e287139fcfcb3c425a225d01 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"full_search_list\">
  ";
        // line 2
        if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "names", array()))) {
            // line 3
            echo "    <div class=\"main_search_tittle border_top border_bottom\">
      <h1>Новые сотрудники (";
            // line 4
            echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "names", array())), "html", null, true);
            echo ")</h1>
    </div>
    <div class=\"result_blocks new_users\">
      ";
            // line 7
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "names", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["userName"]) {
                // line 8
                echo "      <div class=\"pasport_block\">
        <a href=\"profil.php?id=";
                // line 9
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["userName"], "id", array()), "html", null, true);
                echo "\" class=\"pasport_photo\" style=\"background-image:url(";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["userName"], "photo", array()), "html", null, true);
                echo ")\"></a>
        <div class=\"pasport_info\">
          <h3 class=\"tittle\"><a href=\"profil.php?id=";
                // line 11
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["userName"], "id", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["userName"], "name", array()), "html", null, true);
                echo "</a></h3>
          <span>";
                // line 12
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["userName"], "job_tittle", array()), "html", null, true);
                echo "</span>
        </div>
      </div>
      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['userName'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 16
            echo "      <a class=\"show_all_btn\" href=\"#\">Показать всех</a>
      <a class=\"hide_all_btn\" href=\"#\">Скрыть</a>
    </div>
  ";
        }
        // line 20
        echo "  ";
        if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "skills", array()))) {
            // line 21
            echo "    <div class=\"main_search_tittle border_top border_bottom\">
      <h1>Новые навыки (";
            // line 22
            echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "skills", array())), "html", null, true);
            echo ")</h1>
    </div>
    <div class=\"result_skills_list tag_set\">
      <ul>
        ";
            // line 26
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "skills", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["skill"]) {
                // line 27
                echo "        <li>
          <a href=\"full_search.php?skills_query=";
                // line 28
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["skill"], "name", array()), "html", null, true);
                echo "\">#";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["skill"], "name", array()), "html", null, true);
                echo "</a> (";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["skill"], "count", array()), "html", null, true);
                echo ")
        </li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['skill'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 31
            echo "      </ul>
    </div>
  ";
        }
        // line 34
        echo "  ";
        if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "interests", array()))) {
            // line 35
            echo "    <div class=\"main_search_tittle border_top border_bottom\">
      <h1>Новые интересы (";
            // line 36
            echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "interests", array())), "html", null, true);
            echo ")</h1>
    </div>
    <div class=\"result_skills_list tag_set\">
      <ul>
      ";
            // line 40
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "interests", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["interest"]) {
                // line 41
                echo "        <li>
          <a href=\"full_search.php?interests_query=";
                // line 42
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["interest"], "name", array()), "html", null, true);
                echo "\">#";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["interest"], "name", array()), "html", null, true);
                echo "</a> (";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["interest"], "count", array()), "html", null, true);
                echo ")
        </li>
      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['interest'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 45
            echo "      </ul>
    </div>
  ";
        }
        // line 48
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "_news_list.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  149 => 48,  144 => 45,  131 => 42,  128 => 41,  124 => 40,  117 => 36,  114 => 35,  111 => 34,  106 => 31,  93 => 28,  90 => 27,  86 => 26,  79 => 22,  76 => 21,  73 => 20,  67 => 16,  57 => 12,  51 => 11,  44 => 9,  41 => 8,  37 => 7,  31 => 4,  28 => 3,  26 => 2,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_news_list.tpl", "D:\\Files\\doc\\BEP\\sites\\portal\\template\\_news_list.tpl");
    }
}
