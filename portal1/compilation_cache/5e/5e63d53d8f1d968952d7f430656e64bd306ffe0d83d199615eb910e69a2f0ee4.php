<?php

/* _side_bar.twig */
class __TwigTemplate_c51b0e6eccac21c37f8a5f9e00811b49eee5be551a5f67c55cc19d5bcecedcc8 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<ul>
  <li><a href=\"profil.php?id=";
        // line 2
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", array()), "html", null, true);
        echo "\">Моя страница</a></li>
  <li><a href=\"news.php\">Новости</a></li>
  <li><a href=\"employees.php\">Сотрудники</a></li>
  <li><a href=\"#\">Библиотека</a></li>
  <li><a href=\"#\">Столовая</a></li>
</ul>";
    }

    public function getTemplateName()
    {
        return "_side_bar.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 2,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_side_bar.twig", "D:\\Files\\doc\\BEP\\sites\\portal\\template\\_side_bar.twig");
    }
}
