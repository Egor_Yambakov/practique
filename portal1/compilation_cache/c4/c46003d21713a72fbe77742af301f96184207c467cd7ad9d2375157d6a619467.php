<?php

/* _login_base.tpl */
class __TwigTemplate_6bc39d33a3fdeb85fc9f4fbced5312a6971b284950f8646ebdc03fe8539a791b extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html", "_login_base.tpl", 1);
        $this->blocks = array(
            'top_container' => array($this, 'block_top_container'),
            'main_content' => array($this, 'block_main_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_top_container($context, array $blocks = array())
    {
        // line 4
        echo "  ";
        $this->loadTemplate("_header.tpl", "_login_base.tpl", 4)->display($context);
    }

    // line 7
    public function block_main_content($context, array $blocks = array())
    {
        // line 8
        echo "  ";
        $this->loadTemplate("_login.tpl", "_login_base.tpl", 8)->display($context);
        // line 9
        echo "  <script src=\"/js/jquery.js\" type=\"text/javascript\"></script>
  <script src=\"/js/login.js\" type=\"text/javascript\"></script>
";
    }

    public function getTemplateName()
    {
        return "_login_base.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 9,  44 => 8,  41 => 7,  36 => 4,  33 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_login_base.tpl", "D:\\Files\\doc\\BEP\\sites\\online-profil.ru\\template\\_login_base.tpl");
    }
}
