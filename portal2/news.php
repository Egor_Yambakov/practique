<?

require_once('include/common.inc.php');

$id = $_COOKIE['id'] ?? null;
if (empty($id) && !is_numeric($id)) {
    redirect('index.php');
};

$data = getNewsPageData($_COOKIE["id"]);
echo getNewsPage('_news_base.twig', $data);