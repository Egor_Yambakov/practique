<?

require_once('include/common.inc.php');
require_once('include/upload_db.inc.php');

$ldap = new LdapConnection;
$ldap->__construct();
$users = $ldap->GetLdapUserList();

foreach ($users as $id => $user) {
    $user['department'] = requireDepartmentId($user['department']);
    $user['job_title'] = requireJobId($user['job_title']);
    $user['room'] = requireRoomId($user['room']);
    uploadUser($user);
}
removeUnusedRows();