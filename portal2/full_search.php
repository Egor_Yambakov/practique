<?

require_once('include/common.inc.php');

$id = $_COOKIE['id'] ?? null;
if (empty($id) && !is_numeric($id)) {
    redirect('index.php');
};

$data = getSearchPageData($_GET, $_COOKIE["id"]);
echo getSearchPage('_full_search_base.twig', $data);