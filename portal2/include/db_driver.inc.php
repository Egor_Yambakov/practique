<?

/**
 * connect to MySQL db or get error message
 */
function dbInitialConnect()
{
    global $g_dbLink;
    $g_dbLink = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
    if (!$g_dbLink) {
        die('Ошибка соединения: ' . mysqli_error($g_dbLink));
    };
    mysqli_query($g_dbLink, "SET NAMES 'utf8'");
}

/**
 * escapes special characters in a string
 *
 * @param string $value
 * @return string
 */
function dbQuote($value)
{
    global $g_dbLink;
    return mysqli_real_escape_string($g_dbLink, $value);
}

/**
 * find all rows of data from db table by MySQL query and returns array of rows
 *
 * @param string $query
 * @return array
 */
function dbFetchAssoc($query)
{
    global $g_dbLink;
    $data = array();
    $result = mysqli_query($g_dbLink, $query);
    if ($result && mysqli_num_rows($result))
    {
        $data = mysqli_fetch_all($result, MYSQLI_ASSOC);
        mysqli_free_result($result);
    };
    return $data;
}

/**
 * find one row of data from db table by MySQL query and returns it
 *
 * @param string $query
 * @return array
 */
function dbFetchOne($query)
{
    global $g_dbLink;

    $row = array();
    $result = mysqli_query($g_dbLink, $query);
    if ($result && mysqli_num_rows($result))
    {
        $row = mysqli_fetch_assoc($result);
        mysqli_free_result($result);
    }

    return $row;
}

/**
 * find all rows of data from db by MySQL query and returns count of rows
 *
 * @param string $query
 * @return int
 */
function dbCountFetch($query)
{
    global $g_dbLink;
    $count = 0;
    $result = mysqli_query($g_dbLink, $query);
    if ($result)
    {
        $count = mysqli_num_rows($result);
        mysqli_free_result($result);
    };
    return $count;
}

/**
 * make MySQL query and returns the success of the request
 *
 * @param $query
 * @return bool
 */
function dbQuery($query)
{
    global $g_dbLink;
    return (bool)mysqli_query($g_dbLink, $query);
}

/**
 * returns id of last inserted row
 * @return int
 */
function dbLastInsertId()
{
    global $g_dbLink;
    return mysqli_insert_id($g_dbLink);
}