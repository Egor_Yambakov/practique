<?
    function getPage($page_name, $data_arr)
    {
        $loader = new Twig_Loader_Filesystem('template');
        $twig = new Twig_Environment($loader, array
                                              (
                                                  'cache'       => 'template_c',
                                                  'auto_reload' => true
                                              ));

        return $twig->render($page_name, $data_arr);
    }