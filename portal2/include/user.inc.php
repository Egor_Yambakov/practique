<?php

/**
 * find user from db with given login and password and return array of data
 *
 * @param string $login
 * @return array
 */
function findUserIdByLogin($login)
{
    $data = null;
    $query = "SELECT `user_id` FROM `login` WHERE `user_login` = '" . trim(dbQuote($login)) . "'";
    $user_data = dbFetchOne($query);
    if ($user_data) {
        $data = $user_data['user_id'];
    };
    return $data;
}

/**
 * find first $limit users from db and return array of data
 *
 * @param int $limit
 * @return array
 */
function getLimitedUsers($limit)
{
    $query = "SELECT * FROM `user` LIMIT $limit";
    return getUsersData($query);
}

/**
 * find user by id in db and return ALL user data
 *
 * @param int $profile_id
 * @return array
 */
function getProfileById($profile_id)
{
    $profile_id = trim(dbQuote($profile_id));
    $query = "SELECT * FROM `user` WHERE `id` = '$profile_id'";
    return getFullUsersData($query)[0];
}


/**
 * find users by MySQL query and return users data
 *
 * @param string $user_query
 * @return array
 */
function getUsersData($user_query)
{
    $result_arr = dbFetchAssoc($user_query);

    $users_data = array();
    foreach ($result_arr as $user)
    {
        $user['job_title'] = getJobTitle($user['job_title_id']);
        $user['department'] = getDepTitle($user['department_id']);
        $user['room'] = getRoomNumber($user['room_id']);
        $user['email'] = preg_split("/\s*\,\s*/", $user['email']);
        $users_data[] = $user;
    };
    return $users_data;
}

/**
 * find users by MySQL query and return ALL users data
 *
 * @param string $user_query
 * @return array
 */
function getFullUsersData($user_query)
{
    $result_arr = dbFetchAssoc($user_query);

    $users_data = array();
    foreach ($result_arr as $user)
    {
        $user['job_title'] = getJobTitle($user['job_title_id']);
        $user['department'] = getDepTitle($user['department_id']);
        $user['room'] = getRoomNumber($user['room_id']);
        $user['skills'] = getSkillsById($user['id']);
        $user['interests'] = getInterestsById($user['id']);
        $user["birth"] = convertTime($user["birth"]);
        $user["phone"] = preg_split("/\s*\,\s*/", $user["phone"]);
        $user["email"] = preg_split("/\s*\,\s*/", $user["email"]);
        $users_data[] = $user;
    };
    return $users_data;
}
