<?
    require_once('const.php');
    require_once('vendor/autoload.php');
    require_once('main_logic.inc.php');
    require_once('db_get_lib.inc.php');
    require_once('db_update_lib.inc.php');
    require_once('db_driver.inc.php');
    require_once('twig_lib.inc.php');
    require_once('twig_driver.inc.php');
    require_once('user.inc.php');
    require_once('url.inc.php');
    require_once('LdapConnection.class.php');
    require_once('LdapConfig.class.php');

    dbInitialConnect();