<?php

function requireDepartmentId($department)
{
    $dep_id = NULL;
    if ($department) {
        $department = trim(dbQuote($department));
        $query = "SELECT * FROM `department` WHERE `name` = '" . $department . "'";
        $dep = dbFetchOne($query);
        if ($dep)
            $dep_id = $dep['id'];
        else {
            $insert_query = "INSERT INTO `department`
                             (`name`)
                             VALUES 
                             ('" . ucfirst_utf8($department) . "')";
            $result = dbQuery($insert_query);
            $dep_id = dbLastInsertId();
            if ($result) {
                echo "$department добавлен";
            } else {
                echo "Ошибка в запросе $insert_query";
            }
        };
    };
    return $dep_id;
}

function requireJobId($job_title)
{
    $job_id = NULL;
    if ($job_title) {
        $job_title = trim(dbQuote($job_title));
        $query = "SELECT * FROM `job_title` WHERE `name` = '" . $job_title . "'";
        $job = dbFetchOne($query);
        if ($job)
            $job_id = $job['id'];
        else {
            $insert_query = "INSERT INTO `job_title`
                             (`name`)
                             VALUES 
                             ('" . ucfirst_utf8($job_title) . "')";
            $result = dbQuery($insert_query);
            $job_id = dbLastInsertId();
            if ($result) {
                echo "$job_title добавлен";
            } else {
                echo "Ошибка в запросе $insert_query";
            }
        };
    };
    return $job_id;

}

function requireRoomId($room)
{
    $room_id = NULL;
    if ($room) {
        $room = trim(dbQuote($room));
        $query = "SELECT * FROM `room` WHERE `number` = '" . $room . "'";
        $room_result = dbFetchOne($query);
        if ($room_result)
            $room_id = $room_result['id'];
        else {
            $insert_query = "INSERT INTO `room`
                             (`number`)
                             VALUES 
                             ('" . ucfirst_utf8($room) . "')";
            $result = dbQuery($insert_query);
            $room_id = dbLastInsertId();
            if ($result) {
                echo "$room добавлен";
            } else {
                echo "Ошибка в запросе $insert_query";
            }
        };
    };
    return $room_id;

}

function removeUnusedRows()
{

}

function uploadUser($user_data)
{
    $query_user = "SELECT `id` 
                   FROM `user` 
                   WHERE `login` = '" . trim(dbQuote($user_data['user_login'])) . "'";
    $result = dbFetchOne($query_user);
    if ($result) {
        $user_id = $result['id'];
        updateUser($user_id, $user_data);
    } else {
        createUser($user_data);
    };
}

function createUser($user_data)
{
    $user_name = $user_data['user_name'] ? $user_data['user_name'] : $user_data['transcription_name'];
    $photo = $user_data['avatar_link'] ? $user_data['avatar_link'] : './images/users/no_avatar.png';
    $email = $user_data['user_email'] ? $user_data['user_email'] : '';
    $phone = $user_data['phone'] ? $user_data['phone'] : '';
    $department = $user_data['department'] ? $user_data['department'] : 'NULL';
    $job_title = $user_data['job_title'] ? $user_data['job_title'] : 'NULL';
    $room = $user_data['room'] ? $user_data['room'] : 'NULL';

    $insert_query = "INSERT INTO `user`
                     (`name`, `phone`, `department_id`, `job_title_id`, `email`, `photo`, `reg_date`, `birth`, `room_id`, `work_descr`, `login`)
                     VALUES 
                     ('$user_name', '$phone', $department, $job_title, '$email', '$photo', " . $user_data['reg_date'] . ", 0, $room, '', '" . $user_data['user_login'] . "')";
    $result = dbQuery($insert_query);
    $user_id = dbLastInsertId();
    if ($result) {
        echo "Пользователь c id = $user_id добвален<br>";
    } else {
        echo "Ошибка в запросе: $insert_query<br>";
    }
}

function updateUser($user_id, $user_data)
{
    $user_name = $user_data['user_name'] ? $user_data['user_name'] : $user_data['transcription_name'];
    $photo = $user_data['avatar_link'] ? $user_data['avatar_link'] : './images/users/no_avatar.png';
    $email = $user_data['user_email'] ? $user_data['user_email'] : '';
    $phone = $user_data['phone'] ? $user_data['phone'] : '';
    $department = $user_data['department'] ? $user_data['department'] : 'NULL';
    $job_title = $user_data['job_title'] ? $user_data['job_title'] : 'NULL';
    $room = $user_data['room'] ? $user_data['room'] : 'NULL';

    $update_query = "UPDATE `user`
                     SET `name` = '$user_name', 
                         `department_id` = " . $department . ", 
                         `job_title_id` = " . $job_title . ", 
                         `photo` = '$photo',
                         `email` = '$email',
                         `phone` = '$phone',
                         `room_id` = " . $room . "
                     WHERE `id` = " . $user_id;
    $result = dbQuery($update_query);
    if ($result) {
        echo "Пользователь c id = $user_id обновлен<br>";
    } else {
        echo "Ошибка в запросе: $update_query<br>";
    }
}

function ucfirst_utf8($str)
{
    return mb_strtoupper(mb_substr($str, 0, 1)) . mb_substr($str, 1);
}