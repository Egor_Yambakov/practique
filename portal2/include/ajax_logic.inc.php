<?

function updateData($form_data)
{
    $err = '';
    $user_id = $form_data["id"];
    $err .= updateDepartment($user_id, $form_data["department"]);
    $err .= updateEmail($user_id, $form_data["email"]);
    $err .= updateInterests($user_id, $form_data["interests"]);
    $err .= updateSkills($user_id, $form_data["skills"]);
    $err .= updateJob($user_id, $form_data["job_title"]);
    $err .= updatePhone($user_id, $form_data["phone"]);
    $err .= updateRoom($user_id, $form_data["room"]);
    $err .= updateWorkDescr($user_id, $form_data["work_descr"]);
    return $err;
}

function getSearchNamesData()
{
    return getLimitedUsers(SMALL_SEARCH_NAMES_LIMIT);
}

function getSearchQueryData($query)
{
    $data = array();
    if (isset($query) and !empty($query)) {
        $data = getFullList($query);

        if (empty($data['names']) && empty($data['skills']) && empty($data['interests']))
            $data['err'] = 'Совпадений не обнаружено';
        else {
            $data['names'] = highlightQuery(array_slice($data['names'], 0, SMALL_SEARCH_NAMES_LIMIT), $query);
            $data['skills'] = highlightQuery(array_slice($data['skills'], 0, SMALL_SEARCH_TAGS_LIMIT), $query);
            $data['interests'] = highlightQuery(array_slice($data['interests'], 0, SMALL_SEARCH_TAGS_LIMIT), $query);
        };
    };
    return $data;
}

function highlightQuery($result_arr, $query_string)
{
    $query_arr = preg_split("/\s+/", trim($query_string));
    foreach ($result_arr as &$result) {
        foreach ($query_arr as $query) {
            $result["name"] = preg_replace("/$query/ui", "<b>\\0</b>", $result["name"]);
            $result["job_title"] = preg_replace("/$query/ui", "<b>\\0</b>", $result["job_title"]);
        }
    };
    return($result_arr);
}