<?  
    define("DB_SERVER", "localhost");
    define("DB_USERNAME", "root");
    define("DB_PASSWORD", "root");
    define("DB_NAME", "ispring_portal");

    define('EMPLOYEES_EMPTY_TITLE', 'Сотрудники');
    define('NEWS_DAYS_PERIOD', 30);
    define('SMALL_SEARCH_NAMES_LIMIT', 8);
    define('SMALL_SEARCH_TAGS_LIMIT', 4);