<?

function getNewsPageData($user_id)
{
    $data = array
    (
        'list'   => getNewsData(),
        'user'   => getUserDataById($user_id),
        'filter' => getFilterData()
    );
    return $data;
}

function getProfilPageData($profil_id, $user_id)
{
    $data = array
    (
        'profil' => getProfileById($profil_id),
        'user'   => getUserDataById($user_id),
        'edit'   => getEditData()
    );
    return $data;
}

function getEmployeesPageData($dep_id, $user_id)
{
    $list_data = getEmployeesData($dep_id);
    $data = array
    (
        'employees' => $list_data['employees'],
        'query'     => $list_data['dep_query'],
        'user'      => getUserDataById($user_id),
        'filter'    => getFilterData()
    );
    return $data;
}

function getSearchPageData($query_arr, $user_id)
{
    $list_data = getSearchData($query_arr);
    $names = &$list_data['data']['query_data']['names'];
    if ($names) {
        foreach ($names as $key => $value) {
            if ($value['id'] == $user_id) {
                unset($names[$key]);
            };
        };
    };
    $data = array
    (
        'list'      => $list_data['data'],
        'query'     => $list_data['query'],
        'user'      => getUserDataById($user_id),
        'filter'    => getFilterData()
    );
    return $data;
}

function checkDataExist($data, $access_location)
{
    if (isset($data) && !empty($data) && is_numeric($data)) {
        redirect($access_location);
    };
}

function checkDataNotExist($data, $err_location)
{
    if (!isset($data) or empty($data) or !is_numeric($data)) {
        redirect($err_location);
    };
}