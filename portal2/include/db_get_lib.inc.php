<?

function getUserDataById($user_id)
{
    $query = "SELECT * FROM `user` WHERE `id` = '$user_id'";
    return dbFetchOne($query);
}

function getFilterData()
{
    $query_dep = "SELECT * FROM `department`";
    $query_skills = "SELECT * FROM `skills`";
    $query_interests = "SELECT * FROM `interest`";

    $data = array
    (
        'department' => dbFetchAssoc($query_dep),
        'skills'     => getSkills($query_skills),
        'interests'  => getInterests($query_interests)
    );
    return $data;
}

function getEmployeesData($dep_id)
{
    $data = array();
    $query = "SELECT `name` FROM `department` WHERE `id` = ".$dep_id;
    $dep_data = dbFetchOne($query);
    if (!$dep_data) {
        $query = "SELECT * FROM `user`";
        $data['dep_query'] = EMPLOYEES_EMPTY_TITLE.' ('.dbCountFetch($query).')';
    } else {
        $query = "SELECT * FROM `user` WHERE `department_id` = ".$dep_id;
        $data['dep_query'] = $dep_data['name'].' ('.dbCountFetch($query).')';
    };
    $data['employees'] = getUsersData($query);
    return $data;
}

function getNewsData()
{
    $delta_time = time() - (60 * 60 * 24 * NEWS_DAYS_PERIOD);

    $user_query = "SELECT * FROM `user` WHERE `reg_date` > $delta_time";
    $skills_query = "SELECT * FROM `skills` WHERE `reg_date` > $delta_time";
    $interests_query = "SELECT * FROM `interest` WHERE `reg_date` > $delta_time";

    $data = array
    (
        'names'     => getUsersData($user_query),
        'skills'    => getSkills($skills_query),
        'interests' => getInterests($interests_query)
    );
    return $data;
}

function getSearchData($query_arr)
{
    $filter = '';
    $data = array();
    if (isset($query_arr["skill_id"]) AND !empty($query_arr["skill_id"])) {
        $query_tag = "SELECT * FROM `skills` WHERE `id` = ".dbQuote($query_arr["skill_id"]);
        $skill_info = getSkills($query_tag)[0];
        $filter = $skill_info['name'].' ('.$skill_info['count'].')';

        $query = "SELECT * 
                  FROM `user` 
                  WHERE `id` 
                  IN (SELECT `user_id` 
                      FROM `user_skills` 
                      WHERE `skill_id` = ".dbQuote($query_arr["skill_id"]).")";
        $data["skills"] = getUsersData($query);
    }
    if (isset($query_arr["interest_id"]) AND !empty($query_arr["interest_id"])) {
        $query_tag = "SELECT * FROM `interest` WHERE `id` = ".dbQuote($query_arr["interest_id"]);
        $interest_info = getInterests($query_tag)[0];
        $filter = $interest_info['name'].' ('.$interest_info['count'].')';

        $query = "SELECT * 
                  FROM `user` 
                  WHERE `id` 
                  IN (SELECT `user_id` 
                      FROM `user_interest` 
                      WHERE `interest_id` = ".dbQuote($query_arr["interest_id"]).")";
        $data["interests"] = getUsersData($query);
    }
    if (isset($query_arr["search"]) AND !empty($query_arr["search"])) {
        $filter = $query_arr["search"];
        $data["query_data"] = getFullList($query_arr["search"]);
    };
    $data_arr = array
    (
        'query' => $filter,
        'data'  => $data
    );
    return $data_arr;
}

function getFullList($search_query)
{
    $search_query = trim(dbQuote(quotemeta($search_query)));
    $query_names = "SELECT * 
                    FROM `user` 
                    WHERE `id` IN (SELECT `user_id` 
                                   FROM `user_mirror` 
                                   WHERE MATCH(`description`) 
                                         AGAINST('".makeMatchAgainstString($search_query)."' IN BOOLEAN MODE)>0)";
    $query_skills = "SELECT * FROM `skills` WHERE `name` REGEXP '".makeRegexpString($search_query)."'";
    $query_interests = "SELECT * FROM `interest` WHERE `name` REGEXP '".makeRegexpString($search_query)."'";
    $data = array
    (
        'names' => getUsersData($query_names),
        'skills' => getSkills($query_skills),
        'interests' => getInterests($query_interests)
    );
    return $data;
}

function makeMatchAgainstString($query_string)
{
    $query_string = str_replace('-', ' ', $query_string);
    return preg_replace("/(?:^|\s)([^\s]+)/", "+(\\1*) ", $query_string);
}

function makeRegexpString($query_string)
{
    return preg_replace("/\s+/", '|', $query_string);
}

function getEditData()
{
    $query_room = "SELECT * FROM `room`";
    $query_dep = "SELECT * FROM `department`";
    $query_job = "SELECT * FROM `job_title`";

    $data = array
    (
        'rooms'       => dbFetchAssoc($query_room),
        'departments' => dbFetchAssoc($query_dep),
        'job_titles' => dbFetchAssoc($query_job)
    );
    return $data;
}

function getJobTitle($job_id)
{
    $job_query = "SELECT `name` FROM `job_title` WHERE `id` = '$job_id'";
    return dbFetchOne($job_query)['name'];
}

function getDepTitle($dep_id)
{
    $job_query = "SELECT `name` FROM `department` WHERE `id` = '$dep_id'";
    return dbFetchOne($job_query)['name'];
}

function getRoomNumber($room_id)
{
    $job_query = "SELECT `number` FROM `room` WHERE `id` = '$room_id'";
    return dbFetchOne($job_query)['number'];
}

function getSkillsById($user_id)
{
    $skills_query = "SELECT * FROM `skills` WHERE `id` IN (SELECT `skill_id` FROM `user_skills` WHERE `user_id` = '".$user_id."')";
    return getSkills($skills_query);
}

function getInterestsById($user_id)
{
    $interests_query = "SELECT * FROM `interest` WHERE `id` IN (SELECT `interest_id` FROM `user_interest` WHERE `user_id` = '".$user_id."')";
    return getInterests($interests_query);
}

function getSkills($query)
{
    $skills = array();
    $result_arr = dbFetchAssoc($query);

    foreach ($result_arr as $skill) {
        $q1 = "SELECT * FROM `user_skills` WHERE `skill_id` = '".$skill['id']."'";
        $skill['count'] = dbCountFetch($q1);
        if ($skill['count']) {
            $skills[] = $skill;
        };
    };
    return $skills;
}

function getInterests($query)
{
    $interests = array();
    $result_arr = dbFetchAssoc($query);

    foreach ($result_arr as $interest) {
        $q1 = "SELECT * FROM `user_interest` WHERE `interest_id` = '".$interest['id']."'";
        $interest['count'] = dbCountFetch($q1);
        if ($interest['count']) {
            $interests[] = $interest;
        };
    };
    return $interests;
}

function convertTime($time)
{
    return date("j", $time).getMonth(date("n", $time)).date("Y", $time)."г.";
}

function getMonth($month_id)
{
    $months = array
    (
        " null ",
        " января ",
        " февраля ",
        " марта ",
        " aпреля ",
        " мая ",
        " июня ",
        " июля ",
        " aвгуста ",
        " сентября ",
        " октября ",
        " ноября ",
        " декабря "
    );
    return $months[$month_id];
}