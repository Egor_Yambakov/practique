<?php

class LdapConnection
{
    private $ldapConnection   = null;
    private $connectionResult = false;
    private $login            = null;
    private $password         = null;
    private $user_id          = null;

    public function __construct()
    {
        $this->login = LdapConfig::LDAP_LOGIN;
        $this->password = LdapConfig::LDAP_PASS;
        $this->Init();
    }

    public function __setConnection($post_login, $post_pass)
    {
        $this->login = $post_login;
        $this->password = $post_pass;
        $this ->Init();
        $this->SetCredentials();
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    private function Init()
    {
        $this->ldapConnection = ldap_connect(LdapConfig::LDAP_HOST, LdapConfig::LDAP_PORT);
        if (!$this->ldapConnection) {
            return;
        };
        $this->connectionResult = @ldap_bind($this->ldapConnection, $this->login . LdapConfig::LDAP_DOMAIN, $this->password);
    }

    private function ldapTimeToUnix($ldap_time)
    {
        preg_match("/^(\d+).?0?(([+-]\d\d)(\d\d)|Z)$/i", $ldap_time, $matches);
        $tz = (strtoupper($matches[2]) == 'Z') ? 'UTC' : $matches[3].':'.$matches[4];
        $date = new \DateTime($matches[1], new \DateTimeZone($tz));

        return strtotime($date->format('Y-m-d H:i:s'));
    }

    private function SetCredentials()
    {
        if (!$this->ldapConnection) {
            return;
        };
        $this->connectionResult = @ldap_bind($this->ldapConnection, $this->login . LdapConfig::LDAP_DOMAIN, $this->password);
        $user_id = findUserIdByLogin($this->login);
        if ($this->connectionResult && $user_id) {
            $this->user_id = $user_id;
        };
    }

    public function GetLdapUserList()
    {
        $userList = [];
        if ($this->connectionResult) {
            $ldapUserInfoAttributes = array('samaccountname', 'description', 'name', 'mail', 'title', 'thumbnailphoto', 'whencreated', 'telephonenumber', 'department', 'physicaldeliveryofficename');
            $renamedUserInfoAttributes = array('user_login', 'user_name', 'transcription_name', 'user_email', 'job_title', 'avatar_link', 'reg_date', 'phone', 'department', 'room');
            $result = ldap_search($this->ldapConnection, LdapConfig::LDAP_USER_GROUP, LdapConfig::LDAP_FILTER . ")", $ldapUserInfoAttributes);
            $ldapUserListEntries = ldap_get_entries($this->ldapConnection, $result);

            foreach ($ldapUserListEntries as $userIndex => $ldapUserListEntry) {
                if (!empty($ldapUserListEntry['samaccountname'])) {
                    foreach ($renamedUserInfoAttributes as $attrIndex => $renamedUserInfoAttribute) {
                        if (!empty($ldapUserListEntry[$ldapUserInfoAttributes[$attrIndex]][0])) {
                            if ($renamedUserInfoAttribute != 'avatar_link' && $renamedUserInfoAttribute != 'reg_date') {
                                $userList[$userIndex][$renamedUserInfoAttribute] = mb_convert_encoding($ldapUserListEntry[$ldapUserInfoAttributes[$attrIndex]][0], 'UTF-8', 'Windows-1251');
                            } elseif ($renamedUserInfoAttribute == 'avatar_link') {
                                $source = imagecreatefromstring($ldapUserListEntry[$ldapUserInfoAttributes[$attrIndex]][0]);
                                imagejpeg($source, $_SERVER['DOCUMENT_ROOT'] . '/online.portal/public/images/users/' . $ldapUserListEntry['samaccountname'][0] . '.jpg');
                                $userList[$userIndex][$renamedUserInfoAttribute] = './images/users/' . $ldapUserListEntry['samaccountname'][0] . '.jpg';
                            } else {
                                $userList[$userIndex][$renamedUserInfoAttribute] = $this->ldapTimeToUnix($ldapUserListEntry['whencreated'][0]);
                            }
                        }
                    }
                }
            }
        }
        return $userList;
    }
}