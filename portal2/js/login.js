const MIN_FORM_LENGTH = 3;
$(loginHandler);

function loginHandler()
{
    $("#login_form").submit(checkInputs);
    $("#login_form input[name=login]").focus(returnBorder);
    $("#login_form input[name=password]").focus(returnBorder);
}

function checkInputs(event)
{
    if ($("#login_form input[name=login]").val().length < MIN_FORM_LENGTH)
    {
        event.preventDefault();
        ($("#login_form input[name=login]").css("border", "2px solid #ff4e4e"));
    };
    if ($("#login_form input[name=password]").val().length < MIN_FORM_LENGTH)
    {
        event.preventDefault();
        ($("#login_form input[name=password]").css("border", "2px solid #ff4e4e"));
    };
}

function returnBorder(event)
{
    $(event.target).css("border", "auto");
}