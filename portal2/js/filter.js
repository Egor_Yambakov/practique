const MAX_FILTER_LIST_LENGTH = 5;

$(filterHandler);

function filterHandler()
{
    $(".show_tags").each(hideShowButton);
    $(".hide_tags").each(hideHideButton);
    highlightCurrDepart();
}

function highlightCurrDepart()
{
    const query = getGetParameterByName("dep_query");
    $(".department_block ul li").each(function()
    {
        if ($(this).text() == query)
        {
            $(this).addClass("curr_department");
        }
    });
}

function hideShowButton()
{
    let element = $(this);
    if (element.parent().find("ul li").length <= MAX_FILTER_LIST_LENGTH + 1)
    {
        element.hide();
    }
    else
    {
        element.click(function() {
            $(this).parent().find("li").css("display", "inline-block");
            $(this).parent().find(".hide_tags").show();
            $(this).hide();
        });
    };
}

function hideHideButton()
{
    let element = $(this);
    element.hide();
    element.click(function() {
        $(this).parent().find("li").css("display", "");
        $(this).parent().find(".show_tags").show();
        $(this).hide();
    });
}