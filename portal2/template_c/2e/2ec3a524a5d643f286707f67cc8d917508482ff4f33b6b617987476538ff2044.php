<?php

/* _main_search_bar.twig */
class __TwigTemplate_6f1157a53e022036c64c3fd2f2ee7dfcd0529cb98dfb7fd59c366c1b5f52dceb extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"page_title border_bottom\">
  <h1>Результаты поиска</h1>
</div>
<form class=\"main_search border_bottom\" id=\"main_search\" method=\"GET\" action=\"full_search.php\">
  <a href=\"#\" class=\"search_icon\" onClick=\"document.getElementById('main_search').submit();\">
    <img src=\"images/icons/search_icon.svg\" alt=\"\">
  </a>
  <input value=\"";
        // line 8
        echo twig_escape_filter($this->env, ($context["query"] ?? null), "html", null, true);
        echo "\" type=\"text\" name=\"search\" class=\"main_search_box\" id=\"main_search_box\" autocomplete=\"off\">
  <a class=\"clear_search_btn\">
    <img src=\"images/icons/exit_icon.svg\" alt=\"\">
  </a>
</form>";
    }

    public function getTemplateName()
    {
        return "_main_search_bar.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  32 => 8,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_main_search_bar.twig", "C:\\dev\\project\\portal\\template\\_main_search_bar.twig");
    }
}
