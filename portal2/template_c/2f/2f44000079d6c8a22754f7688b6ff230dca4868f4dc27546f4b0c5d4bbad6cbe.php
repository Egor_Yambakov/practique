<?php

/* _header.twig */
class __TwigTemplate_001a73a8dffaf21b735774af781e8b1d2e6e7819c5a08e89319e9c66975081f9 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "    <div class=\"top_bar\">
    <div class=\"brand\">
        <div class=\"main_logo\">
            <a href=\"index.php\">
                <img src=\"images/icons/ispring-logo-bold.png\" alt=\"ispring-logo\">
            </a>
        </div>
        <div class=\"main_title\">
            <span>Корпоративный портал</span>
        </div>
    </div>
    ";
        // line 12
        if (twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", array())) {
            // line 13
            echo "        <form class=\"search_bar\" id=\"top_search\" method=\"GET\" action=\"full_search.php\">
            <input type=\"text\" placeholder=\"Поиск\" name=\"search\" id=\"search_box\" autocomplete=\"off\">
            <div class=\"search_advice border box_shadow\" id=\"search_advice\">
                <a class=\"title result_page_link border_bottom\">Показать все результаты</a>
                <div id=\"result_names_list\">
                </div>
                <div class=\"result_skills\">
                    <h4 class=\"title\">Навыки</h4>
                    <ul id=\"result_skills_list\"></ul>
                </div>
                <div class=\"result_skills\">
                    <h4 class=\"title\">Интересы</h4>
                    <ul id=\"result_interests_list\"></ul>
                </div>
            </div>
        </form>
        <div class=\"login_btn\">
            <span>";
            // line 30
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "name", array()), "html", null, true);
            echo "</span>
            <img src=\"";
            // line 31
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "photo", array()), "html", null, true);
            echo "\" alt=\"\">
            <div class=\"dropdown_menu box_shadow\">
                <a href=\"profile.php?id=";
            // line 33
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", array()), "html", null, true);
            echo "\">Мой профиль</a>
                <a href=\"profile.php?id=";
            // line 34
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", array()), "html", null, true);
            echo "&edit\">Редактировать</a>
                <a href=\"logout.php\">Выйти</a>
            </div>
        </div>
    ";
        }
        // line 39
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "_header.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 39,  70 => 34,  66 => 33,  61 => 31,  57 => 30,  38 => 13,  36 => 12,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_header.twig", "C:\\dev\\project\\portal\\template\\_header.twig");
    }
}
