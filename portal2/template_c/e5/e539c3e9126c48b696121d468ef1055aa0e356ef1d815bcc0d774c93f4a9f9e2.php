<?php

/* _login.twig */
class __TwigTemplate_6fcc6d86a2f7d00ec105062f8c05c426eb808fe09504de2e9256878b8a995bb3 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"login_form_container box_shadow\">
    <h2>Вход в корпоративный портал iSpring</h2>
    <span class=\"err_text\">";
        // line 3
        echo twig_escape_filter($this->env, ($context["err_text"] ?? null), "html", null, true);
        echo "</span>
    <form id=\"login_form\" method=\"POST\">
        <label>Логин<input autofocus type=\"text\" name=\"login\"></label>
        <label>Пароль<input type=\"password\" name=\"password\"></label>
        <input type=\"submit\" name=\"submit\" value=\"Войти\">
    </form>
</div>";
    }

    public function getTemplateName()
    {
        return "_login.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_login.twig", "C:\\dev\\project\\portal\\template\\_login.twig");
    }
}
