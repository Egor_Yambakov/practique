<?php

/* _full_search_list.twig */
class __TwigTemplate_e08631d8915c827aa2ea3d6b76735562bf1a1ee924927319c65afe65c7aed924 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "interests", array())) {
            // line 2
            echo "<div class=\"main_search_title border_top border_bottom\">
  <h1>Интересы (";
            // line 3
            echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "interests", array())), "html", null, true);
            echo ")</h1>
</div>
<div class=\"result_blocks\">
  ";
            // line 6
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "interests", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["userName"]) {
                // line 7
                echo "  <div class=\"pasport_block\">
    <a href=\"profile.php?id=";
                // line 8
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["userName"], "id", array()), "html", null, true);
                echo "\" class=\"pasport_photo\">
      <img src=\"";
                // line 9
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["userName"], "photo", array()), "html", null, true);
                echo "\" alt=\"\">
    </a>
    <div class=\"pasport_info\">
      <h3 class=\"title\"><a href=\"profile.php?id=";
                // line 12
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["userName"], "id", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["userName"], "name", array()), "html", null, true);
                echo "</a></h3>
      <span>";
                // line 13
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["userName"], "job_title", array()), "html", null, true);
                echo "</span>
    </div>
  </div>
  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['userName'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 17
            echo "</div>
";
        } elseif (twig_get_attribute($this->env, $this->source,         // line 18
($context["list"] ?? null), "skills", array())) {
            // line 19
            echo "<div class=\"main_search_title border_top border_bottom\">
  <h1>Навыки (";
            // line 20
            echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "skills", array())), "html", null, true);
            echo ")</h1>
</div>
<div class=\"result_blocks\">
  ";
            // line 23
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "skills", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["userName"]) {
                // line 24
                echo "  <div class=\"pasport_block\">
    <a href=\"profile.php?id=";
                // line 25
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["userName"], "id", array()), "html", null, true);
                echo "\" class=\"pasport_photo\">
      <img src=\"";
                // line 26
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["userName"], "photo", array()), "html", null, true);
                echo "\" alt=\"\">
    </a>
    <div class=\"pasport_info\">
      <h3 class=\"title\"><a href=\"profile.php?id=";
                // line 29
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["userName"], "id", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["userName"], "name", array()), "html", null, true);
                echo "</a></h3>
      <span>";
                // line 30
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["userName"], "job_title", array()), "html", null, true);
                echo "</span>
    </div>
  </div>
  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['userName'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 34
            echo "</div>
";
        } else {
            // line 36
            echo "  ";
            if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "query_data", array()), "names", array()))) {
                // line 37
                echo "    <div class=\"main_search_title border_top border_bottom\">
      <h1>Сотрудники (";
                // line 38
                echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "query_data", array()), "names", array())), "html", null, true);
                echo ")</h1>
    </div>
    <div class=\"result_blocks\">
      ";
                // line 41
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "query_data", array()), "names", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["userName"]) {
                    // line 42
                    echo "      <div class=\"pasport_block\">
        <a href=\"profile.php?id=";
                    // line 43
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["userName"], "id", array()), "html", null, true);
                    echo "\" class=\"pasport_photo\">
          <img src=\"";
                    // line 44
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["userName"], "photo", array()), "html", null, true);
                    echo "\" alt=\"\">
        </a>
        <div class=\"pasport_info\">
          <h3 class=\"title\"><a href=\"profile.php?id=";
                    // line 47
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["userName"], "id", array()), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["userName"], "name", array()), "html", null, true);
                    echo "</a></h3>
          <span>";
                    // line 48
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["userName"], "job_title", array()), "html", null, true);
                    echo "</span>
        </div>
      </div>
      ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['userName'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 52
                echo "    </div>
  ";
            }
            // line 54
            echo "  ";
            if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "query_data", array()), "skills", array()))) {
                // line 55
                echo "    <div class=\"main_search_title border_top border_bottom\">
      <h1>Навыки (";
                // line 56
                echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "query_data", array()), "skills", array())), "html", null, true);
                echo ")</h1>
    </div>
    <div class=\"result_skills_list tag_set\">
      <ul>
      ";
                // line 60
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "query_data", array()), "skills", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["skill"]) {
                    // line 61
                    echo "        <li>
          <a href=\"full_search.php?skill_id=";
                    // line 62
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["skill"], "id", array()), "html", null, true);
                    echo "\">#";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["skill"], "name", array()), "html", null, true);
                    echo "</a> (";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["skill"], "count", array()), "html", null, true);
                    echo ")
        </li>
      ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['skill'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 65
                echo "      </ul>
    </div>
  ";
            }
            // line 68
            echo "  ";
            if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "query_data", array()), "interests", array()))) {
                // line 69
                echo "    <div class=\"main_search_title border_top border_bottom\">
      <h1>Интересы (";
                // line 70
                echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "query_data", array()), "interests", array())), "html", null, true);
                echo ")</h1>
    </div>
    <div class=\"result_skills_list tag_set\">
      <ul>
      ";
                // line 74
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "query_data", array()), "interests", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["interest"]) {
                    // line 75
                    echo "        <li>
          <a href=\"full_search.php?interest_id=";
                    // line 76
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["interest"], "id", array()), "html", null, true);
                    echo "\">#";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["interest"], "name", array()), "html", null, true);
                    echo "</a> (";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["interest"], "count", array()), "html", null, true);
                    echo ")
        </li>
      ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['interest'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 79
                echo "      </ul>
    </div>
  ";
            }
            // line 82
            echo "  ";
            if ((( !twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "query_data", array()), "names", array())) &&  !twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "query_data", array()), "skills", array()))) &&  !twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["list"] ?? null), "query_data", array()), "interests", array())))) {
                // line 83
                echo "    <div class=\"main_search_title border_top border_bottom\">
      <h1>Совпадений не обнаружено</h1>
    </div>
  ";
            }
        }
    }

    public function getTemplateName()
    {
        return "_full_search_list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  246 => 83,  243 => 82,  238 => 79,  225 => 76,  222 => 75,  218 => 74,  211 => 70,  208 => 69,  205 => 68,  200 => 65,  187 => 62,  184 => 61,  180 => 60,  173 => 56,  170 => 55,  167 => 54,  163 => 52,  153 => 48,  147 => 47,  141 => 44,  137 => 43,  134 => 42,  130 => 41,  124 => 38,  121 => 37,  118 => 36,  114 => 34,  104 => 30,  98 => 29,  92 => 26,  88 => 25,  85 => 24,  81 => 23,  75 => 20,  72 => 19,  70 => 18,  67 => 17,  57 => 13,  51 => 12,  45 => 9,  41 => 8,  38 => 7,  34 => 6,  28 => 3,  25 => 2,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_full_search_list.twig", "C:\\dev\\project\\portal\\template\\_full_search_list.twig");
    }
}
