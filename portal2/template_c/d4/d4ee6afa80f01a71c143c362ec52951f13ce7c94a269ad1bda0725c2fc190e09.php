<?php

/* _profil_main.twig */
class __TwigTemplate_05b4216f7c50106db773da3c4d7bf43779f6f5ea163954d717d04f12cbb3b60c extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"profil_container box_shadow\">
  <div class=\"side_block\">
    <div class=\"profil_side\">
      <div class=\"profil_photo\" style=\"background-image:url(";
        // line 4
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "photo", array()), "html", null, true);
        echo ")\"></div>
      ";
        // line 5
        if ((twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "id", array()) == twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", array()))) {
            // line 6
            echo "        <a class=\"button reversed\" id=\"edit_btn\">Редактировать</a>
      ";
        } else {
            // line 8
            echo "        <a class=\"button\" href=\"mailto:";
            echo twig_escape_filter($this->env, (($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5 = twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "email", array())) && is_array($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5) || $__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5 instanceof ArrayAccess ? ($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5[0] ?? null) : null), "html", null, true);
            echo "\" target=\"_blank\">Написать собщение</a>
      ";
        }
        // line 10
        echo "      ";
        if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "skills", array()))) {
            // line 11
            echo "      <div class=\"pasport_info skills tag_set\">
        <h3 class=\"title\">Навыки</h3>
        <ul class=\"skills_list\">
          ";
            // line 14
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "skills", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["skill"]) {
                echo "  
            <li><a href=\"full_search.php?skill_id=";
                // line 15
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["skill"], "id", array()), "html", null, true);
                echo "\">#";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["skill"], "name", array()), "html", null, true);
                echo "</a></li>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['skill'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 17
            echo "        </ul>
        <a class=\"show_btn\">показать все</a>
        <a class=\"hide_btn\">скрыть</a>
      </div>
      ";
        }
        // line 22
        echo "      ";
        if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "interests", array()))) {
            // line 23
            echo "      <div class=\"pasport_info skills tag_set\">
        <h3 class=\"title\">Интересы</h3>
        <ul class=\"skills_list\">
          ";
            // line 26
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "interests", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["interest"]) {
                echo "  
            <li><a href=\"full_search.php?interest_id=";
                // line 27
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["interest"], "id", array()), "html", null, true);
                echo "\">#";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["interest"], "name", array()), "html", null, true);
                echo "</a></li>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['interest'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 29
            echo "        </ul>
        <a class=\"show_btn\">показать все</a>
        <a class=\"hide_btn\">скрыть</a>
      </div>
      ";
        }
        // line 34
        echo "    </div>
  </div>
  <div class=\"main_block\">
    <div id=\"profil_information\">
      <div class=\"pasport_block\">
        <div class=\"pasport_info\">
          <h3 class=\"title\">";
        // line 40
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "name", array()), "html", null, true);
        echo "</h3>
          <span class=\"status\">
            <a href=\"full_search.php?search=";
        // line 42
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "job_title", array()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "job_title", array()), "html", null, true);
        echo "</a>
              ";
        // line 43
        if (twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "department", array())) {
            echo " в
                <a href=\"employees.php?dep_id=";
            // line 44
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "department_id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "department", array()), "html", null, true);
            echo "</a>
              ";
        }
        // line 46
        echo "          </span>
          <table>
            ";
        // line 48
        if ((($__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a = twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "phone", array())) && is_array($__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a) || $__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a instanceof ArrayAccess ? ($__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a[0] ?? null) : null)) {
            // line 49
            echo "            <tr>
              <td><span>Телефон</span></td>
              <td>
                ";
            // line 52
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "phone", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["phone"]) {
                // line 53
                echo "                <span>";
                echo twig_escape_filter($this->env, $context["phone"], "html", null, true);
                echo "</span><br>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['phone'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 55
            echo "              </td>
            </tr>
            ";
        }
        // line 58
        echo "            ";
        if (twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "room", array())) {
            // line 59
            echo "            <tr>
              <td><span>Кабинет</span></td>
              <td><span>";
            // line 61
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "room", array()), "html", null, true);
            echo "</span></td>
            </tr>
            ";
        }
        // line 64
        echo "            ";
        if ((($__internal_b0b3d6199cdf4d15a08b3fb98fe017ecb01164300193d18d78027218d843fc57 = twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "email", array())) && is_array($__internal_b0b3d6199cdf4d15a08b3fb98fe017ecb01164300193d18d78027218d843fc57) || $__internal_b0b3d6199cdf4d15a08b3fb98fe017ecb01164300193d18d78027218d843fc57 instanceof ArrayAccess ? ($__internal_b0b3d6199cdf4d15a08b3fb98fe017ecb01164300193d18d78027218d843fc57[0] ?? null) : null)) {
            // line 65
            echo "            <tr>
              <td><span>Email</span></td>
              <td>
                ";
            // line 68
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "email", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["email"]) {
                // line 69
                echo "                <a href=\"mailto:";
                echo twig_escape_filter($this->env, $context["email"], "html", null, true);
                echo "\" target=\"_blank\">";
                echo twig_escape_filter($this->env, $context["email"], "html", null, true);
                echo "</a><br>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['email'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 71
            echo "              </td>
            </tr>
            ";
        }
        // line 74
        echo "            <tr>
              <td><span>Дата рождения</span></td>
              <td><span>";
        // line 76
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "birth", array()), "html", null, true);
        echo "</span></td>
            </tr>
          </table>
        </div>
      </div>
      ";
        // line 81
        if (twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "work_descr", array())) {
            // line 82
            echo "      <div class=\"pasport_info work_descr\">
        <h3 class=\"title underline\">Описание деятельности</h3>
        <p>";
            // line 84
            echo nl2br(twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "work_descr", array()), "html", null, true));
            echo "</p>
        <a class=\"show_btn\">показать все</a>
        <a class=\"hide_btn\">скрыть</a>
      </div>
      ";
        }
        // line 89
        echo "    </div>

    ";
        // line 91
        if ((twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "id", array()) == twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", array()))) {
            // line 92
            echo "    <form class=\"edit_profil\">
      <div class=\"pasport_info\">
        <h3 class=\"title\">Редактирование профиля</h3>
        <span class=\"status\"></span>
        <label>
          <span>Отдел</span>
          <select name=\"department\">
          ";
            // line 99
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["edit"] ?? null), "departments", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["dep"]) {
                // line 100
                echo "            <option value=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["dep"], "name", array()), "html", null, true);
                echo "\" ";
                if ((twig_get_attribute($this->env, $this->source, $context["dep"], "name", array()) == twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "department", array()))) {
                    echo "selected";
                }
                echo ">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["dep"], "name", array()), "html", null, true);
                echo "</option>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dep'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 102
            echo "          </select>
        </label>
        <label>
          <span>Должность</span>
          <select name=\"job_title\">
          ";
            // line 107
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["edit"] ?? null), "job_titles", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["job"]) {
                // line 108
                echo "            <option value=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["job"], "name", array()), "html", null, true);
                echo "\" ";
                if ((twig_get_attribute($this->env, $this->source, $context["job"], "name", array()) == twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "job_title", array()))) {
                    echo "selected";
                }
                echo ">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["job"], "name", array()), "html", null, true);
                echo "</option>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['job'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 110
            echo "          </select>
        </label>
        <label>
          <span>Телефон</span>
          <input name=\"phone\" type=\"text\" value=\"";
            // line 114
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "phone", array()));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["phone"]) {
                echo twig_escape_filter($this->env, $context["phone"], "html", null, true);
                if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "last", array())) {
                    echo ", ";
                }
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['phone'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo "\">
        </label>
        <label>
          <span>Кабинет</span>
          <select name=\"room\">
          ";
            // line 119
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["edit"] ?? null), "rooms", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["room"]) {
                // line 120
                echo "            <option value=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["room"], "number", array()), "html", null, true);
                echo "\" ";
                if ((twig_get_attribute($this->env, $this->source, $context["room"], "number", array()) == twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "room", array()))) {
                    echo "selected";
                }
                echo ">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["room"], "number", array()), "html", null, true);
                echo "</option>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['room'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 122
            echo "          </select>
        </label>
        <label>
          <span>Email</span>
          <input name=\"email\" type=\"text\" value=\"";
            // line 126
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "email", array()));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["email"]) {
                echo twig_escape_filter($this->env, $context["email"], "html", null, true);
                if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "last", array())) {
                    echo ", ";
                }
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['email'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo "\">
        </label>
        <label class=\"textarea\">
          <h3>Навыки</h3>
          <textarea id=\"edit_skills\" name=\"skills\">";
            // line 130
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "skills", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["skill"]) {
                echo "#";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["skill"], "name", array()), "html", null, true);
                echo "   ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['skill'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo "</textarea>
        </label>
        <label class=\"textarea\">
          <h3>Интересы</h3>
          <textarea id=\"edit_interests\" name=\"interests\">";
            // line 134
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "interests", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["interest"]) {
                echo "#";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["interest"], "name", array()), "html", null, true);
                echo "   ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['interest'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo "</textarea>
        </label>
        <label class=\"textarea\">
          <h3>Описание деятельности</h3>
          <textarea name=\"work_descr\">";
            // line 138
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["profil"] ?? null), "work_descr", array()));
            echo "</textarea>
        </label>
        <a id=\"save_changes\" class=\"button\">Сохранить изменения</a>
      </div>
    </form>
    ";
        }
        // line 144
        echo "  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "_profil_main.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  441 => 144,  432 => 138,  416 => 134,  400 => 130,  362 => 126,  356 => 122,  341 => 120,  337 => 119,  298 => 114,  292 => 110,  277 => 108,  273 => 107,  266 => 102,  251 => 100,  247 => 99,  238 => 92,  236 => 91,  232 => 89,  224 => 84,  220 => 82,  218 => 81,  210 => 76,  206 => 74,  201 => 71,  190 => 69,  186 => 68,  181 => 65,  178 => 64,  172 => 61,  168 => 59,  165 => 58,  160 => 55,  151 => 53,  147 => 52,  142 => 49,  140 => 48,  136 => 46,  129 => 44,  125 => 43,  119 => 42,  114 => 40,  106 => 34,  99 => 29,  89 => 27,  83 => 26,  78 => 23,  75 => 22,  68 => 17,  58 => 15,  52 => 14,  47 => 11,  44 => 10,  38 => 8,  34 => 6,  32 => 5,  28 => 4,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_profil_main.twig", "C:\\dev\\project\\portal\\template\\_profil_main.twig");
    }
}
