<?php

/* _profile_base.twig */
class __TwigTemplate_3d642d81fb00766d26f89d78cf88eb1d7f7e851a545c82b03780a382b1db055b extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html", "_profile_base.twig", 1);
        $this->blocks = array(
            'top_container' => array($this, 'block_top_container'),
            'main_content' => array($this, 'block_main_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_top_container($context, array $blocks = array())
    {
        // line 4
        echo "  ";
        $this->loadTemplate("_header.twig", "_profile_base.twig", 4)->display($context);
    }

    // line 7
    public function block_main_content($context, array $blocks = array())
    {
        // line 8
        echo "<div class=\"main_content\">
  <div class=\"side_bar\">
    ";
        // line 10
        $this->loadTemplate("_side_bar.twig", "_profile_base.twig", 10)->display($context);
        // line 11
        echo "  </div>

  <div class=\"content_bar\">
    ";
        // line 14
        $this->loadTemplate("_profile_main.twig", "_profile_base.twig", 14)->display($context);
        // line 15
        echo "  </div>
</div>
<script src=\"/js/jquery.js\" type=\"text/javascript\"></script>
<script src=\"/js/header.js\" type=\"text/javascript\"></script>
<script src=\"/js/profile.js\" type=\"text/javascript\"></script>
";
    }

    public function getTemplateName()
    {
        return "_profile_base.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 15,  55 => 14,  50 => 11,  48 => 10,  44 => 8,  41 => 7,  36 => 4,  33 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_profile_base.twig", "C:\\dev\\project\\portal\\template\\_profile_base.twig");
    }
}
