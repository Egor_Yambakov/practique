<?php

/* _employees_base.twig */
class __TwigTemplate_a771c8732fcea8d0e4991087c82efa97de517d083f840bb60bafabac6a6d7901 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html", "_employees_base.twig", 1);
        $this->blocks = array(
            'top_container' => array($this, 'block_top_container'),
            'main_content' => array($this, 'block_main_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_top_container($context, array $blocks = array())
    {
        // line 4
        echo "  ";
        $this->loadTemplate("_header.twig", "_employees_base.twig", 4)->display($context);
    }

    // line 7
    public function block_main_content($context, array $blocks = array())
    {
        // line 8
        echo "<div class=\"main_content\">
  <div class=\"side_bar\">
    ";
        // line 10
        $this->loadTemplate("_side_bar.twig", "_employees_base.twig", 10)->display($context);
        // line 11
        echo "  </div>

  <div class=\"main_content_bar\">
    <div class=\"pasport_list box_shadow\">
      ";
        // line 15
        $this->loadTemplate("_main_search_bar.twig", "_employees_base.twig", 15)->display($context);
        // line 16
        echo "      ";
        $this->loadTemplate("_employees_list.twig", "_employees_base.twig", 16)->display($context);
        // line 17
        echo "    </div>
    ";
        // line 18
        $this->loadTemplate("_search_filter.twig", "_employees_base.twig", 18)->display($context);
        // line 19
        echo "  </div>
</div>
<script src=\"/js/jquery.js\" type=\"text/javascript\"></script>
<script src=\"/js/header.js\" type=\"text/javascript\"></script>
<script src=\"/js/search.js\" type=\"text/javascript\"></script>
<script src=\"/js/filter.js\" type=\"text/javascript\"></script>
";
    }

    public function getTemplateName()
    {
        return "_employees_base.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 19,  64 => 18,  61 => 17,  58 => 16,  56 => 15,  50 => 11,  48 => 10,  44 => 8,  41 => 7,  36 => 4,  33 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_employees_base.twig", "C:\\dev\\project\\portal\\template\\_employees_base.twig");
    }
}
