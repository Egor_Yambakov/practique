<?php

/* _employees_list.twig */
class __TwigTemplate_2e538d13c341458acd4864b4ff2a1a98c7657210c97477bedc0c6123bcd7d4e5 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["employees"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
            // line 2
            echo "<div class=\"pasport_block border_bottom\">
  <a href=\"profile.php?id=";
            // line 3
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "id", array()), "html", null, true);
            echo "\" class=\"pasport_photo\" style=\"background-image:url(";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "photo", array()), "html", null, true);
            echo ")\"></a>
  <div class=\"pasport_info\">
    <h3 class=\"title\"><a href=\"profile.php?id=";
            // line 5
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "name", array()), "html", null, true);
            if ((twig_get_attribute($this->env, $this->source, $context["row"], "id", array()) == twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", array()))) {
                echo " (Вы)";
            }
            echo "</a></h3>
    ";
            // line 6
            if ((twig_get_attribute($this->env, $this->source, $context["row"], "department", array()) && twig_get_attribute($this->env, $this->source, $context["row"], "job_title", array()))) {
                echo "<span>";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "job_title", array()), "html", null, true);
                if (twig_get_attribute($this->env, $this->source, $context["row"], "department", array())) {
                    echo " в ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "department", array()), "html", null, true);
                }
                echo "</span>";
            }
            // line 7
            echo "    ";
            if ((($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5 = twig_get_attribute($this->env, $this->source, $context["row"], "email", array())) && is_array($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5) || $__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5 instanceof ArrayAccess ? ($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5[0] ?? null) : null)) {
                echo "<span>";
                echo twig_escape_filter($this->env, (($__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a = twig_get_attribute($this->env, $this->source, $context["row"], "email", array())) && is_array($__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a) || $__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a instanceof ArrayAccess ? ($__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a[0] ?? null) : null), "html", null, true);
                echo "</span>";
            }
            // line 8
            echo "    ";
            if (twig_get_attribute($this->env, $this->source, $context["row"], "room", array())) {
                echo "<span>кабинет ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "room", array()), "html", null, true);
                echo "</span>";
            }
            // line 9
            echo "    <a class=\"email_button\" href=\"mailto:";
            echo twig_escape_filter($this->env, (($__internal_b0b3d6199cdf4d15a08b3fb98fe017ecb01164300193d18d78027218d843fc57 = twig_get_attribute($this->env, $this->source, $context["row"], "email", array())) && is_array($__internal_b0b3d6199cdf4d15a08b3fb98fe017ecb01164300193d18d78027218d843fc57) || $__internal_b0b3d6199cdf4d15a08b3fb98fe017ecb01164300193d18d78027218d843fc57 instanceof ArrayAccess ? ($__internal_b0b3d6199cdf4d15a08b3fb98fe017ecb01164300193d18d78027218d843fc57[0] ?? null) : null), "html", null, true);
            echo "\" target=\"_blank\"></a>
  </div>
</div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "_employees_list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 9,  63 => 8,  56 => 7,  46 => 6,  37 => 5,  30 => 3,  27 => 2,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_employees_list.twig", "C:\\dev\\project\\portal\\template\\_employees_list.twig");
    }
}
