<?
require_once('include/common.inc.php');

$id = $_COOKIE['id'] ?? null;
if (!empty($id) && is_numeric($id)) {
    redirect('/news.php');
};

$err_text = '';
if (isset($_POST['submit'])) {

    $ldap = new LdapConnection();
    $ldap->__setConnection($_POST['login'], $_POST['password']);
    $user_id = $ldap->getUserId();
    if ($user_id) {
        if (isset($_COOKIE["id"]))
        {
            setcookie("id", '', time() - 1, '/', "portal");
        }
        setcookie("id", $user_id, time() + 60 * 60 * 24 * 365, '/', "portal");
        redirect('news.php');
    } else
        $err_text = 'Пожалуйста, проверьте правильность написания логина и пароля';
};
echo getLoginPage('_login_base.twig', $err_text);
echo $err_text;
