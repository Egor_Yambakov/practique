<?

require_once('include/common.inc.php');

$id = $_COOKIE['id'] ?? null;
if (empty($id) && !is_numeric($id)) {
    redirect('index.php');
};

$profil_id = $_GET["id"] ?? null;
if (empty($profil_id) && !is_numeric($profil_id)) {
    redirect('profile.php?id='.$id);
};

$data = getProfilPageData($profil_id, $id);
echo getProfilPage('_profil_base.twig', $data);