<?

require_once('include/common.inc.php');

$id = $_COOKIE['id'] ?? null;
if (empty($id) && !is_numeric($id)) {
    redirect('index.php');
};

$data = getEmployeesPageData($_GET["dep_id"], $_COOKIE["id"]);
echo getEmployeesPage('_employees_base.twig', $data);